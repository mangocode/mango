"""Lennard jones 6 -12 translational potential."""
from numpy import einsum

__all__ = ['vv_trans', 'ff_trans']


def vv_trans(self):
    """Lennard Jones Potential."""
    # unit: erg

    self.lj[:] = self.sigma * self.rij
    einsum("i, i, i, i, i, i -> i", self.lj, self.lj, self.lj, self.lj, self.lj, self.lj, out=self.lj6)

    self.epot_tr[:] = 4. * self.epsilon * self.lj6 * (self.lj6 - 1.)


def ff_trans(self):
    """Calculate Translational Force."""
    # translational force
    self.force_trans = self.zeros.copy()
    self.lj[:] = self.sigma * self.rij
    einsum("i, i, i, i, i, i -> i", self.lj, self.lj, self.lj, self.lj, self.lj, self.lj, out=self.lj6)
    einsum("i, ij, i-> ij",
           self.epsilon24 * (2. * self.lj6 - 1.) * self.lj6,
           self.rijz, self.rij * self.rij, out=self.ftrans)

    ftransR = self.ftrans.ravel()

    self.subtractat(self.force_trans, self.long_ia, ftransR)
    self.addat(self.force_trans, self.long_ja, ftransR)

    self.force_trans = self.force_trans.reshape(self.shape)


def setup():
    import mango.position as mp
    mp.c.Error("M Lennard Jones potential run")


setup()
