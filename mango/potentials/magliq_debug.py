"""Potentials to test against current magliq version."""
from mango.tests.magliq import (vv_mag as vv_mag_liq,
                                vv_wca as vv_wca_liq,
                                ff_mag as ff_mag_liq,
                                ff_wca as ff_wca_liq,
                                hh_mag as hh_mag_liq)
import mango.tests.magliq as ml
from numpy import allclose, zeros, einsum

__all__ = ['vv_trans', 'vv_mag', 'ff_trans', 'ff_mag', 'hh_mag']


def vv_trans(self):
    epot = einsum('i -> ', self._vv_trans())
    d = zeros(d_dm.dm_energy.shape[0])  # dm.shape[0] == d.shape[0] so possible d = dm
    epot2 = wrapper(vv_wca_liq, rijz=d, orij=d_dm.dm_energy, epsilon=self.epsilon, sigma=self.sigma, rc_wca=1 / self.limit)
    if not allclose(epot, epot2):
        print(f"Error vv_wca {epot} {epot2}")
        exit(1)

    return epot


def vv_mag(self, mag, d):
    epot = einsum('i ->', self._vv_mag(mag, d))
    epot2 = vv_mag_liq(d, d_dm.dm_energy, mag)
    if not allclose(epot, epot2):
        print(f"Error vv_mag {epot} {epot2}")
        exit(1)

    return epot


def ff_trans(self):
    force = self._ff_trans()
    force2 = wrapper(ff_wca_liq, rijz=d_dm.d_force, orij=d_dm.dm_force, epsilon=self.epsilon, sigma=self.sigma, rc_wca=1 / self.limit)
    if not allclose(force, force2):
        print(f"Error ff_wca {force} {force2}")
        exit(1)

    return force


def ff_mag(self, mag):
    force = self._ff_mag(mag)
    force2 = ff_mag_liq(d_dm.d_force, d_dm.dm_force, mag)
    if not allclose(force, force2):
        print(f"Error ff_mag {force} {force2}")
        exit(1)

    return force


def hh_mag(self, mag):
    hfield = self._hh_mag(mag)
    hfield2 = hh_mag_liq(d_dm.d_force, d_dm.dm_force, mag)
    if not allclose(hfield, hfield2):
        print(f"Error hh_mag {hfield} {hfield2}")
        exit(1)

    return hfield


def wrapper(f, **kw):
    ml.epsilon = kw['epsilon']
    ml.rc_wca = kw['rc_wca']
    ml.sigma = kw['sigma']
    kw = {'rijz': kw['rijz'], 'orij': kw['orij']}
    return f(**kw)


class d_dm_grabber():

    def __init__(self):
        import mango.position as mp
        mp.c.Error("M Debugging run")
        mp.energy.energy_vars = self.ev(mp.energy.energy_vars)
        mp.force.force_vars = self.fv(mp.force.force_vars)

    def ev(self, f):
        def wrap(*args, **kw):
            self.dm_energy = args[-1]
            return f(*args, **kw)
        return wrap

    def fv(self, f):
        def wrap(*args, **kw):
            self.dm_force = args[-1]
            self.d_force = args[-2]
            return f(*args, **kw)
        return wrap


d_dm = d_dm_grabber()
