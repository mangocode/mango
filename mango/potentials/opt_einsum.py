"""
Opt Einsum test.

This module tests using opt-einsum version of einsum for the potentials
(https://github.com/dgasmith/opt_einsum)

The idea was to be able to precalculate as much as possible.
For small arrays this is deinitely slower that using c_einsum directly (~x2 at writing).

Possible extrapolations for different backends:

Dask (dask.pydata.org)
Tensorflow (tensorflow.org)
etc..

This module could in future be used for a GPU with one of the above backends.

Everything is set up before hand and the variable are just reprovided every time the functions are called

"""
# import opt_einsum as oe
try:
    from opt_einsum import contract_expression
except ImportError:
    from mango.constants import c
    c.Error('F Missing opt_einsum package from https://github.com/dgasmith/opt_einsum')
from numpy import where  # , allclose, array

__all__ = ['vv_trans', 'vv_mag', 'ff_trans', 'ff_mag', 'hh_mag', 'force_vars']


def _setup_forcevars(self):
    ljs = self.lj.shape
    self.fv1 = contract_expression("i, i, i -> i", self.rij.shape, self.rij.shape, self.rij.shape)  # = self.rij * self.rij * self.rij
    self.fv2 = contract_expression("ij,i -> ij", self.unitv.shape, self.rij.shape)  # dimensionles
    self.fv3 = contract_expression("i, i, i, i, i, i -> i", ljs, ljs, ljs, ljs, ljs, ljs)
    self.fv4 = contract_expression("i, i, ij, i-> ij", self.lind.shape, ljs, self.rijz.shape, self.rij.shape)


def _setup_ffmag(self):
    self.fm1 = contract_expression("iz,iz -> i", self.unitv.shape, self.unitv.shape)
    self.fm2 = contract_expression("iz,iz -> i", self.unitv.shape, self.unitv.shape)
    self.fm3 = contract_expression("iz,iz -> i", self.unitv.shape, self.unitv.shape)
    self.fm4 = contract_expression("ij, i -> ij", self.unitv.shape, self.rij.shape)
    self.fm5 = contract_expression("i, ij -> ij", self.dmag.shape, self.unitv.shape)
    self.fm6 = contract_expression("i, ij -> ij", self.magi.shape, self.unitv.shape)
    self.fm7 = contract_expression("i, ij -> ij", self.magj.shape, self.unitv.shape)


def _setup_hhmag(self):
    self.hm1 = contract_expression("iz,iz -> i", self.unitv.shape, self.unitv.shape)
    self.hm2 = contract_expression("iz,iz -> i", self.unitv.shape, self.unitv.shape)
    self.hm3 = contract_expression("ij, i -> ij", self.unitv.shape, self.rij.shape)
    self.hm4 = contract_expression("i, ij -> ij", self.magj.shape, self.unitv.shape)  # unit: oersted
    self.hm5 = contract_expression("ij, i -> ij", self.unitv.shape, self.rij.shape)  # unit: oersted
    self.hm6 = contract_expression("i, ij -> ij", self.magi.shape, self.unitv.shape)


def _setup_vvtrans(self):
    ljs = self.lj.shape
    self.vt1 = contract_expression(self.tr_estr[0], ljs, ljs, ljs, ljs, ljs, ljs)
    self.vt2 = contract_expression(self.tr_estr[1], self.lind.shape, ljs)


def _setup_vvmag(self):
    self.vm1 = contract_expression(self.mag_estr[0], self.rij.shape, self.rij.shape, self.rij.shape)  # = self.rij * self.rij * self.rij
    self.vm2 = contract_expression(self.mag_estr[1], self.unitv.shape, self.rij.shape)  # dimensionles
    self.vm3 = contract_expression(self.mag_estr[2], self.unitv.shape, self.unitv.shape)
    self.vm4 = contract_expression(self.mag_estr[2], self.unitv.shape, self.unitv.shape)
    self.vm5 = contract_expression(self.mag_estr[2], self.unitv.shape, self.unitv.shape)


def vv_trans(self):
    """Calculate translation potential energy."""
    # unit: erg

    self.lind[:] = where(self.rij > self.limit, True, False)

    if True in self.lind:
        self.lj[:] = self.sigma * self.rij
        self.vt1(self.lj, self.lj, self.lj, self.lj, self.lj, self.lj, out=self.lj6)

        self.vt2(self.lind, self.epsilon * (1. + 4. * self.lj6 * (self.lj6 - 1.)), out=self.epot_tr)  # unit: erg


def vv_mag(self, mag, d):
    """Calculate magnetic potential energy."""
    self.vm1(self.rij, self.rij, self.rij, out=self.rij3)  # = self.rij * self.rij * self.rij

    self.vm2(d[self.triag_ind], self.rij, out=self.unitv)  # dimensionles

    self.vm3(mag[self.ia], self.unitv, out=self.magi)
    self.vm4(mag[self.ja], self.unitv, out=self.magj)
    self.vm5(mag[self.ia], mag[self.ja], out=self.dmag)

    return -(3. * self.magi * self.magj - self.dmag) * self.rij3  # potential energy unit: erg


def force_vars(self, d, dm):
    self.rij[:] = dm[self.triag_ind]  # unit: cm
    self.rijz[:] = d[self.triag_ind]

    self.fv1(self.rij, self.rij, self.rij, out=self.rij3)  # = self.rij * self.rij * self.rij
    self.rij4[:] = self.rij3 * self.rij  # = self.rij * self.rij * self.rij

    self.fv2(self.rijz, self.rij, out=self.unitv)  # dimensionles

    self.ff_trans()


def ff_trans(self):
    # translational force
    self.force_trans = self.zeros.copy()
    self.force_transR = self.force_trans.ravel()

    self.lind[:] = where(self.rij > self.limit, True, False)

    if True in self.lind:
        self.lj[:] = self.sigma * self.rij
        self.fv3(self.lj, self.lj, self.lj, self.lj, self.lj, self.lj, out=self.lj6)
        self.fv4(self.lind, self.epsilon24 * (2. * self.lj6 - 1.) * self.lj6, self.rijz, self.rij**2, out=self.ftrans)

        ftransR = self.ftrans.ravel()

        self.subtractat(self.force_transR, self.long_ia, ftransR)
        self.addat(self.force_transR, self.long_ja, ftransR)


def _ff_trans_return(self):
    """Calculate Translational Force."""
    return self.force_trans


def ff_mag(self, mag):
    """Calculate Magnetic force."""
    force = self.zeros.copy()   # unit: dine
    forceR = force.ravel()

    self.fm1(mag[self.ia], self.unitv, out=self.magi)
    self.fm2(mag[self.ja], self.unitv, out=self.magj)
    self.fm3(mag[self.ia], mag[self.ja], out=self.dmag)

    self.fm4(
        (self.fm5(5. * self.magi * self.magj - self.dmag, self.unitv) -
         self.fm6(self.magi, mag[self.ja]) -
         self.fm7(self.magj, mag[self.ia])), self.rij4, out=self.fmag)  # unit: dine

    self.fmag *= 3

    fmagR = self.fmag.ravel()

    self.subtractat(forceR, self.long_ia, fmagR)
    self.addat(forceR, self.long_ja, fmagR)

    return force


def hh_mag(self, mag):
    """Calculate Applied field."""
    hfield = self.zeros.copy()  # unit: oersted
    hfieldR = hfield.ravel()

    self.hm1(mag[self.ia], self.unitv, out=self.magi)
    self.hm2(mag[self.ja], self.unitv, out=self.magj)

    self.hm3(3 * self.hm4(self.magj, self.unitv) - mag[self.ja], self.rij3, out=self.hfia)  # unit: oersted
    self.hm5(3 * self.hm6(self.magi, self.unitv) - mag[self.ia], self.rij3, out=self.hfja)  # unit: oersted

    self.addat(hfieldR, self.long_ia, self.hfia.ravel())
    self.addat(hfieldR, self.long_ja, self.hfja.ravel())

    return hfield


def setup():
    import mango.position as mp
    mp.energy._setup_vvmag = _setup_vvmag
    mp.energy._setup_vvtrans = _setup_vvtrans

    def ewrap(f):
        def esetup(*args, **kw):
            f(*args, **kw)
            args[0]._setup_vvmag()
            args[0]._setup_vvtrans()
        return esetup

    mp.energy.__init__ = ewrap(mp.energy.__init__)

    mp.force._setup_forcevars = _setup_forcevars
    mp.force._setup_ffmag = _setup_ffmag
    mp.force._setup_hhmag = _setup_hhmag

    def fwrap(f):
        def fsetup(*args, **kw):
            f(*args, **kw)
            args[0]._setup_ffmag()
            args[0]._setup_hhmag()
            args[0]._setup_forcevars()
        return fsetup

    mp.force.__init__ = fwrap(mp.force.__init__)


setup()
