"""Lennard jones 6 -12 translational potential."""
from numpy import einsum, add, subtract, zeros

__all__ = ['vv_trans', 'ff_trans']


def vv_trans(self):
    """Lennard Jones Potential."""
    # unit: erg

    self.lj6[:] = (self.sigma * self.rij)**6
    self.epot_tr[:] = 4. * self.epsilon * self.lj6 * (self.lj6 - 1.)


def ff_trans(self):
    """Calculate Translational Force."""
    self.force_trans[:] = 0

    self.lj6[:] = (self.sigma * self.rij) ** 6
    einsum("i, ij, i-> ij",
           self.epsilon24 * (2. * self.lj6 - 1.) * self.lj6,
           self.rijz, self.rij * self.rij, out=self.ftrans)

    subtract.at(self.force_trans, self.ia, self.ftrans)
    add.at(self.force_trans, self.ja, self.ftrans)


def setup():
    import mango.position as mp
    mp.c.Error("M Lennard Jones potential run")


setup()
