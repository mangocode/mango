"""Original potential for use as a template for more complex examples."""
from numpy import where, sqrt
from numpy.core.multiarray import c_einsum

__all__ = ['vv_trans', 'vv_mag', 'ff_trans', 'ff_mag', 'hh_mag', 'energy_vars', 'force_vars']

ADVANCED = False


def deltamag(self, mag, dt, umag, hfield, _Heff):
    """Update magnetisation."""
    mag += self.mag

    c_einsum("iz,i->iz", mag, 1 / sqrt(c_einsum("iz,iz->i", mag, mag)), out=umag)  # dimensionless

    c_einsum("iz,i->iz", umag, self.Mmag, out=mag)  # unit: 1.e-12 emu

    # WARNING: The new mag is used just to compute hfield!
    hfield[:] = self.f.hh_mag(mag) + self.Hext  # unit: 1.e6 oersted

    # sLLG is remaining lines
    # WARNING: alpha is negative! Heff = hfield - _Heff
    c_einsum('i,xyz,ix,iy->iz', self.var.alpha, deltamag.eijk, umag, hfield, out=_Heff)  # unit: 1.e6 oersted

    return dt * c_einsum('i,xyz,ix,iy->iz', self.var.geff, deltamag.eijk, mag, hfield - _Heff)  # unit: 1.e-12 emu


def energy_vars(self, dm):
    """Set reused energy variables."""
    self.rij[:] = dm[self.triag_ind]  # unit: cm


def vv_trans(self):
    """Calculate translation potential energy."""
    # unit: erg

    self.lind[:] = where(self.rij > self.limit, True, False)

    if True in self.lind:
        self.lj[:] = self.sigma * self.rij
        c_einsum(self.tr_estr[0], self.lj, self.lj, self.lj, self.lj, self.lj, self.lj, out=self.lj6)
        c_einsum(self.tr_estr[1], self.lind, self.epsilon * (1. + 4. * self.lj6 * (self.lj6 - 1.)), out=self.epot_tr)  # unit: erg


def vv_mag(self, mag, d):
    """Calculate magnetic potential energy."""
    c_einsum(self.mag_estr[0], self.rij, self.rij, self.rij, out=self.rij3)  # = self.rij * self.rij * self.rij

    c_einsum(self.mag_estr[1], d[self.triag_ind], self.rij, out=self.unitv)  # dimensionles

    c_einsum(self.mag_estr[2], mag[self.ia], self.unitv, out=self.magi)
    c_einsum(self.mag_estr[2], mag[self.ja], self.unitv, out=self.magj)
    c_einsum(self.mag_estr[2], mag[self.ia], mag[self.ja], out=self.dmag)

    return -c_einsum("i ->", (3. * self.magi * self.magj - self.dmag) * self.rij3)  # potential energy unit: erg


def force_vars(self, d, dm):
    self.rij[:] = dm[self.triag_ind]  # unit: cm
    self.rijz[:] = d[self.triag_ind]

    c_einsum("i, i, i -> i", self.rij, self.rij, self.rij, out=self.rij3)  # = self.rij * self.rij * self.rij
    self.rij4[:] = self.rij3 * self.rij  # = self.rij * self.rij * self.rij

    c_einsum("ij,i -> ij", self.rijz, self.rij, out=self.unitv)  # dimensionles

    self.ff_trans()


def ff_trans(self):
    """
    Create variables for all forces.

    forces are updated twice without positional changes
    """
    # translational force
    self.force_trans = self.zeros.copy()
    self.force_transR = self.force_trans.ravel()

    self.lind[:] = where(self.rij > self.limit, True, False)

    if True in self.lind:
        self.lj[:] = self.sigma * self.rij
        c_einsum("i, i, i, i, i, i -> i", self.lj, self.lj, self.lj, self.lj, self.lj, self.lj, out=self.lj6)
        c_einsum("i, i, ij, i-> ij", self.lind, self.epsilon24 * (2. * self.lj6 - 1.) * self.lj6,
                 self.rijz, self.rij * self.rij, out=self.ftrans)

        ftransR = self.ftrans.ravel()

        self.subtractat(self.force_transR, self.long_ia, ftransR)
        self.addat(self.force_transR, self.long_ja, ftransR)


def _ff_trans_return(self):
    """Calculate Translational Force."""
    return self.force_trans


def ff_mag(self, mag):
    """Calculate Magnetic force."""
    force = self.zeros.copy()   # unit: dine
    forceR = force.ravel()

    c_einsum("iz,iz -> i", mag[self.ia], self.unitv, out=self.magi)
    c_einsum("iz,iz -> i", mag[self.ja], self.unitv, out=self.magj)
    c_einsum("iz,iz -> i", mag[self.ia], mag[self.ja], out=self.dmag)

    c_einsum("ij, i -> ij",
             (c_einsum("i, ij -> ij", 5. * self.magi * self.magj - self.dmag, self.unitv) -
              c_einsum("i, ij -> ij", self.magi, mag[self.ja]) -
              c_einsum("i, ij -> ij", self.magj, mag[self.ia])), self.rij4, out=self.fmag)  # unit: dine

    self.fmag *= 3.

    fmagR = self.fmag.ravel()

    self.subtractat(forceR, self.long_ia, fmagR)
    self.addat(forceR, self.long_ja, fmagR)

    return force


def hh_mag(self, mag):
    """Calculate Applied field."""
    hfield = self.zeros.copy()  # unit: oersted
    hfieldR = hfield.ravel()

    c_einsum("iz,iz -> i", mag[self.ia], self.unitv, out=self.magi)
    c_einsum("iz,iz -> i", mag[self.ja], self.unitv, out=self.magj)

    c_einsum("ij, i -> ij", 3. * c_einsum("i, ij -> ij", self.magj, self.unitv) -
             mag[self.ja], self.rij3, out=self.hfia)  # unit: oersted
    c_einsum("ij, i -> ij", 3. * c_einsum("i, ij -> ij", self.magi, self.unitv) -
             mag[self.ia], self.rij3, out=self.hfja)  # unit: oersted

    self.addat(hfieldR, self.long_ia, self.hfia.ravel())
    self.addat(hfieldR, self.long_ja, self.hfja.ravel())

    return hfield


def setup(ADVANCED):
    if ADVANCED:
        import mango.position as mp
        mp.c.posfuncs.append('deltamag')
        __all__.append('deltamag')
        deltamag.eijk = mp.c.eijk


setup()
