from numpy import einsum, triu_indices, zeros
from scipy.special import comb

from mango.constants import c, asciistring
from mango.position import Communal, energy
from mango.boundaries import periodic
from mango.debug import debug


@debug(['energy'])
def conserv(e, pos, mom, mag, dist_matrix, dist, mass):
    """Calculate the energy of the system and its CoM and CoM momentum."""
    # TODO
    # fix for proper array sizes (stats, multiple iterations)
    invmass = 1 / mass

    CoM = einsum("aij, i, i -> aj", pos, mass, invmass) / pos.shape[0]

    CoM_vel = einsum("aij, i -> aj", mom, invmass)

    einsum('xyz, aix, aiy -> az', c.eijk, pos - CoM[:, None], mom, out=e.angular)  # May fail due to CoM dimensions differ from pos

    einsum("aij -> aj", mag, out=e.tmag)  # unit: 1.e-12 emu

    e.tot[:] = e.angular + e.tmag / c.GBARE

    ekin_m = einsum("aij -> ai", 0.5 * einsum("aiz, i -> aiz", einsum("aiz, aiz -> aiz", mom, mom), invmass))

    ekin = einsum("ai -> a", ekin_m)

    e.energy_vars(dist_matrix)
    e.vv_trans()

    epot1_m = e.epot_tr
    epot2_m = e.vv_mag(mag, dist)

    dims = ''.join(asciistring(epot1_m.ndim))
    estr = '{} -> {}'.format(dims, dims[:-1])

    epot1 = einsum(estr, epot1_m)

    epot2 = einsum(estr, epot2_m)

    etot = ekin + epot1 + epot2

    return {"total_M": e.tot, "total_mag": e.tmag, "total_angular": e.angular,
            "kinetic": ekin, "trans_pot": epot1, "mag_pot": epot2,
            "total_E": etot, "CoM": CoM, "CoM_vel": CoM_vel,
            "ekin_pm": ekin_m, 'trans_pot_pm': epot1_m, 'mag_pot_pm': epot2_m}


def energy_calc(pos, mom, mag, epsilon, sigma, limit, mass):
    """Direct energy calculation."""
    nm = pos.shape[-2]
    tri = triu_indices(n=nm, k=1)
    cmb = (pos.shape[0], comb(nm, 2, exact=True))
    dist, dist_matrix = periodic().setup_full(pos)
    e = energy(Communal(epsilon, sigma, tri, cmb, limit), singleframe=False)
    e.set_attr(['tot', 'tmag', 'angular'], zeros((pos.shape[0], 3)))

    return conserv(e, pos, mom, mag, dist_matrix, dist, mass)


def get_eandm(reader, var, name=None):
    """Help function for getting energy and momenta data."""
    return energy_calc(reader.read('position', name)[:var.skip_iters],
                       reader.read("momentum", name)[:var.skip_iters],
                       reader.read("magnetisation", name)[:var.skip_iters],
                       var.epsilon, var.sigma, var.limit, var.mass)
