from scipy.ndimage.filters import gaussian_filter
from os import listdir
from numpy import (amax, einsum, stack, savetxt, triu_indices_from,
                   zeros_like, ones_like, sum as nsum, array, std)

from mango.constants import c
from mango.pp.util import getlocation, get_expon
from mango.debug import debug


def lengthcheckplot(mpl, fig_event, flg, directory, nm, run, timescale, radius, dmeandm, errdm, overallmean, overallerr):
    """
    Plot lengthcheck.

    Parameters
    ----------

    TODO

    """
    # if not flg.saveg or not flg.showg:
    #     return

    figure, PdfPages, *_ = mpl.prettyplot()

    fig = figure(figsize=(5, 6))
    ax = fig.add_subplot(212)
    ax2 = fig.add_subplot(211)
    lst = []
    timescale *= 1e-8
    ov = triu_indices_from(overallmean, k=1)

    for i in range(nm):
        for j in range(nm):
            if i < j:
                lst += [[j - i, "{} to {} mean distance: {:.3f} +- {:.3f}".format(i + 1, j + 1, overallmean[i, j], overallerr[i, j])]]
                ax.fill_between(timescale[0::100], dmeandm[0::100, i, j] - errdm[0::100, i, j],
                                dmeandm[0::100, i, j] + errdm[0::100, i, j], color='#cccccc', zorder=0)
                ax.scatter(timescale[0::100], dmeandm[0::100, i, j], 10, label=f"{i+1}to{j+1}", marker='x', lw=0.5)
                ax.plot(timescale, ones_like(timescale) * nsum(radius[i:j + 1] * 2) - radius[i] - radius[j], color='k', ls='--', lw=0.5, zorder=0)

    ax.legend()
    ax.set_xlabel("Time [1e-4s]")
    ax.set_ylabel("Mean Distance [1e-6cm]")
    ax.tick_params(axis='both', direction='in', which="both", width=0.5)

    ax2.scatter(ov[1] - ov[0], overallmean[ov], marker='x', lw=0.5)
    ax2.errorbar(ov[1] - ov[0], overallmean[ov], yerr=overallerr[ov], color='#cccccc', zorder=0, ls='')
    ax2.plot([1, nm - 1], [2, nsum(radius * 2) - radius[0] - radius[-1]], color='k', ls='--', lw=0.5)

    ax2.set_xlabel("Particle separation [No. Particles]")
    ax2.set_ylabel("Overall Mean Distance [1e-6cm]")
    ax2.tick_params(axis='both', direction='in', which="both", width=0.5)

    lst = array(lst)
    lst = lst[lst[:, 0].argsort()]
    for i in lst[:, 1]:
        print(i)

    fig.subplots_adjust(left=0.17, bottom=0.1, right=0.9, top=0.9, wspace=0.25, hspace=0.25)

    if flg.saveg:
        with PdfPages("{}{}_distance{}.pdf".format(directory, "{}Run{}".format('S_' if flg.suscep else '', run), "_ALIGNED" if flg.align else '')) as pdf:
            pdf.savefig(fig, dpi=1200, bbox='tight', transparent=True)

    if flg.showg:
        fig_event.connect(fig)


@debug(['plot'])
def plot_col(mpl, fig_event, flg, location, directory, run, stats, temp, no_mol):
    """
    Plot raw data.

    Arrays will always be 2D

    Parameters
    ----------
    mpl:
    fig_event:
    location: dict
        nested dictionary of locations, needs roots keys time, xyz, momenta and energy
    column: list
        list of plots
    directory: string
        directory to store plots
    run: int
        Run number
    stats: int
        Number of stastical runs
    no_mol: int
        number of moles

    """
    for plt in flg.column:

        xyname = (flg.column[plt]['x'], flg.column[plt]['y'])
        data = getlocation(xyname, location, stats)

        if xyname[1][:, 0][0].startswith(r"kinetic"):
            data[1][0] /= (3 / 2 * c.KB * temp * no_mol)
            if len(stats) > 1:
                data[1][2] = data[1][2] / (3 / 2 * c.KB * temp * no_mol)
            data[1][1] += r"/(unit KE)"

        plotter(mpl, fig_event, flg, data, xyname, stats, directory, run)

        # if xyname[1][:, 0][0].startswith(r"kinetic"):
        #     print(amax(data[1][0]), 3 / 2 * c.KB * temp * no_mol, (3 / 2 * c.KB * temp * no_mol) - amax(data[1][0]), "\n")


def plotter(mpl, fig_event, flg, xy, xyname, stats, directory, run, fignum=0):
    """
    The actual plotter and saving mechanism.

    TODO multiline plots
    """
    if not (flg.saveg or flg.showg):
        return

    x = xy[0][0]['F'].squeeze()
    y = xy[1][0].squeeze()

    xu = xy[0][1]
    yu = xy[1][1]

    (figure, savefig, handlermap) = mpl.col()

    fig = figure()
    ax = fig.add_subplot(111)

    ax.set_xlabel(r'{} {}'.format(''.join(xyname[0][:, 0]), xu))

    ax.set_ylabel(r'{} {}'.format(''.join(xyname[1][:, 0]), yu))

    ax.ticklabel_format(style='sci', axis='both', scilimits=(-1, 2))

    ydev = std(y, axis=0) if y.ndim >= 2 else None

    y = einsum('ij -> j', y) / y.shape[0] if len(y.shape) > 1 else y

    if x.shape != y.shape:
        c.Error("F Cannot plot {} {} against {} {} points".format(xyname[0][:, 0], x.shape, xyname[1][:, 0], y.shape))

    nmax = x.shape[0]
    ex_leg = []

    if nmax > 100:
        ex_leg += [ax.plot(x, y, color='#cccccc', zorder=2)[0]]
        if len(stats) > 1:
            std_leg = ax.fill_between(x, y - ydev, y + ydev, color='C7', zorder=1)
            ex_leg = [tuple([std_leg, ex_leg[0]])]
        y_filter = gaussian_filter(y, nmax * 0.01)
        ex_leg += [(ax.plot(x, y_filter, color='b', zorder=3)[0])]
    else:
        ex_leg += [(ax.plot(x, y, color='b')[0])]

    leg = ax.legend(ex_leg, ['Raw data', f'{int(nmax*0.01)} step moving average'] if nmax > 100 else [''], handler_map=handlermap)

    files = listdir(directory)
    while any(file.startswith("Run{:g}_Figure{:g}".format(run, fignum)) for file in files):
        fignum += 1

    figname = "{}Run{:g}_Figure{:g}.svg".format(directory, run, fignum)

    data = stack([x, y], axis=-1) if ydev is False or ydev is None else stack([x, y, ydev], axis=-1)

    if flg.saveg:
        savefig(figname, bbox_inches='tight')

    if flg.showg:
        fig_event.connect(fig, leg, ex_leg)

    savetxt(figname + ".dat", data)
