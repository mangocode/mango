from numpy import sum as nsum, amax, absolute, arange, zeros_like, mean, einsum, sqrt, var, conjugate, linalg
from math import pi

import mango.imports as imports
from mango.constants import c


def checker(ret, ret2, freq, freq2, time, time2):
    from numpy import allclose

    print(allclose(freq, freq2))
    print(allclose(time, time2))

    for i, j in ret.items():
        for k in j.keys():
            if ret[i][k].ndim > 1:
                a = allclose(ret2[i][k], nsum(ret[i][k], axis=-1))
            else:
                a = allclose(ret2[i][k], ret[i][k])
            if not a:
                print(i, k, a)
                # I would say the error of sum of the eigeinvalues (the trace)
                # is the sqrt of the sum of the squares of the errors of the single eigenvalues. See here:
                # https://en.wikipedia.org/wiki/Propagation_of_uncertainty#Simplification
                # if 'error' in k:
                #     tr = einsum("bi", ret2[i]['data'])
                #     ol = sqrt(einsum("i ->", ret[i][k]**2))
                diff = ret2[i][k] - nsum(ret[i][k], axis=-1)
                dmean = mean(absolute(diff))
                mdiff = amax(absolute(diff))
                datmean = mean(nsum(ret[i][k], axis=-1))
                dat2mean = mean(ret2[i][k])
                print(mdiff, dmean, datmean, dat2mean)
                if allclose(ret[i]['scale'], ret2[i]['scale']):
                    print(True)
                else:
                    print(ret[i]['scale'], ret2[i]['scale'])
                # try:
                #     print(1 / (amax(ret2[i][k]) / ret2[i]['scale']),
                #           (amax(ret2[i][k]) / ret2[i]['scale']))
                #     print(1 / (amax(nsum(ret[i][k], axis=-1) / ret[i]['scale'])))
                #     # import matplotlib.pyplot as plt
                #     # plt.semilogy(freq, nsum(ret[i][k], axis=-1) / ret[i]['scale'])
                #     # plt.semilogy(freq2, ret2[i][k] / ret2[i]['scale'], '-k')
                #     # plt.show()
                # except ValueError:
                #     pass
            else:
                print(a)


def autoold(Error, sus_data, dt, skip, mmag, blocks=1, ret_collect={}):

    fft, ffts, ifft = imports.fftw(Error)

    shape = sus_data[list(sus_data.keys())[0]].shape
    sample = shape[0] * shape[1] // blocks
    freq = 2.0 * pi * ffts.fftshift(ffts.fftfreq((2 * sample - 1), d=(dt * skip)))
    time = arange(-sample + 1, sample) * dt * skip
    norm_val = freq[1] - freq[0]

    def get_av_err(dummy, scale, blocks):
        ave = mean(dummy.real, axis=0)  # new uses whole of dummy not just real
        if dummy.shape[0] == 1:
            err = zeros_like(ave)
        else:
            err = sqrt(var(dummy, ddof=1, axis=0) / blocks)
        return scale * ffts.fftshift(ave), scale * ffts.fftshift(err)

    for autocorr_type in sus_data.keys():

        if autocorr_type in ['mag', 'vel']:
            # Subtract the average over the iterations
            ave = mean(sus_data[autocorr_type], axis=1)[:, None, :]
        else:
            ave = 0

        mxyz = einsum('ijkl->ijl', sus_data[autocorr_type] - ave)

        # Blocking
        mxyz = mxyz.reshape(-1, 3)[:sample * blocks].reshape((blocks, -1, 3))

        # Scale
        if autocorr_type is 'mag':  # 2*pi*<M*M>/3
            scale = 2. * pi * einsum("i, i -> ", mmag, mmag) / (3 * mmag.size)
        else:  # 2*pi*<v*v>/3
            scale = 2. * pi * einsum("ijk, ijk -> ", mxyz, mxyz) / mxyz.size

        ret_collect[autocorr_type] = {'scale': scale}

        # DOS
        dos = fft(mxyz, n=(2 * mxyz.shape[1] - 1), axis=1, threads=c.processors)

        # modulus of the data gives separate real and imaginary parts
        ret = einsum("bik, bik -> bi", dos, conjugate(dos))

        # Normalisation: Integral of DOS equal 1
        norm = norm_val * einsum('bi -> ', ret).real / blocks
        retpn = ret / norm

        # Normalisation: ACF(0) = 1
        ifftx = 2 * pi * ifft(ret, axis=1, threads=c.processors) / (dt * skip)

        # Error and averaging
        (ret_collect[autocorr_type]['data'],
         ret_collect[autocorr_type]['error']) = get_av_err(retpn, scale, blocks)
        (ret_collect[autocorr_type]['ifft'],
         ret_collect[autocorr_type]['ifft_error']) = get_av_err(ifftx, scale, blocks)

    return ret_collect, freq, time


def autocorr_calc_noclass(Error, sus_data, dt, skip, mmag, blocks=1, ret_collect={}):
    '''
    Calculates the autocorrelation function of each molecule to the external specified data

    Parameters
    ----------
    sus_data: dict
        dict of arrays of data of the particles
    dt: float
        timestep
    skip: float
        number of iterations skipped

    Returns
    -------
    ret_collect: dict
        The fft and ifft of the specified data averaged over stats and particles
    freq: array
        the frequencies shifted
    time: array
        timesteps (t0, t0+dt,..., t0+(dt*n))
    '''
    fft, ffts, ifft = imports.fftw(Error)

    shape = sus_data[list(sus_data.keys())[0]].shape
    sample = shape[0] * shape[1] // blocks
    freq = 2.0 * pi * ffts.fftshift(ffts.fftfreq((2 * sample - 1), d=(dt * skip)))
    time = arange(-sample + 1, sample) * dt * skip
    norm_val = freq[1] - freq[0]

    def get_av_err(dummy, scale, blocks):
        ave = scale * ffts.fftshift(mean(dummy, axis=0))
        if dummy.shape[0] == 1:
            err = zeros_like(ave)
        else:
            err = scale * ffts.fftshift(sqrt(var(dummy, ddof=1, axis=0) / blocks))
        return ave, err

    # Paralisable loop c.processors/numkeys
    for autocorr_type in sus_data.keys():

        if autocorr_type in ['mag', 'vel']:
            # Subtract the average over the iterations
            ave = mean(sus_data[autocorr_type], axis=1)[:, None, :]
        else:
            ave = 0

        # Sum over the number of atoms
        mxyz = einsum('ijkl->ijl', sus_data[autocorr_type] - ave).reshape(-1, 3)

        # Blocking
        # Flatten and refold -> Blocking will only trim from first trajetory
        mxyz = mxyz[:sample * blocks].reshape(blocks, -1, 3)

        # Scale
        if autocorr_type is 'mag':  # 2*pi*<M*M>/3
            scale = 2. * pi * einsum("i, i -> ", mmag, mmag) / (3. * mmag.size)
        else:  # 2*pi*<v*v>/3
            scale = 2. * pi * einsum("ijk, ijk -> ", mxyz, mxyz) / mxyz.size

        ret_collect[autocorr_type] = {'scale': scale}

        # DOS
        dos = fft(mxyz, n=(2 * mxyz.shape[1] - 1), axis=1, threads=c.processors)

        # modulus of the data gives separate real and imaginary parts
        mod_dos = einsum("bih, bik -> bihk", dos, conjugate(dos))
        # Calculate eigenvalues
        eig_dos = linalg.eigvalsh(mod_dos)

        # Normalisation: Integral of DOS equal 1
        norm = norm_val * einsum('bih -> ', eig_dos).real / blocks
        eig_dos_pn = eig_dos / norm

        # Normalisation: ACF(0) = 1
        ifftx = 2 * pi * ifft(eig_dos_pn, axis=1, threads=c.processors) / (dt * skip)

        # Error and averaging
        (ret_collect[autocorr_type]['data'],
         ret_collect[autocorr_type]['error']) = get_av_err(eig_dos_pn, scale, blocks)
        (ret_collect[autocorr_type]['ifft'],
         ret_collect[autocorr_type]['ifft_error']) = get_av_err(ifftx, scale, blocks)

    return ret_collect, freq, time
