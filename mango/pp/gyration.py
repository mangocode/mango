# from scipy.special import comb
from numpy import triu_indices, einsum, zeros, arange, add, linalg, random


def gyrationrad(no_molecules, position):

    # combin = comb(no_molecules, 2, exact=True)  # number of elements
    triu = triu_indices(n=3, k=1)
    tril = (triu[1], triu[0])
    mtriu = triu_indices(n=no_molecules, k=1)
    gyro = zeros((position.shape[0], 3, 3))
    factor = 1 / (2 * no_molecules**2)
    num = (factor * gyrotensor(position, mtriu, triu))
    for i in range(position.shape[0]):
        add.at(gyro, ([i, i, i], triu[0], triu[1]), num[i])
        add.at(gyro, ([i, i, i], tril[0], tril[1]), num[i])
    print(linalg.eigvalsh(gyro))


def gyrotensor(position, mtriu, triu):
    outer = position[:, :, triu[0]] * position[:, :, triu[1]]
    outer = einsum('ijk -> ik', outer[:, mtriu[0]] + outer[:, mtriu[1]])

    for i in range(len(triu[0])):
        inner = einsum('ij, ij -> i', -2 * position[:, mtriu[0], triu[0][i]], position[:, mtriu[1], triu[1][i]])
        outer[:, i] += inner
    return outer


def test(mol=2, iters=3):
    random.seed(12345)
    # posit = arange(iters*mol*3).reshape((iters, mol,3))

    posit = random.random((iters, mol, 3))

    gyrationrad(mol, posit)


test(2, 3)
