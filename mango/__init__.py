from mango.mango import _main_run
from mango.constants import c
from mango.io import inputregen
from mango.multiproc import ismpi

__all__ = ['run', 'help', 'mpi_run', 'inputregen']

__version__ = c.version


def run(opts):
    _main_run(False, opts)


def mpi_run(opts):
    ismpi(opts, False)


def help(arg='-h'):
    run(arg)
