import unittest
from unittest.mock import patch, mock_open, Mock, MagicMock

import pytest
import sys
from os import path

from mango.tests import captured_output

def imports():
    MOCK_MODULES = ['imageio', 'vispy',
                    'vispy.visuals.transforms', 'vispy.visuals.transforms._util',
                    'vispy.visuals.visual', 'vispy.visuals.mesh', 'vispy.scene',
                    'vispy.geometry']

    for mod_name in MOCK_MODULES:
        mocked = MagicMock()
        mocked.__iter__.return_value = [0]
        sys.modules[mod_name] = mocked


class vistest(unittest.TestCase):
    def setUp(self):
        imports()
        from mango.tools import visualiser
        self.vis = visualiser

    def test_read(self):

        with captured_output() as (out, err):
            ret_val = self.vis.read('{}/../5particles_lj_vis.xyz'.format(path.dirname(path.realpath(__file__))), 2)

        assert int(out.getvalue().split()[-1]) == 5
        assert ret_val['size'] == 25
        assert ret_val['no_mol'] == 5
        assert ret_val['box'] == 1
        for k in ['force', 'momentum', 'mag']:
            assert ret_val[k]
        assert ret_val['xyz_dump'].dtype.names == ('name', 'x', 'y', 'z',
                                                   'col0', 'col1', 'col2',
                                                   'col3', 'col4', 'col5',
                                                   'col6', 'col7', 'col8')
