import unittest
from unittest.mock import patch, Mock
from numpy import sqrt, sum as nsum, zeros, where

from mango.tests.numpy_assert import assertNumpyArraysEqual
from mango.tools.string_generation import r_unit, v_print, strings
from mango.tests import it_Mock, captured_output


class stringgen(unittest.TestCase):

    def test_strings(self):

        margv = it_Mock()
        rv = [['', '7.5', '5'], ['', '7.5', '5', '0', '0'], ['', '7.5%1.0', '2']]
        arr = zeros((5, 9))
        arr[0, 2] = -30.
        arr[1, 2] = -15.
        arr[4, 2] = 30.
        arr[3, 2] = 15.
        arr[:, 8] = 1
        arr2 = zeros((2, 9))
        arr2[0, 2] = -7.575
        arr2[1, 2] = 7.575
        arr2[:, 8] = 1
        larr = [arr, arr, arr2]
        for i in range(len(rv)):
            margv.return_value = rv[i]
            with patch('mango.tools.string_generation.savetxt') as mst:
                with patch('mango.tools.string_generation.argv', margv):
                    strings()

            data = mst.call_args

            self.assertEqual(data[0][0], 'string_{}r{}.xyz'.format(rv[i][2], rv[i][1]))
            assertNumpyArraysEqual(data[0][1], larr[i])
            dic = {'fmt': 'MNP {}'.format(9 * ' % .8e'),
                   'comments': '',
                   'header': ' {}\n # Generated with mango strings generator'.format(rv[i][2])}
            self.assertDictEqual(data[1], dic)

    def test_r_unit(self):
        self.assertEqual(sqrt(nsum(r_unit(30, 30)**2)), 1)

    def test_v_print(self):
        def rerun(b, m):
            test = v_print(b)
            self.assertEqual(str(test.print.__name__), '_print' if b else '_dont_print')
            with captured_output() as (out, err):
                test.print(m)
            if m != '': m += '\n'
            self.assertEqual(out.getvalue(), m)

        b = [True, False]
        m = ['hi', '']

        for i, j in zip(b, m):
            rerun(i, j)

    def test_help(self):
        margv = it_Mock()
        margv.return_value = ['', '-h']
        with captured_output() as (out, err):
            with self.assertRaises(SystemExit) as se:
                with patch('mango.tools.string_generation.argv', margv):
                    strings()
            self.assertEqual(se.exception.code, 0)


if __name__ == '__main__':
    unittest.main(verbosity=2)
