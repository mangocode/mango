import numpy as np
from mango.magnetic_motion import calculate
#from mango.pp.acf import autocorr_calc

import matplotlib.pyplot as plt
#import randomstate as rnd
import scipy.stats as spst


class flg():
    restart = False


class posit():
    pass


class var():
    nmax = 1  # number
    extra_iter = 1  # list of numbers (probably all 0)
    RandNoState = {}  # dict of random number states
    no_molecules = 1  # Number
    c2 = np.zeros((no_molecules))


def newinit(self, **argd):
    self.__dict__.update(argd)


def Error(arg):
    print(arg)


def getrandomstream(rng, calc, stat, store):
    for i in range(rng):
        point = i * 3
        calc._returnnewnoise()
        store[stat, point: point + 3, :, :] = calc.noise_setup
    return store


def randomness(blocking, store):
    for b in blocking:
        ret, freq, time = autocorr_calc(Error, sus_data={'store': store}, dt=1, skip=1, mmag=1, block=b)
        chi2, p, dof, ex = spst.chi2_contingency(ret['store']['data'])
        print(chi2, p, dof, ex)
        plt.figure()
        plt.plot(freq, ret['store']['data'][:, 0])
        plt.plot(freq, ret['store']['data'][:, 1])
        plt.plot(freq, ret['store']['data'][:, 2])


def main():
    calc_data = {'stat': 1,  # number
                 'pos': 1,  # list of numbers
                 'mag': 1,  # list of numbers
                 'mom': 1,  # list of numbers
                 'noise_setup': np.zeros((3, var.no_molecules, 3)),
                 'Hext': np.zeros((var.no_molecules, 3))}

    calc_data['flg'] = flg()
    calc_data['posit'] = posit()
    calc_data['var'] = var()

    calculate.__init__ = newinit
    calc = calculate(**calc_data)

    calc.newrandomnumbers()

    stlng = 1.8e6
    store = np.zeros((1, int(stlng), 1, 3))
    rng = int(stlng // 3)
    blocking = [5, 10, 20, 50, 100]

    randomness(blocking, getrandomstream(rng, calc, 0, store))

    flg.restart = True
    var.extra_iter = [0, 0, 0]
    state = rnd.get_state()
    var.RandNoState = {'stat0': state, 'stat1': state, 'stat2': state}
    var.skip = 1

    calc_data['flg'] = flg()
    calc_data['var'] = var()
    calc_data['pos'] = [1, 1, 1]  # list of numbers
    calc_data['mag'] = [1, 1, 1]  # list of numbers
    calc_data['mom'] = [1, 1, 1]  # list of numbers

    calc = calculate(**calc_data)

    store = np.zeros((3, int(stlng), 1, 3))
    for i in range(3):
        calc.stat = i
        calc.stname = f"stat{i}"
        calc.newrandomnumbers()
        store = getrandomstream(rng, calc, i, store)

    randomness(blocking, store)

    calc.pos = [1]  # list of numbers
    store = np.zeros((3, int(stlng), 1, 3))

    for i in range(3):
        calc.stat = i
        calc.newrandomnumbers()
        store = getrandomstream(rng, calc, i, store)

    randomness(blocking, store)

    plt.show()


if __name__ == '__main__':
    main()
