import unittest
from unittest import skip
from unittest.mock import patch, mock_open, Mock, call, MagicMock
from os import get_terminal_size
from io import StringIO
from contextlib import suppress, ExitStack
import sys
from numpy import array, arange, zeros, ones, block

from mango.constants import c
import  mango.managers as mm
from mango.errors import ExitQuiet, _strings
from mango.tests.numpy_assert import assertNumpyArraysEqual
from mango.tests import stEQ, xfail, platform, it_Mock, captured_output

from mango.io import (_input, file_write, write_data, read_data, restartfile_read, restart_gen,
                      read_inputfile, get_restartdata, txt_data, inputregen)


class functionio(unittest.TestCase):

    def setUp(self):
        c.Error = mm.start_server('Error')

    def tearDown(self):
        c.Error('END')

    def test__input(self):
        def s_e(*args):
            return args
        cols = Mock()
        cols.columns = 20
        with patch('mango.io.input') as m_in:
            with patch('mango.io.stdout') as m_out:
                with patch('mango.io.get_terminal_size', return_value=cols):
                    m_in.side_effect = s_e
                    m_out.write.side_effect = s_e
                    _input("Test:")
            # test
        self.assertEqual(m_out.write.call_args_list[0], call("\x1b[A\x1b[A"))
        self.assertEqual(m_out.write.call_args_list[1], call(" " * cols.columns))
        self.assertEqual(m_in.call_args_list[0], call("Test:"))

    @xfail(platform == "darwin",reason='NotImplementedError on Mac OSX because of broken sem_getvalue', raises=NotImplementedError)
    def test_read_inputfile(self):
        testkeys = {'ExternalFieldFreq': 4.0, 'BackgroundNoise': True, 'Optimisation': 'True',
                    'PositionFile': 'file', 'Repetitions': 5.0, 'SusceptibilityCalc': 'mag chi',
                    'PlotColumns': ['time', 'kinetic'], 'P': 'file'}
        outkeys = {'nu': 4.0, 'noise': True, 'opt': True, 'location': 'file',
                   'stats': 5, 'suscep': ['mag', 'chi'],
                   'column': ['time', 'kinetic'], 'restart': None}

        with self.assertRaises(ExitQuiet):
            read_inputfile(testkeys)
        self.assertEqual(stEQ.read()[0], "{}Input parameter 'P' doesn't exist\n\n".format(_strings._F[0]))

        del testkeys['P']
        args = read_inputfile(testkeys)
        outkeys['inputfile'] = testkeys
        self.assertDictEqual(args.__dict__, outkeys)

        string = ''
        for i, j in testkeys.items():
            if i == 'PlotColumns':
                j = f'{j[0]} {j[1]}'

            string += f'{i} {j}\n'

        string = '#hello\n\n' + string[:-1]

        testkeyfile = StringIO(string)
        with patch('mango.io.open', return_value=testkeyfile):
            args = read_inputfile('dummyname')
        outkeys['inputfile'] = 'dummyname'
        self.assertDictEqual(args.__dict__, outkeys)

    @xfail(platform == "darwin",reason='NotImplementedError on Mac OSX because of broken sem_getvalue', raises=NotImplementedError)
    def test_read_in_NF(self):

        with self.assertRaises(ExitQuiet):
            read_inputfile('dummy')
        self.assertEqual(stEQ.read()[0], "{}Input File not found\n\n".format(_strings._F[0]))

    def test_xyz(self):
        mo = mock_open()
        data = arange(0, 48).reshape(-1, 2, 12)
        flg = [[True, True, True, True], [False, False, False, False]]
        outfile = [[b'2 \ni=0 time=0.000000e+00[ps]', b' momentum=True mag=True force=True boxsize=10 \n',
                    b'2 \ni=1 time=1.000000e+00[ps]', b' momentum=True mag=True force=True boxsize=10 \n'],
                   [b'2 \ni=0 time=0.000000e+00[ps]', b' momentum=True mag=False force=True boxsize=False \n',
                    b'2 \ni=1 time=1.000000e+00[ps]', b' momentum=True mag=False force=True boxsize=False \n']]
        for i, f in enumerate(flg):
            with patch('mango.io.open', mo):
                self._file_write(data, 'X', f, 1, tdata=data[1], sdict={'fmt': 'MNP {}'.format(12 * ' % 15.7e')})
            mo.assert_has_calls([call('./here/{}Run1{}.m.xyz'.format('S_' if f[0] else '', 'ALIGN' if f[0] else ''), 'wb')])
            mo.assert_has_calls([call().write(x) for x in outfile[i]])
            mo.reset_mock()

    def test_M(self):
        data = arange(0, 30).reshape(-1, 15)
        flg = [[True, True], [False, False]]
        for f in flg:
            self._file_write(data, 'M', f, 0, './here/{}Run1Conservation_M.txt'.format('S_' if f[0] else ''),
                             block([block([[0, 1], [0, 1]]).T, data]),
                             {'fmt': '%.0d{}'.format(16 * ' % 15.7e'),
                              'header': 'itern. time total_m(x,y,z) mag_m(x,y,z) angular_m(x,y,z) CoM(x,y,z) CoM_vel(x,y,z)'})

    def test_E(self):
        data = arange(0, 8).reshape(-1, 4)
        flg = [[True, True], [False, False]]
        for f in flg:
            self._file_write(data, 'E', f, 0, './here/{}Run1Conservation_E.txt'.format('S_' if f[0] else ''),
                             block([block([[0, 1], [0, 1]]).T, data]),
                             {'fmt': '%.0d{}'.format(5 * ' % 15.7e'),
                              'header': 'itern. time kinetic_e pot_e1 pot_e2 total_e'})

    @xfail(platform == "darwin",reason='NotImplementedError on Mac OSX because of broken sem_getvalue', raises=NotImplementedError)
    def test_errs(self):
        class flg():
            suscep = False
            align = False
            lastframe = False
        errors = ['{}Complete Momenta file exists, skipping\n'.format(_strings._M[0]),
                  '{}Complete Energy file exists, skipping\n'.format(_strings._M[0]),
                  '{}Complete XYZ file exists, skipping\n'.format(_strings._M[0])]

        with patch('mango.io.path.isfile') as mp:
            mp.return_value = True
            for no, f in enumerate(['M', 'E', 'X']):
                file_write(arange(0, 8).reshape(-1, 4), f, {'F' if f in ['E', 'M'] else 'X': arange(0, 4)}, './here/', '1', flg, mmag='', boxsize='')
                self.assertEqual(stEQ.read()[0], errors[no])

    def _file_write(self, data, f, flg, i, name=None, tdata=None, sdict=None):  # (f, *args, **kwds):
        class flg():
            suscep = flg[0]
            mag_sw = flg[1]
            lastframe = False
            with suppress(IndexError):
                pbc = flg[2]
                align = flg[3]

        timescale = {'F' if f in ['E', 'M'] else 'X': arange(0, data.shape[i])}

        with patch('mango.io.savetxt') as mst:
            file_write(data, f, timescale, './here/', 1, flg, mmag=ones(data.shape[-2]), boxsize=10)
        d = mst.call_args
        if name is not None:
            self.assertEqual(d[0][0], name)
        assertNumpyArraysEqual(tdata, d[0][1])
        self.assertDictEqual(sdict, d[1])
        return mst

    def test_inputregen(self):
        filenames = ['', 'data.hdf5', 'inp']
        mo = mock_open()
        mrd = Mock()
        margv = it_Mock()

        inpfile = [f'{"ExternalFieldFreq":21s} 4.0\n', f'{"PositionFile":21s} file\n',
                   f'{"ParticleRadius":21s} 7. 7.\n', f'{"BackgroundNoise":21s} True\n',
                   f'{"Optimisation":21s} False\n']

        mrd.read.side_effect = self.inp_side_effect

        patches = [patch('mango.io.read_data', return_value=mrd),
                   patch('mango.io.open', mo),
                   patch('mango.io.argv', margv)]

        def recall(file=None, outfile=None):
            def err(*args):
                print(args)
            mm.serverwrapper = MagicMock()
            with patch("mango.io.c") as cpatch:
                cpatch.Error = err
                with ExitStack() as stack:
                    for i in patches:
                        stack.enter_context(i)
                    with captured_output() as (out, err):
                        self.inp_caller(file=file, outfile=outfile)

            if file is None and len(margv.return_value) < 2:
                assert out.getvalue().split("\n")[-2] == "('M Filename not provided, generating default inputfile',)"

            else:
                of = filenames[2] if len(margv.return_value) == 3 else outfile
                mo.assert_has_calls([call("Input" if outfile is None and len(margv.return_value) < 3 else of, 'w')])
                mo.assert_has_calls([call().write(x) for x in inpfile])
            mo.reset_mock()

        margv.return_value = filenames
        recall(outfile=filenames[2])
        recall()
        margv.return_value = filenames[:-1]
        recall(file='In.file')
        recall(filenames[1], filenames[2])
        margv.return_value = []
        recall()

    @staticmethod
    def inp_side_effect(val):
        if val == 'flags':
            return {'noise': True, 'opt': False, 'run': 1}
        if val == 'vars':
            return {'nu': 4.0, 'location': 'file', 'radius': array([7.0, 7.0])}

    @staticmethod
    def inp_caller(*args, **kw):
        if args is None:
            inputregen()
        else:
            inputregen(*args, **kw)

    def test_getrdata(self):
        class restart():
            mag = [3, 3, 3]
            mom = [4, 4, 4]
            pos = [5, 5, 5]
            RandNoState = {"hi": 1}
        returndict = {'mag': array([[3, 3, 3],
                                    [3, 3, 3],
                                    [3, 3, 3]]),
                      'mom': array([[4, 4, 4],
                                    [4, 4, 4],
                                    [4, 4, 4]]),
                      'pos': array([[5, 5, 5],
                                    [5, 5, 5],
                                    [5, 5, 5]]),
                      'RandNoState': {'hi': 1}}

        rflgs = None
        last_iter = None
        filenames = [".1.hi", ".2.hi", ".3.hi"]
        mockrdata = Mock()
        mockrdata.read.return_value = restart(), rflgs, last_iter
        r, rf, li = get_restartdata(mockrdata, filenames)
        for no, i in enumerate(returndict):
            if i != 'RandNoState':
                print(r.__dict__[i][no], returndict[i][no])
                assertNumpyArraysEqual(r.__dict__[i][no], returndict[i][no])
            else:
                self.assertDictEqual(r.__dict__[i], returndict[i])
        self.assertEqual(rf, None)

    def test_restartfile_read(self):
        restartfile = StringIO('''
            save_type hdf5
            directory /file/location/
            total_tosave 0
            Run14_mol-5.3.hdf5 0
            Run14_mol-5.2.hdf5 0
            Run14_mol-5.4.hdf5 0
            Run14_mol-5.1.hdf5 0
            Run14_mol-5.5.hdf5 0''')
        files = ['/file/location/Run14_mol-5.3.hdf5', '/file/location/Run14_mol-5.2.hdf5',
                 '/file/location/Run14_mol-5.4.hdf5', '/file/location/Run14_mol-5.1.hdf5',
                 '/file/location/Run14_mol-5.5.hdf5']
        files2 = ['dummyname/Run14_mol-5.3.hdf5', 'dummyname/Run14_mol-5.2.hdf5',
                  'dummyname/Run14_mol-5.4.hdf5', 'dummyname/Run14_mol-5.1.hdf5',
                  'dummyname/Run14_mol-5.5.hdf5']
        total_tosave = 0
        with patch('mango.io.open', return_value=restartfile, create=True):
            file_out, save_type, directory, total = restartfile_read('dummyname')
        try:
            self.assertEqual(files, file_out)
        except AssertionError:
            self.assertEqual(files2, file_out)
        self.assertEqual(save_type, 'hdf5')
        try:
            self.assertEqual(directory, '/file/location/')
        except AssertionError:
            self.assertEqual(directory, 'dummyname/')
        self.assertEqual(total, total_tosave)

    def test_restart_gen(self):
        mo = mock_open()
        restartfile = ['directory /file/location\n', 'file.1.hdf5 \n',
                       'file.2.hdf5 \n', 'file.3.hdf5 \n', 'file.4.hdf5 \n',
                       'file.5.hdf5 \n']

        def recall(file):
            with captured_output() as (out, err):
                with patch('mango.io.open', mo):
                    with patch('mango.io.read_data', return_value=MagicMock(), create=True):
                        with patch("mango.io.getcwd", return_value="HERE"):
                            restart_gen(file, 5, 'dummyname')

        recall("/file/location/file.1.hdf5")
        mo.assert_has_calls([call("dummyname", "w")])
        mo.assert_has_calls([call().write(restartfile[0])])
        mo.assert_has_calls([call().write(x) for x in restartfile[1:]])
        mo.reset_mock()
        recall("./file.1.hdf5")
        mo.assert_has_calls([call("dummyname", "w")])
        mo.assert_has_calls([call().write(restartfile[0].split()[0] + " HERE\n")])
        mo.assert_has_calls([call().write(x) for x in restartfile[1:]])
        mo.reset_mock()
        recall("/file.1.hdf5")
        mo.assert_has_calls([call("dummyname", "w")])
        mo.assert_has_calls([call().write(restartfile[0].split()[0] + " /\n")])
        mo.assert_has_calls([call().write(x) for x in restartfile[1:]])

    @skip('Unfinished test')
    def test_getvar(self):  # (f, *args, **kwds):
        pass
        # return f(*args, **kwds)

    def test_txt_data(self):  # (f, *args, **kwds):
        mo = mock_open()
        no_mol = 5
        iters = 5
        vkeys = [['radius', 'dens', 'ms', 'chi0', 'tauB', 'theta', 'phi'], ['vol'], ['alpha', 'geff', 'c2'],
                 ['no_mol', 't0', 'dt', 'nmax', 'skip', 'stats', 'temp',
                  'H_0', 'nu', 'eta', 'boxsize'],
                 ['tauN', 'keff', 'nu_0']]

        arrkeys = ['magnetisation', 'position', 'forces', 'momentum']
        data_dict = {'vars': {i: zeros(no_mol) for v in vkeys[:-2] for i in v}}
        data_dict = {**data_dict, **{i: zeros((iters, no_mol, 3)) for i in arrkeys}, 'iter_time': zeros((iters, 2))}

        data_dict['vars'] = {**data_dict['vars'], **{i: 0 for i in vkeys[-2]}}
        data_dict['name'] = 'test'
        data_dict['neel_relaxation'] = {f"mol_{i}": {it: 1 for it in range(iters)} for i in range(no_mol)}

        with patch("mango.io.open", mo):
            with patch("mango.io.rmemptyfile"):
                with patch("mango.io.path.isfile") as pisf:
                    for i in [True, False]:
                        for n in [True, False]:

                            arrstr = '\n'.join([''.join([f"mol_{i} 0. 0. {1 if n else 0}{' 0.' * 12}\n"
                                                         for i in range(no_mol)]) for j in range(iters)]) + "\n"

                            pisf.return_value = i

                            if n:
                                data_dict['vars'] = {**data_dict['vars'], **{i: 0 for i in vkeys[-1]}}

                            txt_data(data_dict, no_mol)

                            if n:
                                [data_dict['vars'].pop(k, None) for k in vkeys[-1]]

                            if i:
                                mo.assert_has_calls([call().write(arrstr)])
                            else:
                                self.vkeys_check(mo, vkeys if n else vkeys[:-1])
                            mo.reset_mock()

    @staticmethod
    def vkeys_check(mo, vkeys):
        for v in vkeys:
            for i in v:
                assert i in mo().write.call_args_list[0][0][0]


class Write(unittest.TestCase):

    def setUp(self):
        c.Error = mm.start_server('Error')

    def tearDown(self):
        c.Error('END')

    def test_write(self):
        mo = mock_open()

        def wr(self, *args, **kw):
            return args, kw

        with patch('mango.io.open', mo):
            with patch('mango.io.imports') as imp:
                for i in ['hdf5', 'pkl']:
                    imp.write.return_value = (wr, i)
                    test = self.writer('', {'/hi/t0': 0, '/hi/t1': 1})
                    test.write({'name': '/hi/t2'})
                    test.write({'name': '/hi/t1'})
                    test.write({'name': '/hi/t0'})
                    self.assertDictEqual(test.filenames, {'/hi/t0': 0, '/hi/t1': 1})

    def writer(self, s_type, filenames):
        class var:
            skip_iters = 10
            no_molecules = 5
            name = '/hi/testRun1_'

        class flg:
            save_type = s_type
            restart = False

        test = write_data(var, flg)
        test.filenames = filenames
        test.setup(list(filenames.keys())[0])
        return test

    def test_restartfile(self):
        mo = mock_open()
        outfile = ['save_type hdf5\ndirectory /hi/\ntotal_tosave 11\n', 't0\n', 't1\n']
        with patch('mango.io.open', mo):
            test = self.writer('hdf5', {'/hi/t0': 0})
            test.addtorestart('/hi/t1')

        mo.assert_has_calls([call('/hi/Run1restart', 'a')])
        mo.assert_has_calls([call('/hi/Run1restart', 'w')])
        file_handle = mo.return_value.__enter__.return_value
        file_handle.write.assert_has_calls([call(x) for x in outfile], any_order=True)
        mo.reset_mock()


class Read(unittest.TestCase):

    def setUp(self):
        c.Error = mm.start_server('Error')

    def tearDown(self):
        c.Error('END')

    def test__readtype(self):
        name = 'test'
        read_type = ['_hdf5read', '_pickleread', 'txtread']
        for no, i in enumerate(['hdf5', 'pkl', 'txt']):
            read = read_data(name, i)

            self.assertEqual(str(read._rd.__name__), read_type[no])
            self.assertEqual(read.fname, "test." + i)

    def test__hdf5read(self, name='test'):
        def load(self, *args):
            return args
        with patch('mango.io.imports') as imp:
            imp.read.return_value = (load, 'hdf5')
            test = read_data(name, 'hdf5')
            data = test._hdf5read('here')
        self.assertEqual(data[-2:], ('here',))
        self.assertEqual(test.fname, f'{name}.hdf5')

    def test__pickleread(self, name='test'):
        def load(*args):
            return {'here': name}

        def load2(*args):
            return {'right': {'here': name}}
        mo = mock_open()

        self.num = -1

        def tell():
            self.num += 1
            return self.num

        def rerun(load, loc):
            with patch('mango.io.imports') as imp:
                with patch('mango.io.open', mo):
                    with patch("mango.io.fstat") as miof:
                        mo().tell = tell
                        miof().st_size = 1
                        imp.read.return_value = (load, 'pkl')
                        test = read_data('pkl', name)
                        data = test._pickleread(loc)
            self.num = -1
            self.assertEqual(data, ['test'])

        rerun(load, 'here')
        rerun(load2, 'right/here')

    def test_read(self):

        test = read_data('test', 'hdf5')

        def side_effect(*args):
            return args

        def load(self, *args):
            return args
        rd = Mock()
        rd.side_effect = side_effect
        test._rd = rd
        test.load = load

        args = test.read('here')

        self.assertEqual(args, ('here', None, False))

        mo = mock_open()

        with patch('mango.io.imports') as imp:
            with patch('mango.io.open', mo):
                imp.read.return_value = (load, 'hdf5')
                test2 = read_data('test', 'hdf5')
                args = test2.read('here', 'new.hdf5')
                self.assertEqual(args, ('here', None, False))
                self.assertEqual(test2.fname, 'new.hdf5')

    def test_readrestart(self):
        one = arange(27).reshape((3, 3, 3))
        two = one.copy()
        two[2, :, :] = 0
        rdat = (one, two, zeros((3, 3, 3)))
        outdat = (arange(18, 27).reshape((3, 3)),
                  arange(9, 18).reshape((3, 3)), zeros((3, 3)))

        def rd_sideeffect(self, *args):
            data = ['position', 'magnetisation', 'momentum']
            if args[0] in data:
                return dat
            elif args[0] == 'vars':
                return {'hi': 'hi', 'nmax': 'nmax'}
            elif args[0] == 'flags':
                return {'hi': 'hi', 'nmax': 'nmax'}

        with patch.object(read_data, '_hdf5read', rd_sideeffect):
            test = read_data('test', 'hdf5')
            for i, dat in enumerate(rdat):
                r, rf, li = test.read(fname='test', restart=True)
                assertNumpyArraysEqual(r.pos, r.mag)
                assertNumpyArraysEqual(r.pos, r.mom)
                assertNumpyArraysEqual(r.pos, outdat[i])
                self.assertDictEqual(rf.__dict__, {'hi': 'hi', 'nmax': 'nmax'})
                assert {'hi': 'hi'}.items() <= r.__dict__.items()

    @skip('Unfinished test')
    def test_txtread(self):  # (f, *args, **kwds):
        pass
        # return f(*args, **kwds)


if __name__ == '__main__':
    unittest.main(verbosity=2)
