import unittest
from time import time
from mango.time import _time, grace, start, end


class timings(unittest.TestCase):

    def test_startend(self):
        wall = 10
        timer = end(start(wall))
        t1 = timer.gettimeleft()
        self.assertEqual(timer.walltime, wall)
        self.assertGreater(timer.finishtime, time())
        self.assertEqual(timer.finished(), 'Completed in 0.00 seconds')
        t2 = timer.gettimeleft()
        self.assertGreater(t1, t2)

    def test_time(self):
        t1 = _time(dt=1, t0=0)
        t2 = _time(dt=1, t0=1)
        t1.time_update()
        t2.time_update()
        self.assertEqual(t1.time, 1)
        self.assertEqual(t1.iter, 1)
        self.assertEqual(t1.time, t2.time - 1)
        self.assertEqual(t1.iter, t2.iter)

    def test_grace(self):
        self.assertEqual(grace([1]).time, 120)
        self.assertEqual(grace(list(range(20))).time, 200)


if __name__ == '__main__':
    unittest.main(verbosity=2)
