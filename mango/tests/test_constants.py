import unittest
from unittest import skip
from unittest.mock import Mock

import sys
import os

from numpy import triu_indices, zeros, ravel, array, allclose, random, add
from scipy.special import comb
from collections import OrderedDict
from contextlib import suppress

from mango.constants import getkeywords, nestedDict, _variables, switch, c, const, rmemptyfile, bin_count
from mango.tests import captured_output

import pytest

class constants(unittest.TestCase):

    # @pytest.fixture(autouse=True)
    # def inject_fixtures(self, capfd): # capsys):
    #     # self._capsys = capsys
    #     self._capfd = capfd

    def tearDown(self):
        with suppress(AttributeError, FileNotFoundError):
            os.remove(self.file)

    def test_c(self):
        with captured_output() as (out, err):
            c._banner = Mock()
            c._banner
            c.header(True, True)
            output = out.getvalue()
            p = printed()
            # HOw to force a specific case?
            for j in ['redirected', 'to screen']:
                for k in ['redirected', 'to screen']:
                    for l in [['', ''], ['not ', "'t"]]:
                        for m in [['', ''], ['not ', "'t"]]:
                            with suppress(AssertionError):
                                self.assertEqual(p.format(j, k, l, m), output)

            # try:
            #     self.assertEqual(p.format('redirected', '', ''), output[11:])
            # except AssertionError:
            #     try:
            #         self.assertEqual(p.format('to screen', '', ''), output[11:])
            #     except AssertionError:
            #         try:
            #             self.assertEqual(p.format('redirected', 'not ', "'t"), output[11:])
            #         except AssertionError:
            #             self.assertEqual(p.format('to screen', 'not ', "'t"), output[11:])

        with captured_output() as (out, err):
            c.header(False, False)
        output = out.getvalue()
        assert output, '\x1b[H\x1b[J\x1b[1d\n'

    def test_const_profile(self):
        sys.argv += ['--profile']
        testc = const()
        self.assertEqual(testc.profile, True)

    def test_allowed2plot(self):
        try:
            s_display = os.environ['DISPLAY']
            del os.environ['DISPLAY']
        except KeyError:
            s_display = None
        testc = const()

        if s_display:
            os.environ['DISPLAY'] = s_display
            testc = const()
            self.assertEqual(testc.havedisplay, True)
        else:
            try:
                # May fail here dependant on whether mpl imports or not
                self.assertEqual(testc.havedisplay, False)
            except AssertionError:
                print("TODO force fail")

    def test_rmfile(self):
        i = 0
        self.file = f'file{i}'
        while os.path.isfile(self.file):
            i += 1
            self.file = f'file{i}'
        with open(self.file, 'w') as w:
            w.write("hi")
        rmemptyfile(self.file)
        self.assertEqual(os.path.isfile(self.file), True)
        open(self.file, 'w').close()
        rmemptyfile(self.file)
        self.assertEqual(os.path.isfile(self.file), False)

    @skip('Unfinished test')
    def test_processors(self):
        pass

    def test_nestedDict(self):
        nd = nestedDict()
        nd["a"]["b"] = "c"
        nnd = {"a": {"b": "c"}}
        self.assertEqual(nd, nnd)

    def test_defaults(self):

        self.assertEqual(type(getkeywords().defaults), dict)

    def test_keywords(self):
        self.assertEqual(type(getkeywords().words), OrderedDict)

    def test_switchandvariables(self):  # (f, *args, **kwds):
        var_d = {"sw": False}
        self.flg = _variables(**var_d)
        self.assertEqual(self.flg.__dict__["sw"], var_d["sw"])
        self.assertEqual(self.switchoff(), None)
        self.flg.sw = True
        self.assertEqual(self.switchoff(), "func_on")

    @switch("sw")
    def switchoff(self):
        return "func_on"


class bincount(unittest.TestCase):

    def setUp(self):
        no = 10
        leng = 30

        self.bc = bin_count(**{'leng': leng, "shape": (no, 3)})
        self.tri = triu_indices(n=no, k=1)

        self.to = zeros((no, 3))
        self.fro = random.random((comb(no, 2, exact=True), 3))

        self.longtri = (ravel(array([self.tri[0] * 3, self.tri[0] * 3 + 1, self.tri[0] * 3 + 2]), 'F'),
                        ravel(array([self.tri[1] * 3, self.tri[1] * 3 + 1, self.tri[1] * 3 + 2]), 'F'))

    def test_bincount(self):
        bcto, bcfro = self.to.copy(), self.fro.copy()

        self.bc.addat(bcto.ravel(), self.longtri[0], bcfro.ravel())
        add.at(self.to, self.tri[0], self.fro)

        assert allclose(bcto, self.to)

        self.bc.addat(bcto.ravel(), self.longtri[1], bcfro.ravel())
        add.at(self.to, self.tri[1], self.fro)

        assert allclose(bcto, self.to)


def printed():
    return """Constants
---------
KB       1.3806485e-10 \u03BCerg/K
GBARE   -1.7608596e+01 1/(gauss*ps)
EPS2     2.2204460e-16 (float64 precision)
EPS      1.1920929e-07 (float32 precision)

Running on posix
stdout {}
stderr {}
Display {}available graphs can{} be viewed onscreen

"""


if __name__ == '__main__':
    unittest.main(verbosity=2)
