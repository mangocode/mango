import unittest
from unittest import skip
from unittest.mock import MagicMock, PropertyMock, sentinel, Mock, patch
from numpy import array
from functools import partial
from mango.tests import captured_output, stEQ, xfail, platform
from mango.hdf5io import _save_type, _append_check
from mango.managers import start_server
from mango.constants import c
from mango.errors import _strings

class hdf5(unittest.TestCase):

    def setUp(self):
        c.Error = start_server('Error')

    def tearDown(self):
        c.Error('END')

    @skip('Unfinished test')
    def test_save(self):  # (f, *args, **kwds):
        pass
        # return f(*args, **kwds)

    @xfail(platform == "darwin",reason='NotImplementedError on Mac OSX because of broken sem_getvalue', raises=NotImplementedError)
    def test_save_type(self):  # (f, *args, **kwds):
        file = Mock()

        location = "./here"
        with patch("mango.hdf5io.backupcheck"):
            with patch("mango.hdf5io._save_ltd") as hdfpatch:
                for data in [{'test': 1}, [1, 2, 3], (1, 2, 3)]:
                    _save_type(file, location, data, 'test', filters=None, datalength=None, append=None)
                    assert hdfpatch.call_args[1:][0]['ltd'] == str(type(data)).split("'")[-2]

        with patch("mango.hdf5io.backupcheck"):
            with patch("mango.hdf5io._save_numpy") as hdfpatch:
                _save_type(file, location, array([1, 2, 3]), 'test', filters=None, datalength=None, append=None)
                assert hdfpatch.called

        location = Mock()
        with patch("mango.hdf5io.backupcheck"):
            _save_type(file, location, 10, 'test', filters=None, datalength=None, append=None)
        assert "test" in dir(location._v_attrs)
        assert location._v_attrs.test == 10

        class testclass():
            pass

        error = ["{}Saving {}{} not yet implemented, sorry\n".format(_strings._W[0], 'test', type(testclass()))]
        with patch("mango.hdf5io.backupcheck"):
            _save_type(file, location, testclass(), 'test', filters=None, datalength=None, append=None)

        self.assertEqual(stEQ.read()[0], error[0])

    @skip('Unfinished test')
    def test_save_numpy(self):  # (f, *args, **kwds):
        pass
        # return f(*args, **kwds)

    @skip('Unfinished test')
    def test_save_ltd(self):  # (f, *args, **kwds):
        pass

    @skip('Unfinished test')
    def test_append_check(self):
        pass
        # node = MagicMock()
        # n=node
        # for i in ['v3', 'v2']:
        #     print(i)
        #     type(n)._v_parent = PropertyMock(return_value=getattr(sentinel, i))
        #     n = getattr(n, '_v_parent')
        # node._v_parent._v_parent._v_parent = PropertyMock(return_value=getattr(sentinel, 'v1'))
        # print(node._v_parent, dir(node._v_parent._v_parent), node._v_parent._v_parent._v_parent, dir(node))
        # self.assertEqual(_append_check(node, 'test', set(['test'])), True)

    @skip('Unfinished test')
    def test_load(self):  # (f, *args, **kwds):
        pass
        # return f(*args, **kwds)

    @skip('Unfinished test')
    def test_load_type(self):  # (f, *args, **kwds):
        pass
        # return f(*args, **kwds)


if __name__ == '__main__':
    unittest.main(verbosity=2)
