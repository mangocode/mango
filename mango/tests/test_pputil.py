import unittest
from unittest import skip
from unittest.mock import Mock, patch
from numpy import zeros, array

from mango.pp.util import get_expon, getvars, at_least2d_end, getlocation, showgraph, onevent, loader #plotter
from mango.tests.numpy_assert import assertNumpyArraysEqual
from mango.tests import stEQ, xfail, platform

from mango.managers import start_server
from mango.imports import matplotlib
from mango.errors import _strings
from mango.constants import c

class functionpost(unittest.TestCase):

    def setUp(self):
        c.Error = start_server('Error')

    def tearDown(self):
        c.Error('END')

    # moved to plotting
    #def test_debye(self):
    #    self.assertEqual(debye.chi(1, 1, 1), 0.5)

    def test_getvar(self):
        varib = {'hi': 'lo'}
        lst = ['hi']
        self.assertEqual(getvars(varib, lst).hi , 'lo')

    def test_getexpon(self):
        self.assertEqual(get_expon(5e5), 5)
        self.assertEqual(get_expon(5e-5), -5)

    def test_atleast2d_end(self):
        test = (array([[1, 1], [1, 1], [1, 1]]),
                array([[1, 1], [1, 1]]), array([1, 1]), array([1]))

        for i in test:
            t = at_least2d_end(i)
            assert t.ndim >= 2

        t = at_least2d_end(*test)
        for i in t:
            assert i.ndim >= 2

    def test_getlocation(self):  # (f, *args, **kwds):
        test = zeros((10, 10, 3))
        location = {'xyz': {'position': test},
                    'momenta': {"CoM": test + 1},
                    'energy': {'kinetic': test + 2},
                    'time': test + 3}
        self.col_out = ({"plot_1": {"x": array([["time", True]]), "y": array([["kinetic", "[:,2]"],
                                                                              ["+", False],
                                                                              ["kinetic", "[:,2]"],
                                                                              ["/", False],
                                                                              ["-", False],
                                                                              ["kinetic", "[:,2]"]])}},
                        {"plot_1": {"x": array([["time", True]]), "y": array([["position", "[5,:,2]"],
                                                                              ["+", False],
                                                                              ["CoM", "[5,:,2]"],
                                                                              ["/", False],
                                                                              ["-", False],
                                                                              ["kinetic", "[5,:,2]"]])}},)
        # {"plot_1": {"x": array([["time", True]]), "y": array([["position", "[5,:,2]"],
        #                                                       ["+", False],
        #                                                       ["CoM", "[5,:,2]"],
        #                                                       ["/", False],
        #                                                       ["-", False],
        #                                                       ["position", "[5,:,2]"]])}},)

        a = test[1, :, 2]
        b = test[5, :, 2]
        yaxis = ((a + 2) + (a + 2) / - (a + 2), (b) + (b + 1) / - (b + 2))

        answers = (
            ((test + 3, '(ps)', False), (yaxis[0], '($\\mu$ erg)+($\\mu$ erg)/-($\\mu$ erg)', zeros(10))),
            ((test + 3, '(ps)', False), (yaxis[1], '(nm)+(nm)/-($\\mu$ erg)', False)),
        )
        # {"plot_1": {"x": array([["time", True]]), "y": array([["kinetic", "[5,:,3]"],
        #                                                       ["+", False],
        #                                                       ["trans_pot", "[:,1,1]"],
        #                                                       ["/", False],
        #                                                       ["-", False],
        #                                                       ["mag_pot", True]])}},
        # {"plot_1": {"x": array([["time", True]]), "y": array([["kinetic", True],
        #                                                       ["+", False],
        #                                                       ["trans_pot", "[:,1,1]"],
        #                                                       ["/", False],
        #                                                       ["-", False],
        #                                                       ["mag_pot", True]])}},
        # {"plot_1": {"x": array([["time", True]]), "y": array([["kinetic", True],
        #                                                       ["+", False],
        #                                                       ["trans_pot", "[:,1,1]"],
        #                                                       ["/", False],
        #                                                       ["-", False],
        #                                                       ["mag_pot", "[:100]"]])}},
        # {"plot_1": {"x": array([["time", True]]), "y": array([["mag_pot", "[:100]"]])}},
        # {"plot_1": {"x": array([["time", True]]), "y": array([["mag_pot", True]])}})
        for no, plots in enumerate(self.col_out):
            for i in plots:
                j = plots[i]
                loc = getlocation((j['x'], j['y']), location, 10)
                self.assertEqual(loc[0][1], answers[no][0][1])
                self.assertEqual(loc[1][1], answers[no][1][1])
                print(j, loc[1][0], answers[no][1][0])
                #try:
                #    self.assertEqual(loc[1][2], answers[no][1][2])
                #except ValueError:
                #    assertNumpyArraysEqual(loc[1][2], answers[no][1][2])

                assertNumpyArraysEqual(loc[0][0], answers[no][0][0])
                assertNumpyArraysEqual(loc[1][0][:, 0] if len(loc[1][0].shape) == 2 else loc[1][0], answers[no][1][0])

    @skip('Unfinished test')
    def test_interactive_getfiles(self):  # (f, *args, **kwds):
        pass
        # return f(*args, **kwds)

    def test_loader(self):
        d = './'
        r = 1
        class flg():
            suscep = True
            align = True
        with patch('mango.pp.util.load', return_value=None) as load:
            lfile = loader(flg, r, d)
            lfile.lengthcheck(5)

        load.assert_called_with("./S_Run1_distance_arrays_5_ALIGNED.npz")

    @skip('Unfinished test')
    def test_dataarrays(self):  # (f, *args, **kwds):
        pass

    @xfail(platform == "darwin",reason='NotImplementedError on Mac OSX because of broken sem_getvalue', raises=NotImplementedError)
    def test_showgraph(self):
        errors = ['{}>No Graph to Show\n'.format(_strings._M[0]),
                  '{}Press Enter to close all graphs,\n'.format(_strings._M[0]) +
                  '{}Space to close current graph\n'.format(_strings.tab + _strings._M[1] * ' ') +
                  '{}Click on legend to show/hide lines\n'.format(_strings.tab + _strings._M[1] * ' ')]

        mpl = Mock()
        mpl.showg.return_value = (lambda: 1, lambda: [])

        showgraph(mpl, showg=True)
        self.assertEqual(stEQ.read()[0], errors[0])
        mpl.showg.return_value = (lambda: 1, lambda: ['hi'])
        showgraph(mpl, showg=True)
        self.assertEqual(stEQ.read()[0], errors[1])

    @skip('Unfinished test')
    def test_onevent(self):  # (f, *args, **kwds):
        pass
        # return f(*args, **kwds)


if __name__ == '__main__':
    unittest.main(verbosity=2)
