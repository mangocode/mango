import unittest
from unittest import skip
from unittest.mock import patch
import sys
from numpy import array
from contextlib import suppress

from mango.arguments import (SmartFormatter as SF, parse, column_manip, array_vars, verbosity,
                             setdefaults, _help, notebook_help, interactive_help, _mlines_nb)
from mango.constants import getkeywords, _variables, c
from mango.managers import start_server
from mango.errors import ExitQuiet, _strings
from _version import version

from mango.tests.numpy_assert import assertNumpyArraysEqual
from mango.tests import stEQ, captured_output, xfail, platform
from io import BytesIO

class Errors():
    errors = ['{}test array must be the same length as No. MNPs\n\n'.format(_strings._F[0]),
              "{}Invalid column ".format(_strings._F[0]) +"{'avmang_pot'}, unable to plot graph\n\n"]

class SmartFormatter(unittest.TestCase):

    def setUp(self):
        self.out_ans = ['123', '456']

    def test_SF(self):
        self.assertEqual(SF._split_lines(self, "R|123\n456", 3), self.out_ans)


class funcargs(unittest.TestCase):

    def test_verbosity(self):
        class var():
            directory = "test"
            stats = [0]

        class flg():
            run = 1
        v = var()
        f = flg()

        with patch("mango.arguments.c") as cpatch:
            with patch("mango.arguments.start_server") as servpatch:
                for i_n in [True, False]:
                    for pp in [True, False]:
                        for fn in range(5):
                            cpatch.tinfo = {'otty': i_n}
                            f.nout = fn
                            f.pp = pp
                            verbosity(v, f)
                            assert cpatch.header.called
                            assert cpatch.Error.called
                            assert cpatch.Error.call_args[0][0] == f"EV {fn >=2} test 1 {int(pp)}"
                            if f.prog:
                                assert servpatch.called
                                assert servpatch.call_args[0][0] == "Prog"
                                if not f.pp:
                                    assert cpatch.progress.called
                                    assert cpatch.progress.call_args[0][0] == "setup [0]"


class parsing(unittest.TestCase, Errors):

    def __init__(self, *args, **kwargs):
        super(parsing, self).__init__(*args, **kwargs)
        self.maxDiff = None
        self.defaults = getkeywords().defaults
        self.opts = ({"PlotColumns": ["time", "kinetic[5,:,3] + trans_pot / - mag_pot[:100]"]},
                     {"PlotColumns": ["time", "kinetic[5,:,3]  + trans_pot[:,1,1] / - mag_pot"]},
                     {"PlotColumns": ["time", "kinetic + trans_pot[:,1,1] / - mag_pot"]},
                     {"PlotColumns": ["time", "kinetic + trans_pot[:,1,1] / - mag_pot[:100]"]},
                     {"PlotColumns": ["time", "mag_pot[:100]"]},
                     {"PlotColumns": ["time", "mag_pot"]})

        self.string_opts = [["time", "kinetic[5,:,3]+trans_pot/-mag_pot[:100]"],
                            ["time", "kinetic[5,:,3]  + trans_pot[:,1,1] / - mag_pot"],
                            ["time", "kinetic + trans_pot[:,1,1] / - mag_pot"],
                            ["time", "kinetic + trans_pot[:,1,1] / - mag_pot[:100]"],
                            ["time", "mag_pot[:100]"],
                            ["time", "mag_pot"]]
        self.wrongstring_opts = ["time", "avmang_pot"]

        self.files = {"xyz": False, "energy": False, "momenta": False}
        self.col_average = ({"x": False, "e": True, "m": False})

        self.col_out = ({"plot_1": {"x": array([["time", True]]), "y": array([["kinetic", "[5,:,3]"],
                                                                              ["+", False],
                                                                              ["trans_pot", True],
                                                                              ["/", False],
                                                                              ["-", False],
                                                                              ["mag_pot", "[:100]"]])}},
                        {"plot_1": {"x": array([["time", True]]), "y": array([["kinetic", "[5,:,3]"],
                                                                              ["+", False],
                                                                              ["trans_pot", "[:,1,1]"],
                                                                              ["/", False],
                                                                              ["-", False],
                                                                              ["mag_pot", True]])}},
                        {"plot_1": {"x": array([["time", True]]), "y": array([["kinetic", True],
                                                                              ["+", False],
                                                                              ["trans_pot", "[:,1,1]"],
                                                                              ["/", False],
                                                                              ["-", False],
                                                                              ["mag_pot", True]])}},
                        {"plot_1": {"x": array([["time", True]]), "y": array([["kinetic", True],
                                                                              ["+", False],
                                                                              ["trans_pot", "[:,1,1]"],
                                                                              ["/", False],
                                                                              ["-", False],
                                                                              ["mag_pot", "[:100]"]])}},
                        {"plot_1": {"x": array([["time", True]]), "y": array([["mag_pot", "[:100]"]])}},
                        {"plot_1": {"x": array([["time", True]]), "y": array([["mag_pot", True]])}})

    def setUp(self):
        self.argvold = sys.argv
        sys.argv = []
        c.Error = start_server("Error")

    def tearDown(self):
        c.Error("END")
        sys.argv = self.argvold

    def _parse(self, argparse, opts):
        out = parse(argparse=argparse, opts=opts)
        defaults = {**self.defaults.copy(), **{
                    "files": {"xyz": False, "energy": False, "momenta": False}},
                    **{k: False for k in ["defaults", "field", "lastframe",
                                          "pp", "location", "time", "limit",
                                          "ms", "RandNoState", 'extra_iter', "sigma"]}}

        self.assertEqual(set(list(out[0].__dict__.keys()) + list(out[1].__dict__.keys())), set(defaults.keys()))

    def _parse_plotting(self, opts, col_out, col_average):
        out = parse(argparse=False, opts=opts)
        self.assertEqual(out[1].column.keys(), col_out.keys())
        self._col_strings(out[1].column, col_out)

    def _col_strings(self, out, col_out):
        for i in col_out:
            self.assertEqual(out[i].keys(), col_out[i].keys())
            for no, x in enumerate(out[i]["x"]):
                self.assertEqual(out[i]["x"][no, 0], col_out[i]["x"][no, 0])
                self.assertEqual(out[i]["x"][no, 1], col_out[i]["x"][no, 1])
            for no, y in enumerate(out[i]["y"]):
                self.assertEqual(out[i]["y"][no, 0], col_out[i]["y"][no, 0])
                self.assertEqual(out[i]["y"][no, 1], col_out[i]["y"][no, 1])

    @xfail(platform == "darwin",reason='NotImplementedError on Mac OSX because of broken sem_getvalue', raises=NotImplementedError)
    def test_col_strings(self):

        for i in range(len(self.string_opts)):
            col_out,  files = column_manip(self.string_opts[i], self.files)
            self._col_strings(col_out, self.col_out[i])

        with self.assertRaises(ExitQuiet):
            column_manip(self.wrongstring_opts, self.files)
        self.assertEqual(self.errors[1], stEQ.read()[0])

    def test_parse(self):
        self._parse(True, {})

    def test_parse_jupyter(self):
        self._parse(False, {})

    def noipython():
        try:
            import IPython
        except ModuleNotFoundError:
            return True

    @xfail(noipython(), reason='IPython Not installed', raises=ModuleNotFoundError)
    @patch('IPython.display.display', return_value=None)
    @patch('IPython.display.HTML', return_value=None)
    def test_help(self, mockedhtml, mockeddisplay):
        with captured_output() as (out, err):
            with patch('mango.arguments.c._banner', return_value=''):
                with self.assertRaises(SystemExit) as se:
                    self._parse(False, "-h")
            self.assertEqual(se.exception.code, 0)
            output = out.getvalue()
        assert output.startswith(helpout('cli'))

        with captured_output() as (out, err):
            with patch('mango.arguments.c._banner', return_value=''):
                with self.assertRaises(SystemExit) as se:
                    interactive_help()
            self.assertEqual(se.exception.code, 0)
            output = out.getvalue()
        assert output.startswith(helpout('interactive'))

        with captured_output() as (out, err):
            with patch('mango.arguments.c._banner', return_value=''):
                with self.assertRaises(SystemExit) as se:
                    self._parse(False, "-ifh")
            self.assertEqual(se.exception.code, 0)
            output = out.getvalue()
        assert output.startswith(helpout('interactive'))
        with suppress(ModuleNotFoundError):
            with self.assertRaises(SystemExit) as se:
                with patch('mango.arguments.c._banner', return_value=''):
                    notebook_help()
            self.assertEqual(se.exception.code, 0)
            assert mockedhtml.call_args[0][0].startswith(helpout('jupyter'))
            k = {"hello": (('', '', ''), ('', {'help': 'R|hi{}'}))}
            r = _help(k, 'start', 'var:{} help:{}', _mlines_nb, 'end')
            self.assertEqual(r, 'startvar:hello help:hiend')

    def test_parse_plotting(self):

        for i in range(len(self.opts)):
            self._parse_plotting(self.opts[i], self.col_out[i], self.col_average)

    @xfail(platform == "darwin", reason='NotImplementedError on Mac OSX because of broken sem_getvalue', raises=NotImplementedError)
    def test_array_vars(self):
        var = ({'no_molecules': 5, 'test': array([3.]), 'defaults': []},
               {'no_molecules': 5, 'test': array([3., 3., 3., 3., 3.]), 'defaults': []},
               # {'no_molecules': 5, 'test': BytesIO(' 3. 3. 3. 3. 3. '.encode()), 'defaults': []},
               {'no_molecules': 5, 'test': 3., 'defaults': []})
        var_fail = {'no_molecules': 5, 'test': array([3, 4])}

        for i in var:
            parsing = array_vars(_variables(**i), ['test'])
            assertNumpyArraysEqual(parsing.test, array([3., 3., 3., 3., 3.]))

        with self.assertRaises(ExitQuiet):
            array_vars(_variables(**var_fail), ['test'])
        self.assertEqual(self.errors[0], stEQ.read()[0])

    @skip("Unfinished test")
    def test_parse_restart(self):
        return

    # def test_set_nullvars(self):
    #     val = {"a": "Hippopotamus"}
    #     p_val = ({}, {"P": "Swiggle"}, {"R": "HIII"})
    #     out = ({'P': None, 'R': None, 'a': None},
    #            {'P': 'Swiggle', 'R': None, 'a': None},
    #            {'P': None, 'R': 'HIII', 'a': None})

    #     for i in range(len(p_val)):
    #         parsing = set_nullvars(_variables(**p_val[i]), val)
    #         self.assertDictEqual(parsing.__dict__, out[i])

    def test_setdefaults(self):
        defaults = {"Wee": "Woo", "Hi": "hello", "Boo": 4, "P": ['l', 'o', 'g']}
        parsing = _variables(**{"Wee": "bllaa", "P": 'No'})
        parsing = setdefaults(parsing, defaults)
        del defaults['Wee']
        del defaults['P']
        self.assertEqual(parsing.defaults, list(defaults.keys()))
        self.assertEqual(parsing.Wee, 'bllaa')
        self.assertEqual(parsing.P, 'No')
        del parsing.Wee
        del parsing.defaults
        del parsing.P
        self.assertDictEqual(parsing.__dict__, defaults)


def helpout(version):
    if version == 'cli':
        return cli
    elif version == 'interactive':
        return interactive
    elif version == 'jupyter':
        return jupyter

cli = """usage: mango [-h]"""

interactive = '''VariableName          Help'''

jupyter = '''<table><tr><th>VariableName</th><th>Help</th></tr><tr>'''

if __name__ == '__main__':
    unittest.main(verbosity=2)
