import unittest
from mango.boundaries import periodic as P
from mango.tests.numpy_assert import assertNumpyArraysEqual, assertshapeequal, assertvalueequal
from mango.constants import c

from numpy import genfromtxt, array, maximum, einsum, sqrt
from os import path


class periodic(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(periodic, self).__init__(*args, **kwargs)
        lj5_loc = "{}/5particles_lj.txt".format(path.dirname(path.realpath(__file__)))
        self.lj5 = genfromtxt(lj5_loc)
        self.pos_out = array([[0.5, 0.5, 0.5],
                              [0.5, 0.5, -0.5],
                              [0.5, -0.5, 0.5],
                              [0.5, -0.5, -0.5],
                              [-0.5, 0.5, 0.5],
                              [-0.5, 0.5, -0.5],
                              [-0.5, -0.5, 0.5],
                              [-0.5, -0.5, -0.5]])
        self.dist_out = array([[[0., 0., 0.], [0., 0., 1.], [0., 1., 0.], [0., 1., 1.],
                                [1., 0., 0.], [1., 0., 1.], [1., 1., 0.], [1., 1., 1.]],
                               [[0., 0., -1.], [0., 0., 0.], [0., 1., -1.], [0., 1., 0.],
                                [1., 0., -1.], [1., 0., 0.], [1., 1., -1.], [1., 1., 0.]],
                               [[0., -1., 0.], [0., -1., 1.], [0., 0., 0.], [0., 0., 1.],
                                [1., -1., 0.], [1., -1., 1.], [1., 0., 0.], [1., 0., 1.]],
                               [[0., -1., -1.], [0., -1., 0.], [0., 0., -1.], [0., 0., 0.],
                                [1., -1., -1.], [1., -1., 0.], [1., 0., -1.], [1., 0., 0.]],
                               [[-1., 0., 0.], [-1., 0., 1.], [-1., 1., 0.], [-1., 1., 1.],
                                [0., 0., 0.], [0., 0., 1.], [0., 1., 0.], [0., 1., 1.]],
                               [[-1., 0., -1.], [-1., 0., 0.], [-1., 1., -1.], [-1., 1., 0.],
                                [0., 0., -1.], [0., 0., 0.], [0., 1., -1.], [0., 1., 0.]],
                               [[-1., -1., 0.], [-1., -1., 1.], [-1., 0., 0.], [-1., 0., 1.],
                                [0., -1., 0.], [0., -1., 1.], [0., 0., 0.], [0., 0., 1.]],
                               [[-1., -1., -1.], [-1., -1., 0.], [-1., 0., -1.], [-1., 0., 0.],
                                [0., -1., -1.], [0., -1., 0.], [0., 0., -1.], [0., 0., 0.]]])
        self.distmatout = 1 / maximum(sqrt(einsum('ijz,ijz->ij', self.dist_out, self.dist_out)), c.EPS2)

    def setUp(self):
        self.p = P()
        # tri is unused currently

    def test_setup(self):
        dist, dist_mat = self.p.setup(boxsize=100, pbc=True, pos=self.lj5, sigma=10, tri=None)
        self.assertEqual(dist.shape, (5, 5, 3))
        self.assertEqual(dist_mat.shape, (5, 5))

    def test_distance(self):
        self._distance_vbox(100, True)
        self._distance_vbox(100, False)
        self._distance_vbox(1, True)
        self._distance_vbox(1, False)

    # give a small box and some coords to wrap pbc on and off
    def _distance_vbox(self, boxsize, pbc):
        sigma = 10
        self.p.setup(boxsize=boxsize, pbc=pbc, pos=self.pos_out, sigma=sigma, tri=None)

        for _ in range(2):
            dist, distmat = self.p.distance(self.pos_out)

            if sigma < boxsize or str(self.p.wrap.__name__) == '_nowrap':
                assertNumpyArraysEqual(dist, self.dist_out)
                assertNumpyArraysEqual(distmat, self.distmatout)
            else:
                assertshapeequal(dist, self.dist_out)
                assertshapeequal(distmat, self.distmatout)
                assertvalueequal(distmat, 1 / c.EPS2)
                assertvalueequal(dist, 0)

    def test_position(self):
        pass

    def test__call__(self):
        dist = self.dist_out.copy()
        dist_matrix = self.distmatout.copy()
        pos = self.pos_out.copy()

        class test():

            p = P()

            def __init__(self, pbc):
                self.dist = dist
                self.dist_matrix = dist_matrix
                self.pos = pos
                self.p.setup(boxsize=100, pbc=pbc, pos=self.pos, sigma=10, tri=None)

            @p
            def t1(self):
                self.pos += 100

        for p in [True, False]:
            t = test(p)
            for _ in range(2):
                t.t1()  # Not wrapped on first iteration
            if p is True:
                assertNumpyArraysEqual(t.dist, self.dist_out)
                assertNumpyArraysEqual(t.dist_matrix, self.distmatout)
                assertNumpyArraysEqual(t.pos, self.pos_out)
            else:
                assertNumpyArraysEqual(t.dist, self.dist_out)
                assertNumpyArraysEqual(t.dist_matrix, self.distmatout)
                assertNumpyArraysEqual(t.pos, self.pos_out + 200)


if __name__ == '__main__':
    unittest.main(verbosity=2)
