
#  Copyright (C) 2017 by Lorenzo Stella <lorenzo DOT stella77 AT gmail.com>

#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:

#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.

#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#  THE SOFTWARE.

from sys import exit, stdout
import numpy as np
from scipy.optimize import fmin_slsqp
from scipy.constants import physical_constants

# Units

# length: 1.e-6 [cm] # BUGFIX: shouldn't ne 1.e-7 cm?
# mass: 1.e-18 [g] # BUGFIX: shouldn't ne 1.e-21 g?
# time: 1.e-12 [s]

# velocity: 1.e6 [cm/s]
# momentum: 1.e-12 [g*cm/s]
# force: [g*cm/s^2]
# energy unit: 1.e-6 [erg]
# temperature unit: [K]
# magnetic induction: 1.e6 [gauss]
# magnetisation: 1.e-12 [emu]

# Levi-Civita tensor
exyz = np.zeros((3, 3, 3))

exyz[0, 1, 2] = exyz[1, 2, 0] = exyz[2, 0, 1] = 1.

exyz[0, 2, 1] = exyz[2, 1, 0] = exyz[1, 0, 2] = -1.


class cluster(object):
    def __init__(self, fname, Troom=298.15):
        self.box = 400.  # unit: 1.e-6 cm

        self.radius = mnp_radius  # unit: 1.e-6 cm

        self.hradius = self.radius  # unit: 1.e-6 cm

        self.vol = (4. / 3.) * np.pi * (self.radius**3)  # unit: 1.e-18 cm^3

        self.hvol = (4. / 3.) * np.pi * (self.hradius**3)  # unit: 1.e-18 cm^3

        self.dens = 6.99  # unit: g/cm^3

        self.mass = self.dens * self.vol  # unit: 1.e-18 g

        self.Mdens = 8.5e-5  # unit: 1.e6 emu/g

        self.Ms = self.Mdens * self.dens  # unit: 1.e6 emu/cm^3

        self.Mmag = self.Ms * self.vol  # unit: 1.e-12 emu

        # self.eta = 0. # unit: g/(cm*s) # NVE
        self.eta = 1.002e-2  # unit: g/(cm*s) # water
        # self.eta = 1.002 # unit: g/(cm*s) # for relaxation

        # WARNING: GBARE is negative!
        self.alpha = 6. * self.eta * GBARE / self.Ms  # dimensionless damping parameter

        # WARNING: GBARE is negative!
        self.geff = GBARE / (1. + self.alpha**2)  # effective gyromagnetic ratio, unit: 1.e6/(gauss*s)

        self.Troom = Troom  # unit: K

        self.pos, self.mom, self.mag = self.read_frame(fname)  # unit: 1.e-6 cm

        # To get rid of the CoM translation
        self.mom -= np.einsum("iz->z", self.mom) / self.mom.shape[0]  # unit: 1.e-12 g*cm/s

        # noise initialisation
        self.gammat = 6. * np.pi * self.eta * self.hradius / self.mass  # unit: 1.e12/s

        np.random.seed(12345)

        self.count = 0

        self.time = 0.  # unit: 1.e-12 s

        print(" # box = %s [1.e-6 cm]" % (str(self.box)))

        print(" # radius = %s [1.e-6 cm]" % (str(self.radius)))

        print(" # vol = %s [1.e-18 cm^3]" % (str(self.vol)))

        print(" # hradius = %s [1.e-6 cm]" % (str(self.hradius)))

        print(" # hvol = %s [1.e-18 cm]" % (str(self.hvol)))

        print(" # dens = %s [g/cm^3]" % (str(self.dens)))

        print(" # mass = %s [1.e-18 g]" % (str(self.mass)))

        print(" # Mdens = %s [1.e6 emu/g]" % (str(self.Mdens)))

        print(" # Ms = %s [1.e6 emu/cm^3]" % (str(self.Ms)))

        print(" # Mmag = %s [1.e-12 emu]" % (str(self.Mmag)))

        print(" # eta = %s [g/(cm*s)]" % (str(self.eta)))

        print(" # gammat = %s [1.e12/s]" % (str(self.gammat)))

        print(" # alpha = %s []" % (str(self.alpha)))

        print(" # geff = %s [1.e6/(gauss*s)]" % (str(self.geff)))

        print(" # Troom = %s [K]" % (str(self.Troom)))

        stdout.flush()

    def read_frame(self, fname):
        fp = open(fname)

        self.natom = int(fp.readline())

        # skip the comment line
        fp.readline()

        name = []

        pos = []

        mom = []

        mag = []

        for atom in range(self.natom):
            line = fp.readline().split()

            ll = len(line)

            if ll < 10:
                line += ['0.'] * (10 - ll)

                line[9] = '1.'  # By default, pointing up in the z-direction

            name.append(line[0])

            pos.append(line[1:4])

            mom.append(line[4:7])

            mag.append(line[7:10])

        fp.close()

        pos = np.array(pos, dtype=float)  # unit: 1.e-6 cm

        mom = np.array(mom, dtype=float)  # unit: 1.e-12 g*cm/s

        # ##################### CHANGE MADE ########################
        # mom *= self.mass  # unit: 1.e-12 g*cm/s
        ###########################################################

        mag = np.array(mag, dtype=float)  # unit: 1.e-12 emu

        # BUGFIX: What if the norm is very small?
        scale = self.Mmag / np.maximum(np.sqrt(np.einsum("iz,iz->i", mag, mag)), EPS2)

        mag = np.einsum("iz,i->iz", mag, scale)  # unit: 1.e-12 emu

        return pos, mom, mag

    def write_nrg(self, fp):
        ekin = 0.5 * np.einsum("iz,iz", self.mom, self.mom) / self.mass

        epot1 = vv_wca(self.rijz, self.orij)

        epot2 = vv_mag(self.rijz, self.orij, self.mag)

        fp.write(("  %12d") % (self.count) + ("  %12.5e") % (self.time) + ("  %12.5e" * 5) %
                 (ekin, epot1, epot2, ekin + epot1 + epot2, ekin / (1.5 * self.natom * KB)) + "\n")

    def write_mag(self, fp):
        mag = np.einsum("iz->z", self.mag)  # unit: 1.e-12 emu

        fp.write("  %12d  %12.5e" % (self.count, self.time))

        for el in mag:
            fp.write("  %12.5e" % (el))

        fp.write("\n")

    def write_ang(self, fp):
        CoM = np.einsum("iz->z", self.pos) / self.pos.shape[0]  # BUGFIX: missing masses

        angular = np.einsum('iyz,iy->z', np.einsum('xyz,ix->iyz', exyz, self.pos - CoM), self.mom)

        mag = np.einsum("iz->z", self.mag)  # unit: 1.e-12 emu

        # WARNING: GBARE is negative!
        tot = angular + mag / GBARE

        fp.write("  %12d  %12.5e" % (self.count, self.time))

        for el in tot:
            fp.write("  %12.5e" % (el))

        fp.write("\n")

    def write_frame(self, fp):
        fp.write("%9d\n" % (self.pos.shape[0]))

        fp.write("  step =  %12d; time =  %12.5e ps\n" % (self.count, self.time))

        for pos, mom, mag in zip(self.pos, self.mom, self.mag):
            fp.write("%6s" % ('MNP'))

            for el in pos:
                fp.write("  %12.5e" % (el))

            for el in mom:
                fp.write("  %12.5e" % (el / self.mass))

            # BUGFIX: What if the norm is very small?
            scale = 1. / np.maximum(np.sqrt(np.einsum("z,z->", mag, mag)), EPS2)

            for el in mag:
                fp.write("  %12.5e" % (el * scale))

            fp.write("\n")

    def wrap_position(self):
        # PBC: cubic cell [-box/2:+box/2]
        self.pos -= self.box * np.floor(self.pos / self.box + 0.5)

    def update_position(self, dt):
        self.pos += (self.mom / self.mass) * dt

    def update_momentum(self, dt):
        force = np.zeros_like(self.pos)

        force += ff_wca(self.rijz, self.orij)

        force += ff_mag(self.rijz, self.orij, self.mag)

        self.mom += force * dt

    def update_stochastic(self, dt):
        c1t = np.exp(-self.gammat * dt)  # dimensionless

        c2t = np.sqrt(KB * self.Troom * self.mass * (1. - c1t**2))  # [A/fs]

        self.mom = c1t * self.mom + c2t * np.random.standard_normal(size=self.mom.shape)

    def update_magnetisation(self, dt):
        # BUGFIX: The following line is redundant as the magnetic moments are set at the beginning
        mmag = np.sqrt(np.einsum("iz,iz->i", self.mag, self.mag))  # unit: 1.e-12 emu

        # stochastic part
        # Note that c2m is divided here by sqrt(dt), but finally multiplied by dt.
        # The stochastic part correctly scales with sqrt(dt)
        # WARNING: alpha and GBARE are negative!
        c2m = np.sqrt(2. * KB * self.Troom * self.alpha / (self.Ms * GBARE * self.hvol * dt))  # unit: 1.e6 oersted

        # WARNING: It does not depend on mag, it does not go into the self-consistency loop!
        hfield_rnd = c2m * np.random.standard_normal(size=self.mag.shape)  # unit: 1.e6 oersted

        def deltamag(mag, dt):
            mag += self.mag  # unit: 1.e-12 emu

            umag = np.einsum("iz,i->iz", mag, 1. / np.sqrt(np.einsum("iz,iz->i", mag, mag)))  # dimensionless

            mag = np.einsum("iz,i->iz", umag, mmag)  # unit: 1.e-12 emu

            # WARNING: The new mag is used just to compute hfield!
            hfield = hh_mag(self.rijz, self.orij, mag) + hfield_rnd  # unit: 1.e6 oersted

            # WARNING: alpha is negative!
            Heff = hfield - self.alpha * np.einsum('iyz,iy->iz', np.einsum('xyz,ix->iyz', exyz, umag), hfield)  # unit: 1.e6 oersted

            return self.geff * dt * np.einsum('iyz,iy->iz', np.einsum('xyz,ix->iyz', exyz, mag), Heff)  # unit: 1.e-12 emu

        # self-consistency starts here
        mag = np.copy(self.mag)

        miter = 1

        mag_old = np.zeros_like(mag)

        while True:
            mag_old[:] = mag  # unit: 1.e-12 emu

            mag = self.mag + deltamag(mag, dt)  # unit: 1.e-12 emu

            mnorm = np.sqrt(np.einsum("iz,iz", mag - mag_old, mag - mag_old) / np.einsum("iz,iz", mag_old, mag_old))  # dimensionless

            if mnorm < EPS2 or miter > MAXITER:
                break

            miter += 1

        # print " # Magnetic iterations: %3d [mnorm %12.5e, MD step %9d]"%(miter, mnorm, self.count)

        global mitercount
        mitercount += miter

        if miter > MAXITER:
            print(" # WARNING: Maximum number of iterations exceeded during step %9d with residual norm %12.5e." %
                  (self.count, mnorm)); stdout.flush()

        # whole step
        self.mag += deltamag(mag, dt)  # unit 1.e-12 emu

    def propagate(self, nsteps=1.e4, dt=10., skip=10):
        print(" # nsteps = %s []" % (str(nsteps)))
        stdout.flush()

        print(" # dt = %s [1.e-12 s]" % (str(dt)))

        print(" # skip = %s []" % (str(skip)))

        fpxyz = open("wca_mag.xyz", "w")

        fpe = open("wca_mag_nrg.dat", "w")

        fpm = open("wca_mag_mag.dat", "w")

        fpa = open("wca_mag_ang.dat", "w")

        self.wrap_position()

        self.rijz, self.orij = update_dist(self.pos, self.box)

        self.write_frame(fpxyz)

        self.write_nrg(fpe)

        self.write_mag(fpm)

        self.write_ang(fpa)

        for step in range(nsteps):
            self.count += 1

            self.time += dt

            # half position_step
            self.update_position(0.5 * dt)

            self.wrap_position()

            self.rijz, self.orij = update_dist(self.pos, self.box)

            # half momentum_step
            self.update_momentum(0.5 * dt)

            # half stochastic_step
            self.update_stochastic(0.5 * dt)

            # full magnetisation_step
            if self.Mmag > EPS2:
                self.update_magnetisation(dt)

            # half stochastic_step
            self.update_stochastic(0.5 * dt)

            # half momentum_step
            self.update_momentum(0.5 * dt)

            # half position_step
            self.update_position(0.5 * dt)

            self.wrap_position()

            self.rijz, self.orij = update_dist(self.pos, self.box)

            if self.count % int(skip) == 0:
                self.write_frame(fpxyz)

                self.write_nrg(fpe)

                self.write_mag(fpm)

                self.write_ang(fpa)

        fpa.close()

        fpm.close()

        fpe.close()

        fpxyz.close()

        print(" # self-consistency iterations = %s []" % (str(mitercount)))

        print(" # average self-consistency iterations per step = %s []" % (str(mitercount / nsteps)))
        stdout.flush()

    def optimise(self, toll=1.e-5, thermal=False, iters=2000):
        def func(x):
            pos = x[:self.pos.size].reshape(self.pos.shape)

            mag = x[self.pos.size:self.pos.size + self.mag.size].reshape(self.mag.shape)

            rijz, orij = update_dist(pos, self.box)

            return vv_wca(rijz, orij) + vv_mag(rijz, orij, mag)

        def dfunc(x):
            pos = x[:self.pos.size].reshape(self.pos.shape)

            mag = x[self.pos.size:self.pos.size + self.mag.size].reshape(self.mag.shape)

            rijz, orij = update_dist(pos, self.box)

            # Note that the gradient with respect to the magnetisation
            # gives the magnetic field. This cannot go to zero because
            # of the constrained normalisation (see below).
            return -np.append(ff_wca(rijz, orij), hh_mag(rijz, orij, mag))

        def normalisation(x):
            mag = x[self.pos.size:self.pos.size + self.mag.size].reshape(self.mag.shape)

            return np.einsum("iz,iz->i", mag, mag) - np.einsum("iz,iz->i", self.mag, self.mag)

        print(" # Initial optimisation")
        stdout.flush()

        x0 = np.append(self.pos, self.mag)

        x = fmin_slsqp(func, x0, fprime=dfunc, f_eqcons=normalisation, acc=toll, iter=iters, iprint=2)

        self.pos = x[:self.pos.size].reshape(self.pos.shape)  # unit: 1.e-6 cm

        if thermal:
            self.mom = np.sqrt(self.mass * KB * self.Troom) * np.random.standard_normal(size=self.mom.shape)  # unit: 1.e-12 g*cm/s

        if self.Mmag > EPS2:
            self.mag = x[self.pos.size:self.pos.size + self.mag.size].reshape(self.mag.shape)  # unit: 1.e-12 emu

            scale = self.Mmag / np.maximum(np.sqrt(np.einsum("iz,iz->i", self.mag, self.mag)), EPS2)

            self.mag = np.einsum("iz,i->iz", self.mag, scale)  # unit: 1.e-12 emu


def update_dist(pos, box):
    # rijz(i,j,z) = pos(i,z)-pos(j,z)
    dummy = np.ones_like(pos)

    rijz = np.einsum("iz,jz->ijz", pos, dummy) - np.einsum("iz,jz->jiz", pos, dummy)

    # PBC: cubic cell [-box/2:+box/2]
    rijz -= box * np.floor(rijz / box + 0.5)

    orij = 1. / np.maximum(np.sqrt(np.einsum('ijz,ijz->ij', rijz, rijz)), EPS2)

    return rijz, orij


def vv_wca(rijz, orij):
    epot = 0.  # unit: 1.e-6 erg

    for ia in range(rijz.shape[0]):
        for ja in range(ia + 1, rijz.shape[0]):
            lj6 = (sigma * orij[ia, ja])**6

            if orij[ia, ja] > 1. / rc_wca:
                epot += epsilon * (1. + 4. * lj6 * (lj6 - 1.))  # unit: 1.e-6 erg

    return epot


def ff_wca(rijz, orij):
    force = np.zeros_like(rijz[0, :, :])  # unit: dine

    for ia in range(rijz.shape[0]):
        for ja in range(ia + 1, rijz.shape[0]):
            if orij[ia, ja] > 1. / rc_wca:
                lj6 = (sigma * orij[ia, ja])**6

                dummy = -24. * epsilon * (2. * lj6 - 1.) * lj6 * rijz[ia, ja] * (orij[ia, ja]**2)  # unit: dine

                force[ia] -= dummy  # unit: dine

                force[ja] += dummy  # unit: dine

    return force


def vv_mag(rijz, orij, mag):
    epot = 0.  # unit: 1.e-6 erg

    for ia in range(rijz.shape[0]):
        for ja in range(ia + 1, rijz.shape[0]):
            unitv = rijz[ia, ja] * orij[ia, ja]  # dimensionless

            magi = np.einsum("i, i->", mag[ia], unitv)  # unit: 1.e-12 emu

            magj = np.einsum("i, i->", mag[ja], unitv)  # unit: 1.e-12 emu

            magij = np.einsum("i, i->", mag[ia], mag[ja])  # unit: 1.e-24 emu^2

            epot -= (3. * magi * magj - magij) * (orij[ia, ja]**3)  # unit: 1.e-6 erg

    return epot


def ff_mag(rijz, orij, mag):
    force = np.zeros_like(rijz[0, :, :])  # unit: dine

    for ia in range(rijz.shape[0]):
        for ja in range(ia + 1, rijz.shape[0]):
            unitv = rijz[ia, ja] * orij[ia, ja]  # dimensionless

            magi = np.einsum("i, i->", mag[ia], unitv)  # unit: 1.e-12 emu

            magj = np.einsum("i, i->", mag[ja], unitv)  # unit: 1.e-12 emu

            magij = np.einsum("i, i->", mag[ia], mag[ja])  # unit: 1.e-24 emu^2

            dummy = 3. * ((5. * magi * magj - magij) * unitv - magi * mag[ja] - magj * mag[ia]) * (orij[ia, ja]**4)  # unit: dine

            force[ia] -= dummy  # unit: dine

            force[ja] += dummy  # unit: dine

    return force


def hh_mag(rijz, orij, mag):
    hfield = np.zeros_like(mag)  # unit: 1.e6 oersted

    for ia in range(mag.shape[0]):
        for ja in range(ia + 1, mag.shape[0]):
            unitv = rijz[ia, ja] * orij[ia, ja]  # dimensionless

            magi = np.einsum("i, i->", mag[ia], unitv)  # unit: 1.e-12 emu

            magj = np.einsum("i, i->", mag[ja], unitv)  # unit: 1.e-12 emu

            dummy = (orij[ia, ja])**3  # unit: 1.e6 oersted

            hfield[ia] += (3. * magj * unitv - mag[ja]) * dummy  # unit: 1.e6 oersted

            hfield[ja] += (3. * magi * unitv - mag[ia]) * dummy  # unit: 1.e6 oersted

    return hfield


# ####################### CHANGE MADE ############################
def constants():
    # constants
    AMU = physical_constants["atomic mass constant"][0] * 1.e21  # unit: 1.e-18 g

    print(" # AMU = %s [1.e-18 g]" % (str(AMU)))

    KB = physical_constants["Boltzmann constant"][0] * 1.e13  # unit: 1.e-6 erg/K

    print(" # KB = %s [1.e-6 erg/K]" % (str(KB)))

    # WARNING: GBARE is negative!
    GBARE = -physical_constants["electron gyromag. ratio"][0] * 1.e-10  # unit: 1.e6/(gauss*s)

    print(" # GBARE = %s [1.e6/(gauss*s)]" % (str(GBARE)))

    BMAG = physical_constants["Bohr magneton"][0] * 1.e15  # unit: 1.e-12 emu

    print(" # BMAG = %s [1.e-12 emu]" % (str(BMAG)))

    EPS1 = np.finfo(np.float32).eps

    print(" # EPS1 = %s []" % (str(EPS1)))

    EPS2 = np.finfo(np.float64).eps

    print(" # EPS2 = %s []" % (str(EPS2)))

    mnp_radius = 7.5  # unit: 1.e-6 cm

    # Lennard-Jones parameters
    rc_wca = 2. * mnp_radius  # unit: 1.e-6 cm

    print(" # rc_wca = %s [1.e-6 cm]" % (str(rc_wca)))

    sigma = rc_wca / (2.**(1. / 6.))  # unit: 1.e-6 cm

    print(" # sigma = %s [1.e-6 cm]" % (str(sigma)))

    # WARNING: rescaled with respect to the Ar epsilon
    # in order to have a similar isothermal compressibility
    epsilon = 119.8 * KB * (sigma / 0.03405)**3  # unit: 1.e-6 erg

    print(" # epsilon = %s [1.e-6 erg]" % (str(epsilon)))

    stdout.flush()

    MAXITER = 50

    # magnetic iteration counter
    mitercount = 0

    dt = 10
    skip = 10
    return AMU, KB, GBARE, BMAG, EPS1, EPS2, epsilon, sigma, rc_wca, skip, dt, mnp_radius, MAXITER, mitercount


def main():
    toll = EPS1

    nsteps = int(5.e7)
    #nsteps = int(5.e8)

    wca_mag = cluster("/u/jamesc/1p.xyz")

    wca_mag.optimise(toll=toll, thermal=False)

    wca_mag.propagate(nsteps=nsteps, dt=dt, skip=skip)


if __name__ == "__main__":

    AMU, KB, GBARE, BMAG, EPS1, EPS2, epsilon, sigma, rc_wca, skip, dt, mnp_radius, MAXITER, mitercount = constants()

    main()

######################################################################################################
