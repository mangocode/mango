import unittest
from unittest.mock import patch

from mango.tests import it_Mock, captured_output

import mango.debug as md
from mango.constants import _variables


class debugtest(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(debugtest, self).__init__(*args, **kwargs)

        def get_input(text):
            return input(text)

        md.input = get_input

    def _set_debug(self, arg, args0=['']):
        margv = it_Mock()
        margv.return_value = args0 + arg
        with patch('mango.debug.argv', margv):
            md.WHAT_TO_DEBUG, md.DBG = md.set_debug()
            return (md.WHAT_TO_DEBUG, md.DBG)

    def test_sdbg(self):
        arg = ['--debug']
        # No extra output but debug on
        self.assertEqual(self._set_debug(arg), (set(), True))

        @md.debug(['test1'])
        def test1():
            pass

        @md.debug(['test2'])
        class tester():

            def __init__(self):
                self.test = True

            @md.debug(['test2'])
            def test2(self):
                pass

        with captured_output() as (out, err):
            test = tester()
            test1()
            test.test2()

        self.assertEqual(out.getvalue() + err.getvalue(), '')

    def test_debug_tst(self):

        names = ['test3', 'test4']
        titles = ['took', 'total', 'returned']
        arg = ['--debug', '[', names[0], names[1], ']']

        self.assertEqual(self._set_debug(arg), ({names[0], names[1]}, True))

        # Multiple calls multiple functions
        @md.debug([names[0]])
        def test3():
            return True

        @md.debug([names[1]])
        def test4(test):
            pass

        with captured_output() as (out, err):
            test3()
            test3()
            test4('test')
            test3()

        output = self.split_output(out.getvalue())

        for i in [0, 2, 6]:
            self.assertEqual(output[i], [names[0], '()', '{}', '[]'])

        self.assertEqual(output[4], [names[1], "('test',)", '{}', '[]'])

        current_total = 0
        for i in [1, 3, 7]:
            val = output[i]
            current_total += float(val[2][:-1])
            total = float(val[4][:-1])
            self.assertAlmostEqual(current_total, total)
            self.assertEqual(val[0], names[0])
            self.assertEqual(val[-1], 'True')
            for no, j in enumerate(titles):
                num = ((no + 1) * 2) - 1
                self.assertEqual(val[num], j)

        val = output[5]
        self.assertEqual(val[-5], names[1])
        self.assertEqual(val[-4], titles[0])
        self.assertEqual(val[-2], titles[1])
        self.assertAlmostEqual(float(val[-3][:-1]), float(val[-1][:-1]))

    def test_dbgcls(self):
        arg = ['--debug', '[', 'test_cls', 'test5', 'test6', ']']
        self.assertEqual(self._set_debug(arg), ({'test_cls', 'test5', 'test6'}, True))

        class tester():

            @md.debug(['test_cls'])
            def __init__(self):
                self.test = True

            @md.debug(['test5'])
            def test5(self):
                return True

            @md.debug(['test6'])
            def test6(self, test):
                return test

        with captured_output() as (out, err):
            test = tester()
            test.test5()
            test.test6("test")

        output = self.split_output(out.getvalue())
        outclsname = '<{}.debugtest.test_dbgcls.<locals>.tester'.format(
                     '__main__' if __name__ == '__main__' else 'mango.tests.test_debug')
        self.assertEqual(output[0][1], outclsname)
        self.assertEqual(output[3][-1], 'True')
        self.assertEqual(output[4], ['test6', outclsname, "'test')", '{}', "[{'DBG':", "True,", "'test':", "True}]"])
        self.assertEqual(output[5][-1], 'test')

    def test_dbgvars(self):
        arg = ['--debug', '[', 'test7', ']']
        self.assertEqual(self._set_debug(arg), ({'test7'}, True))

        @md.debug(['test7'])
        def test7(vard):
            pass

        vard = _variables(**{'test': 'test'})
        with captured_output() as (out, err):
            test7(vard)

        output = self.split_output(out.getvalue())
        val = eval(output[0][3] + output[0][4])[0]
        self.assertTrue(isinstance(val, dict))
        self.assertDictEqual(val, {'test': 'test'})

    def test_ndbg(self):
        self.assertEqual(self._set_debug([]), (set(), False))
        # No extra output

        @md.debug(['test8'])
        def test8():
            pass
        with captured_output() as (out, err):
            test8()
        self.assertEqual(out.getvalue() + err.getvalue(), '')

    def test_dbgTT(self):
        self.assertEqual(self._set_debug(['--debug', '[', 'TT', 'test9', ']']), ({'TT', 'test9'}, True))

        # Timing only
        @md.debug(['test9'])
        def test9():
            pass

        with captured_output() as (out, err):
            test9()
            test9()
        output = self.split_output(out.getvalue())

        count = 0
        for i in range(len(output)):
            count += float(output[i][-3][:-1])
            self.assertAlmostEqual(float(output[i][-1][:-1]), count)

    @patch('mango.debug.input', return_value='test10')
    def test_reg(self, input):
        args0 = ['Regtests.py']
        self.assertEqual(self._set_debug([], args0=args0), ({'test10'}, True))

    def split_output(self, output):
        output = output.split('\n')
        for i in range(len(output)):
            output[i] = output[i].split()
            if 'object' in output[i]:
                obj = output[i].index('object')
                del output[i][obj + 2]
                del output[i][obj + 1]
                del output[i][obj]
                output[i][obj - 1] = output[i][obj - 1][1:]
        return output[:-1]


if __name__ == '__main__':
    unittest.main(verbosity=2)
