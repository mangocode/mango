from contextlib import contextmanager
from io import StringIO

import sys


@contextmanager
def captured_output():
    nout, nerr = StringIO(), StringIO()
    oout, oerr = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = nout, nerr
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = oout, oerr
