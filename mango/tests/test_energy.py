import unittest

from numpy import zeros, arange, array
from functools import partial
from scipy.constants import physical_constants

from mango.tests import captured_output, ml_transwrap
from mango.tests.numpy_assert import assertNumpyArraysEqual

import mango.tests.magliq as ml
from mango.tests.magliq import cluster
from mango.pp.energy import energy_calc
from mango.boundaries import periodic


class energy(unittest.TestCase):

    def setUp(self):
        self.sav_vv = ml.vv_wca

    def tearDown(self):
        del ml.KB
        del ml.GBARE
        ml.vv_wca = self.sav_vv

    def test_energy_calc(self):

        iters = 3
        nm = 5
        zero = zeros((iters, nm, 3))
        pos = arange(0, zero.size).reshape(zero.shape) * 0.1
        mag = pos.copy()
        mom = pos.copy()

        epsilon = 1
        sigma = 1
        maxdiam = 1
        limit = 1 / maxdiam
        mass = 1e-5 + zeros(nm)

        eout = energy_calc(pos, mom, mag, epsilon, sigma, limit, mass)

        self.x = ''

        class cl(cluster):

            def __init__(self):
                pass

        class file():
            def write(x):
                self.x += x

        clust = cl()
        clust.count = clust.time = 0
        clust.natom = nm
        clust.mass = mass[0]
        clust.Troom = 100

        rijz, orij = periodic().setup_full(pos)
        ml.KB = physical_constants["Boltzmann constant"][0] * 1.e13
        ml.GBARE = -physical_constants["electron gyromag. ratio"][0] * 1.e-10  # unit: 1.e6/(gauss*s)
        # ml.GBARE *= 1000  # ?fix
        ml.vv_wca = partial(ml_transwrap, ml.vv_wca, epsilon=epsilon, sigma=sigma, rc_wca=1 / limit)
        for i in range(iters):
            self.x = ''

            clust.rijz, clust.orij = rijz[i], orij[0]
            clust.mag = mag[i].copy()
            clust.mom = mom[i].copy()
            clust.pos = pos[i].copy()

            with captured_output() as (kin, err):
                # with patch('mango.test.magliq.vv_wca', new_callable=vv_wca):
                clust.write_nrg(file)

            self.assertAlmostEqual(float(self.x.split()[2]), float('{:12.5e}'.format(eout['kinetic'][i])))
            self.assertAlmostEqual(float(self.x.split()[3]), float('{:12.5e}'.format(eout['trans_pot'][i])))
            self.assertAlmostEqual(float(self.x.split()[4]), float('{:12.5e}'.format(eout['mag_pot'][i])))
            self.assertAlmostEqual(float(self.x.split()[5]), float('{:12.5e}'.format(eout['total_E'][i])))

            self.x = ''

            clust.write_ang(file)
            assertNumpyArraysEqual(array(self.x.split(), dtype=float)[2:], eout['total_M'][i])
