import unittest
from unittest import skip
from unittest.mock import patch, Mock
from numpy import zeros, array, arange, sin, einsum, ones, errstate
from math import pi
from mango.tests.numpy_assert import assertNumpyArraysEqual
from mango.tests import captured_output, tester

from mango.managers import start_server
from mango.pp.acf import Autocorr
from mango.pp.main import collectdata, readin
from mango.imports import fftw
from mango.constants import c
from contextlib import ExitStack

pytest = tester['testbed']
xfail = tester['xfail']
#import matplotlib.pyplot as plt


# class regtests(unittest.TestCase):

#     def test_magauto(self):
# import sys
# name = '~/Documents/_Python/xyz_tools.py'
# spec = imp_u.spec_from_file_location(name.rsplit("/")[-1].split(".")[0], name)
# module = imp_u.module_from_spec(spec)
# spec.loader.exec_module(module)
# sys.argv = ['Name', '-f', '/data/jamesc/test/n5e+07_300K_1.67e+08gauss_2.67e-10kHz_5nm/S_Run1.0.m.xyz']
# dummy = module.main()
# mango.run({'Logdirectory': '/data/jamesc/test/n5e+07_300K_1.67e+08gauss_2.67e-10kHz_5nm/', 'Run': 1, 'SuceptibilityCalc': ['chi']})


class pp_funcs(unittest.TestCase):

    def setUp(self):
        c.Error = start_server('Error')

    def tearDown(self):
        c.Error('END')

    def test_readin(self):
        class vr():

            fname = 'test'
            name = 'test'
            lst = []
            tauN = False
            written = 2.0

            def read(self, *args, **kw):
                return vr()

            def __getitem__(self, name):
                self.lst += [name]
                if name in ['nmax', 'stats', 'skip_iters', 'dt', 'skip']:
                    return 1.0
                elif name == 'written':
                    return self.written
                else:
                    return name

            def __contains__(self, name):
                if name == 'tauN':
                    self.lst += ['Cont_' + name]
                    return self.tauN
                else:
                    return False

        class flg():
            save_type = 'hdf5'
            kinetic = False
            suscep = False
            column = False
            showg = False
            files = {'xyz': False, 'energy': False, 'momenta': False}
            average = ['x', 'e', 'm']
            ufile = False
            lengthcheck = False
            eq = 0.1

        data = ('timescale', 'datalen', 'xyz_data',
                'energy_data', 'momenta_data', 'sus_data', 'stats')
        sdata = ('time', 'freq', 'atc_data')
        kdata = ('baseline', 'histo', 'err', 'raw')
        pms = [Mock(), Mock(), Mock(), Mock(), Mock(), Mock(), Mock(), Mock(), Mock(), Mock(), Mock()]
        patches = [patch('mango.imports', pms[0]), patch('mango.pp.main.read_data', pms[1]),
                   patch('mango.pp.main.collectdata', pms[2]),
                   patch('mango.pp.main.plot_col', pms[3]), patch('mango.pp.main.get_suscep', pms[4]),
                   patch('mango.pp.main.kinetic_temperature', pms[6]),
                   patch('mango.pp.main.onevent', pms[7]), patch('mango.pp.main.showgraph', pms[8]),
                   patch('mango.pp.main.plotter', pms[9]), patch('mango.pp.main.listdir', pms[10])]

        pms[0].matplotlib.return_value = 'mpl'
        pms[1].return_value = vr()
        pms[2].return_value = data
        pms[4].return_value = sdata
        pms[6].return_value = kdata
        pms[7].return_value = 'figevent'
        pms[10].return_value = ['a.svg', 'a.dat', 'test']

        with ExitStack() as stack:
            for i in patches:
                stack.enter_context(i)
            readin('test', 1, '', 1, flg)

        varslist = ['boxsize', 'no_molecules', 'stats', 'dt',
                    'nmax', 'vol', 'radius', 'ms',
                    'temp', 'mass', 'skip', 'written',
                    'skip_iters', 'epsilon', 'sigma', 'limit', 'Cont_tauN']

        self.assertEqual(pms[1].return_value.lst, varslist)
        self.assertEqual(pms[8].call_args_list[0][0][-1], False)

        vr.lst = []
        vr.tauN = True
        pms[1] = Mock()
        pms[1].return_value = vr()
        patches[1] = patch('mango.pp.main.read_data', pms[1])

        with ExitStack() as stack2:
            for i in patches:
                stack2.enter_context(i)
            readin('test', 1, '', 1, flg)

        self.assertEqual(pms[1].return_value.lst, varslist + ['tauN_0', 'keff', 'temp0'])

    def test_collectdata(self):
        iters = 10
        no_mol = 10
        stats = 10

        class flg():
            prog = False
            align = False
            suscep = False
 
        class var():
            dt = 20 
            stats = 10
            no_mol = 10
            skip = 10 
            skip_iters = epsilon = sigma = limit = mass = None

        collectdata_varsd = {i: None for i in ["reader", "name",]}
        collectdata_varsd['flg'] = flg()
        collectdata_varsd['var'] = var()
        collectdata_varsd['eq'] = 0.1

        str_out = ''
        for i in range(stats):
            str_out += f"Stat_{i} ...Done\n"
        str_out += "\n\n"

        class da():
            def __init__(self, iters, no_mol, var):
                stats = var.stats
                self.energy_data = zeros((stats, iters))
                self.momenta_data = zeros((stats, iters, 3))
                self.xyz_data = zeros((stats, iters, no_mol, 12))
                self.sus_data = zeros((stats, iters, no_mol))
                self.var = var()
                self.datalen = iters
                self.var.skip_iters = int((1 - collectdata_varsd['eq']) * iters)
                self.xyz = self.energy = self.momenta = self.sus = self._noop
                self.names = {i: f'Stat_{i}' for i in range(stats)}

            def _noop(self, *args):
                return

        with patch('mango.pp.main.Dataarrays', return_value=da(iters, no_mol, var)):
            with captured_output() as (out, err):
                (timescale, datalen, xyz_data,
                 energy_data, momenta_data, sus_data, stats) = collectdata(**collectdata_varsd)

        output = out.getvalue()
        assert output == str_out

        self.assertEqual(datalen, iters)
        print(timescale)
        assert all(k.shape == (9,) for k in timescale.values())

        with patch('mango.pp.main.Dataarrays', return_value=da(iters, no_mol, var)):
            with patch("mango.pp.main.CoM_motion_rm") as CoM:
                with captured_output() as (out, err):
                    collectdata_varsd['flg'].align = True
                    collectdata(**collectdata_varsd)
                    collectdata_varsd['flg'].align = False

        output = out.getvalue()
        assert output == str_out
        assert CoM.called
        assert CoM().align.called

        with patch('mango.pp.main.Dataarrays', return_value=da(iters, no_mol, var)):
            with patch("mango.pp.main.c") as cprog:
                with captured_output() as (out, err):
                    collectdata_varsd['flg'].prog = True
                    collectdata(**collectdata_varsd)
                    collectdata_varsd['flg'].prog = False

        assert cprog.progress.sections == datalen
        assert cprog.progress.bar.called

    @skip('Function changes')
    def test_autocorr(self):
        fft, ffts, ifft, bytealign = fftw()
        hundred = arange(0, 100)
        z = zeros((1, 100, 1, 3))
        z[..., 2] = hundred[None, :, None]
        data = {'test': sin(z)}
        mmag = 1
        ret, freq, time = autocorr_calc(data, 100, 1, mmag, 1)
        assertNumpyArraysEqual(time, arange(-99, 100))
        assertNumpyArraysEqual(freq, 2 * pi * ffts.fftshift(ffts.fftfreq(199)))
        self.assertEqual(ret['test']['data'].shape, (199,))
        self.assertEqual(ret['test']['ifft'].shape, (199,))

        ret, freq, time = autocorr_calc(data, 100, 1, 1, mmag, 2)
        self.assertEqual(ret['test']['data'].shape, (99, ))
        self.assertEqual(ret['test']['ifft'].shape, (99, ))
        # Check numbers?
        # plt.plot(freq, ret['test']['data'])
        # plt.plot(freq, ret['test']['ifft'])
        # plt.show()
        # self.assertAlmostEqual(ret['test']['scale'], 1)

    @skip('Unfinished test')
    def test_suscep_calbc(self):  # (f, *args, **kwds):
        pass  # return f(*args, **kwds)

    @skip('Unfinished test')
    def test_plot_suscep(self):  # (f, *args, **kwds):
        pass
        # return f(*args, **kwds)

    @skip('Unfinished test')
    def test_get_suscep(self):
        pass

    @skip('Unfinished test')
    def test_plot_col(self):  # (f, *args, **kwds):
        pass
        # return f(*args, **kwds)


if __name__ == '__main__':
    unittest.main(verbosity=2)
