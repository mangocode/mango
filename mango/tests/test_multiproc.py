import unittest
from unittest.mock import patch
from mango.multiproc import mp_handler
import mango.multiproc
from mango.errors import ExitQuiet


class mp(unittest.TestCase):

    def setUp(self):
        pass  # unittest.installHandler()

    def test_mp_handler(self):  # (f, *args, **kwds):
        stats = list(range(5))
        out1 = mp_handler(runner, worker(**{'hi': 'hi', 'lo': 'lo'}), stats)
        self.assertEqual(out1, [(0, 'hi', 'lo'), (1, 'hi', 'lo'), (2, 'hi', 'lo'), (3, 'hi', 'lo'), (4, 'hi', 'lo')])
        with patch("mango.multiproc.min", return_value=1):
            out2 = mp_handler(runner, worker(**{'hi': 'hi', 'lo': 'lo'}), stats, para=True)
        out3 = mp_handler(runner, worker(**{'hi': 'hi', 'lo': 'lo'}), stats, para=False)
        self.assertEqual(out1, out2)
        self.assertEqual(out1, out3)

        # parallel
        with self.assertRaises(ExitQuiet) as eq:  # Catch stdout  'Test exception'
            mp_handler(runner, worker(**{'hi': 'hi', 'lo': 'lo', 'raise_it': 'Ex'}), [1], para=True)
        self.assertEqual(eq.exception.args[0], 'Exception: Test exception')

        with self.assertRaises(Exception) as eq:  # Catch stdout  'Test exception'
            mp_handler(runner, worker(**{'hi': 'hi', 'lo': 'lo', 'raise_it': 'Ex'}), stats, para=False)
        self.assertEqual(eq.exception.args[0], 'Test exception')

        mango.multiproc.DBG = True

        with self.assertRaises(Exception):
            mp_handler(runner, worker(**{'hi': 'hi', 'lo': 'lo', 'raise_it': "Ex"}), stats, para=True)
        # with self.assertRaises(KeyboardInterrupt):
        #     mp_handler(runner, worker(**{'hi': 'hi', 'lo': 'lo', 'raise_it': "Key"}, 1, para=False)


def runner(work, stat):
    return work.run(stat)


class worker():
    def __init__(self, hi, lo, raise_it=None):
        self.hi = hi
        self.lo = lo
        self.raise_it = raise_it

    def run(self, stat):
        if self.raise_it == "Ex":
            raise Exception("Test exception")

        # elif raise_it == 'Key':
        #     raise KeyboardInterrupt

        return (stat, self.hi, self.lo)


if __name__ == '__main__':
    unittest.main(verbosity=2)
