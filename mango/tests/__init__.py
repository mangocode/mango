import multiprocessing
import multiprocessing.queues as mpq
import sys
import mango.managers as mm
from unittest.mock import Mock, mock_open
import mango.tests.magliq as ml
from mango.pp.util import captured_output


__all__ = ["run_all"]


def testbed():
    import pytest
    print("Testing with pytest")
    return {"testbed": pytest, "testwith": pytest.main, "args": None, "xfail": pytest.mark.xfail}


def run_all():
    tester = testbed()
    try:
        if len(tester['args']) == 1:
            tester['testwith'](tester['args'])
        else:
            tester['testwith'](*tester['args'])
    except TypeError:
        tester['testwith']()


class StdQueue(mpq.Queue):

    def __init__(self, qtype, *args, **kwargs):
        ctx = multiprocessing.get_context()
        super(StdQueue, self).__init__(*args, **kwargs, ctx=ctx)
        if qtype == 'E':
            self.write = self.e_write
            self.io = sys.__stderr__
        elif qtype == 'O':
            self.write = self.o_write
            self.io = sys.__stdout__

    def e_write(self, msg):
        if set(msg) not in [{'\n'}, {' ', '\n'}] and msg not in ['', '\033[A']:
            self.put(msg, timeout=5)

    def o_write(self, msg):
        self.put(msg, timeout=5)

    def read(self):
        messages = []
        for _ in range(self.qsize()):
            messages += [self.get(timeout=5)]
        return messages

    def flush(self):
        self.io.flush()


class Process(multiprocessing.Process):

    def __init__(self, target, args, daemon):
        super(Process, self).__init__(daemon=daemon)
        self._target = target
        args = list(args)
        args.append(stFQ)
        self._args = tuple(args)

    def run(self):
        sys.stdout = stOQ
        sys.stderr = stEQ
        self._target(*self._args)


class it_Mock(Mock):

    def __getitem__(self, index):
        return self.return_value[index]

    def index(self, name):
        return self.return_value.index(name)

    def __delitem__(self, index):
        del self.return_value[index]

    def __len__(self):
        return len(self.return_value)


def ml_transwrap(f, *args, **kw):
    ml.epsilon = kw['epsilon']
    ml.rc_wca = kw['rc_wca']
    ml.sigma = kw['sigma']
    if 'rijz' not in kw:
        kw = {'rijz': args[0], 'orij': args[1]}
    else:
        kw = {'rijz': kw['rijz'], 'orij': kw['orij']}
    return f(**kw)


stOQ = StdQueue('O')
stFQ = mock_open()
stEQ = StdQueue('E')
mm.Process = Process
tester = testbed()
xfail = tester['xfail']
platform = sys.platform
