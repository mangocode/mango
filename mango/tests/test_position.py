import unittest
from unittest import skip

from unittest.mock import patch, Mock, call, MagicMock
from mango.position import force, energy, position, Communal
from mango.constants import c
from mango.tests.magliq import vv_mag, vv_wca, ff_mag, ff_wca, hh_mag, cluster
import mango.tests.magliq as ml
from mango.tests import captured_output, ml_transwrap
from numpy import allclose, random, triu_indices, ones, zeros, zeros_like, arange, array, einsum, sqrt, maximum
from scipy.special import comb
from scipy.constants import physical_constants
from contextlib import ExitStack, suppress
from functools import partial

from mango.tests.numpy_assert import assertNumpyArraysEqual, assertvalueequal


class test_comm(unittest.TestCase):

    def test_setattr(self):
        com = Communal(0, 0, (0, 0), 3, 0)
        com.set_attr(['a', 'b'], ones(2))
        assertNumpyArraysEqual(com.a, ones(2))
        com.a += 1
        assertNumpyArraysEqual(com.a, ones(2) + 1)
        assertNumpyArraysEqual(com.b, ones(2))


class test_position(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(test_position, self).__init__(*args, **kwargs)
        self.size = random.randint(1, 500)
        self.ones = random.rand(self.size, 3)
        self.zeros = zeros_like(self.ones)

    def setUp(self):
        ml.KB = physical_constants["Boltzmann constant"][0] * 1.e13
        ml.GBARE = -physical_constants["electron gyromag. ratio"][0] * 1.e-10  # unit: 1.e6/(gauss*s)
        ml.EPS2 = c.EPS2
        ml.MAXITER = 20
        ml.mitercount = 0

        class cl(cluster):

            def __init__(self):
                pass

        class ps(position):

            def __init__(self):
                self.estrings()

        class blank():
            pass

        self.no_molecules = self.ones.shape[0]
        self.cl = cl
        self.cl.mass = 0.5
        self.cl.Troom = 100
        self.ps = ps
        self.ps.var = blank
        self.ps.var.boxsize = False
        self.ps.flg = blank
        self.ps.var.mass = self.zeros.copy()[:, 0] + self.cl.mass
        self.ps.pos_tmp = self.zeros.copy()
        self.ps.noise_tmp = self.zeros.copy()
        self.ps.invmass = 1 / self.ps.var.mass
        self.ps.var.temp = self.cl.Troom
        self.ps.halfdt = 0.5
        self.ps.var.dt = 1
        self.ps.e = blank
        self.ps.f = blank
        self.comb = comb(self.no_molecules, 2, exact=True)

    def tearDown(self):
        del ml.KB
        del ml.GBARE
        del ml.EPS2
        del ml.MAXITER
        del ml.mitercount

    @skip('Unfinished test')
    def test___init__(self):  # (f, *args, **kwds):
        pass
        # return f(*args, **kwds)

    def __noise(self):
        posit = self.ps()
        posit.c1t = zeros(self.no_molecules)
        posit.c2t = zeros_like(posit.c1t)
        posit.mom = self.ones.copy()
        posit.noise_setup = array([self.ones.copy(), self.ones.copy()])
        posit.noise = posit._noise
        return posit

    def test__noop(self):
        posit = self.ps()
        posit.mom = self.zeros.copy()
        posit._noop(1)
        assertNumpyArraysEqual(posit.mom, self.zeros)

    def test_set_pot(self):
        def blank_func(*args, **kw):
            return 1

        def blank_func2(*args, **kw):
            return 2

        class n():
            pass

        class mpot():
            def __init__(self):
                pass

        npot = n()
        npot.__all__ = ['vvtest', 'fftest']
        npot.vvtest = blank_func2
        npot.fftest = blank_func2
        mpot.vvtest = blank_func
        mpot.fftest = blank_func
        mpot1 = mpot()
        mpot2 = mpot()
        self.ps.e = mpot1
        self.ps.f = mpot2
        self.ps.e.vvtest = blank_func
        self.ps.f.fftest = blank_func
        run = self.ps()
        run.var.potential = True

        with patch('mango.position.energy', mpot1):
            with patch('mango.position.force', mpot2):
                with patch('mango.position.getpotentials', return_value=npot):
                    self.assertEqual(run.e.vvtest(), 1)
                    self.assertEqual(run.f.fftest(), 1)
                    run.set_pot()
                    self.assertEqual(run.e.vvtest(), 2)
                    self.assertEqual(run.f.fftest(), 2)

    def test_initialconditions(self):
        keys = ['pos', 'mom', 'mag', 'force', 'boxsize', 'tri']
        keys2 = keys[:3].copy()
        keys2 += ['neel_count', 'forces']

        def called(lst):
            for i in lst:
                i.assert_called_once()

        def notcalled(lst):
            for i in lst:
                i.assert_not_called()

        def getposit():
            posit = self.ps()
            posit.flg.noise = True
            posit.flg.restart = False
            posit.flg.opt = True
            posit.flg.mag_sw = False
            posit.neel_count = None
            posit.pos = self.ones.copy()
            for i in keys[1:]:
                setattr(posit, i, None)
            posit.flg.pbc = None
            posit.var.sigma = None
            posit.flg.nout = 0
            posit.f.force_vars = lambda *args: args
            posit.boundary = MagicMock()
            with suppress(AttributeError):
                del posit.boundary.wrap
            return posit

        def initfunc():
            with patch.object(posit.boundary, 'wrapping', return_value=self.zeros) as psp:
                with patch.object(posit.boundary, 'distance', return_value=('d', 'dm')) as psd:
                    with patch.object(posit.boundary, 'setup') as pss:
                        with patch.object(posit, 'force_step') as fs:
                            with patch.object(posit, 'optimise') as op:
                                # with patch.object(self.ps, 'conserv') as c:
                                posit.initialconditions()
                                return psp, psd, pss, fs, op, posit.prop

        posit = getposit()
        psp, psd, pss, fs, op, ret_dict = initfunc()
        assertNumpyArraysEqual(ret_dict['pos'], self.zeros)
        self.assertEqual(list(ret_dict.keys()), keys2)
        self.assertEqual(str(posit.noise.__name__), '_noise')
        called([pss, psd, psp, fs])
        notcalled([op])

        posit = getposit()
        posit.flg.noise = False
        posit.flg.restart = True
        psp, psd, pss, fs, op, ret_dict = initfunc()
        self.assertEqual(str(posit.noise.__name__), '_noop')
        called([psp, psd, pss, fs])

        posit = getposit()
        posit.flg.restart = False
        posit.flg.opt = False
        psp, psd, pss, fs, op, ret_dict = initfunc()
        called([psp, psd, pss, fs])
        notcalled([op])

    def test_neel(self):
        magstep = Mock()
        posit = self.ps()
        posit.neel_count = MagicMock()
        posit.neel_count.mag = self.ones.copy()
        posit.mag = self.zeros.copy()
        posit.neel(magstep, posit.var.dt)
        magstep.assert_called_once()
        self.assertEqual(posit.neel_count.run.call_count, 2)
        assertNumpyArraysEqual(posit.mag, self.ones.copy())

    def test_propagate(self):
        alg = ['position_step', 'momentum_step', 'stochastic_step',
               'magnetisation_step', 'f',
               'stochastic_step', 'momentum_step', 'position_step']
        arg = [0.5, 0.5, (0.5, 1), 1, (0.5, 2), 0.5, 0.5]
        arg = [call(i[0], i[1]) if isinstance(i, tuple) else call(i) for i in arg]
        ptc = Mock()
        patches = [patch.object(self.ps, i, ptc)for i in alg]
        posit = self.ps()
        keys = ['pos', 'mom', 'mag', 'neel_count', 'force']
        keys2 = keys.copy()
        keys2[-1] = 'forces'
        keys += ['dist', 'dist_matrix']
        posit.prop = {}
        for i in keys:
            setattr(posit, i, None)

        for i in keys2[:-1]:
            posit.prop[i] = getattr(posit, i)
        posit.prop['forces'] = getattr(posit, 'force')

        posit.Hext = ones((self.no_molecules, 3))
        posit.noise_setup = ones((3, self.no_molecules, 3))

        with ExitStack() as stack:
            for i in patches:
                stack.enter_context(i)
            posit.propagate(zeros((self.no_molecules , 3)), zeros((3, self.no_molecules , 3)))

        print(ptc.call_args_list, arg)
        self.assertEqual(ptc.call_args_list, arg)
        self.assertEqual(list(posit.prop.keys()), keys2)
        assertNumpyArraysEqual(posit.Hext, zeros((self.no_molecules, 3)))
        assertNumpyArraysEqual(posit.noise_setup, zeros((3, self.no_molecules, 3)))

    def test_position_step(self):
        def wrap(f):
            return f

        def getdist(f):
            return zeros((self.comb, 3, 3))
        clust = self.cl()
        posit = self.ps()
        for i in [posit, clust]:
            setattr(i, 'pos', zeros((self.no_molecules, 3)))
            setattr(i, 'mom', ones((self.no_molecules, 3)))
            setattr(i, 'mass', ones(self.no_molecules) / 2)
        clust.mass = clust.mass[0]
        posit.boundary.dist = zeros((self.no_molecules, self.no_molecules, 3))
        posit.boundary.wrap = wrap
        posit.boundary.getdist = wrap
        posit.dist = posit.boundary.dist
        posit.dist_matrix = zeros((self.no_molecules, self.no_molecules))
        posit.position_step(1)
        clust.update_position(1)
        assertNumpyArraysEqual(posit.pos, clust.pos)

    def test_force_step(self):
        posit = self.ps()
        clust = self.cl()

        clust.pos = self.zeros.copy()
        clust.mom = self.zeros.copy()
        posit.force = self.zeros.copy()

        class f():

            def __init__(self):
                self.ff_trans = self.ff_mag = self._ff_trans_return = self.func

            def func(*args):
                return self.ones / 2

        posit.f = f()

        for i in ['rijz', 'orij', 'mag']:
            setattr(clust, i, None)

        for i in ['dist', 'dist_matrix', 'mag']:
            setattr(posit, i, None)

        with patch('mango.tests.magliq.ff_wca', return_value=self.ones / 2):
            with patch('mango.tests.magliq.ff_mag', return_value=self.ones / 2):
                clust.update_momentum(1)

        posit.force_step()

        assertNumpyArraysEqual(posit.force, clust.mom)

    def test_momentum_step(self):  # (f, *args, **kwds):
        class psm(self.ps):

            def force_step(self):
                pass

        clust = self.cl()
        posit = psm()

        posit.force = self.ones.copy()
        clust.pos = self.zeros.copy()
        clust.mom = self.zeros.copy()
        posit.mom = self.zeros.copy()

        for i in ['rijz', 'orij', 'mag']:
            setattr(clust, i, None)

        with patch('mango.tests.magliq.ff_wca', return_value=self.ones / 2):
            with patch('mango.tests.magliq.ff_mag', return_value=self.ones / 2):
                clust.update_momentum(posit.var.dt)

        posit.momentum_step(posit.var.dt)

        assertNumpyArraysEqual(posit.mom, clust.mom)

    def test_stochastic_step(self):
        clust = self.cl()
        posit = self.__noise()
        clust.gammat = 1
        posit.stoc_constant = zeros_like(self.ones[:, 0]) + 1
        clust.mom = self.ones.copy()
        posit.mom = self.ones.copy()

        with patch('mango.tests.magliq.np.random.standard_normal', return_value=self.ones.copy()):
            clust.update_stochastic(1)
        posit.stochastic_step(1, 1)
        assertNumpyArraysEqual(clust.mom, posit.mom)

    @skip('Unfinished test')
    def test_optimise(self):
        pass

    def test_magnetisation_step(self):  # (f, *args, **kwds):
        alpha = -random.random()
        Ms = random.random()
        vol = random.random()
        geff = random.random()
        clust = self.cl()
        clust.mag = self.ones.copy()
        clust.alpha = alpha
        clust.Ms = Ms
        clust.geff = geff
        clust.rijz = clust.orij = None
        clust.hvol = vol
        clust.count = 0

        posit = self.ps()
        posit.mag = self.ones.copy()
        posit.dm_umag = self.zeros.copy()
        posit.dm_Heff = self.zeros.copy()
        posit.dm_hfield = self.zeros.copy()
        posit.dm_mag_old = self.zeros.copy()
        posit.dm_diff = self.zeros.copy()
        posit.new_norm = [0, 0]
        posit.count = 0
        posit.scfrange = range(0, 20)
        posit.Mmag = sqrt(einsum("iz,iz->i", self.ones.copy(), self.ones.copy()))
        posit.Hext = self.ones.copy()
        posit.var.alpha = zeros(self.no_molecules) + alpha
        posit.var.geff = zeros(self.no_molecules) + geff
        posit.inv_Mmag2 = 1 / einsum('i,i -> i', posit.Mmag, posit.Mmag)
        posit.inv_Mmag2_atm = posit.inv_Mmag2 / posit.var.mass
        posit.sqEPS = c.EPS**2

        magliqhhmag = self.ones - (self.ones * sqrt(2. * ml.KB * clust.Troom * clust.alpha / (clust.Ms * ml.GBARE * clust.hvol * 1)))
        with patch('mango.tests.magliq.hh_mag', return_value=magliqhhmag):
            with patch('mango.tests.magliq.np.random.standard_normal', return_value=self.ones.copy()):
                clust.update_magnetisation(1)

        posit.f.hh_mag = lambda args: self.ones
        posit.magnetisation_step(1)

        assertNumpyArraysEqual(clust.mag, posit.mag)


class energy_test(unittest.TestCase):

    def setUp(self):
        self.epsilon = random.rand()
        self.sigma = random.rand()
        self.no_molecules = random.randint(1, 500)
        self.tri = triu_indices(n=self.no_molecules, k=1)
        self.limit = 1 / (self.sigma * 2 ** (1 / 6))
        self.d = random.rand(self.no_molecules, self.no_molecules, 3)
        self.dm = random.rand(self.no_molecules, self.no_molecules)
        self.mag = random.rand(self.no_molecules, 3)
        self.comb = comb(self.no_molecules, 2, exact=True)
        com = Communal(self.epsilon, self.sigma, self.tri, self.comb, self.limit)
        self.e = energy(com)
        self.vv_wca = partial(ml_transwrap, vv_wca, epsilon=self.epsilon, sigma=self.sigma)

    def test_vv_trans(self):
        self.e.energy_vars(self.dm)
        self.e.vv_trans()
        assertvalueequal(einsum('i->',self.e.epot_tr), self.vv_wca(rijz=self.d, orij=self.dm, rc_wca=1 / self.limit))
        self.limit = c.EPS2
        self.e.limit = c.EPS2
        self.e.energy_vars(self.dm)
        self.e.vv_trans()
        self.assertTrue(allclose(einsum('i ->',self.e.epot_tr), self.vv_wca(rijz=self.d, orij=self.dm, rc_wca=1 / self.limit)))

    def test_vv_mag(self):
        self.e.energy_vars(self.dm)
        assertvalueequal(einsum('i ->',self.e.vv_mag(self.mag.copy(), self.d)), vv_mag(self.d, self.dm, self.mag.copy()))


class force_test(unittest.TestCase):

    def setUp(self):
        self.epsilon = random.rand()
        self.sigma = random.rand()
        self.no_molecules = random.randint(1, 500)
        self.tri = triu_indices(n=self.no_molecules, k=1)
        self.limit = 1 / (self.sigma * 2 ** (1 / 6))
        self.d = random.rand(self.no_molecules, self.no_molecules, 3)
        self.dm = 1 / maximum(sqrt(einsum('ijz,ijz->ij', self.d, self.d)), c.EPS2)
        self.mag = random.rand(self.no_molecules, 3)
        self.comb = comb(self.no_molecules, 2, exact=True)
        com = Communal(self.epsilon, self.sigma, self.tri, self.comb, self.limit)
        self.f = force(com, self.no_molecules)
        self.ff_wca = partial(ml_transwrap, ff_wca, epsilon=self.epsilon, sigma=self.sigma)

    @skip('Unfinished test')
    def test___init__(self):  # (f, *args, **kwds):
        pass
        # return f(*args, **kwds)

    def test_ff_trans(self):
        self.f.force_vars(self.d, self.dm)
        assertNumpyArraysEqual(self.f._ff_trans_return(), self.ff_wca(rijz=self.d, orij=self.dm, rc_wca=1 / self.limit))
        self.limit = c.EPS2
        self.f.limit = c.EPS2
        self.f.force_vars(self.d, self.dm)
        assertNumpyArraysEqual(self.f._ff_trans_return(), self.ff_wca(rijz=self.d, orij=self.dm, rc_wca=1 / self.limit))

    def test_ff_mag(self):
        self.f.force_vars(self.d, self.dm)
        assertNumpyArraysEqual(self.f.ff_mag(self.mag), ff_mag(self.d, self.dm, self.mag))

    def test_hh_mag(self):
        self.f.force_vars(self.d, self.dm)
        assertNumpyArraysEqual(self.f.hh_mag(self.mag), hh_mag(self.d, self.dm, self.mag))


if __name__ == '__main__':
    unittest.main(verbosity=2)
