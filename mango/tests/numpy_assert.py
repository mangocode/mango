from numpy import allclose, ndarray, finfo, float64
__unittest = True

EPS2 = finfo(float64).eps


def assertNumpyArraysEqual(one, two, tol=EPS2):
    assertisarray(one, two)
    assertshapeequal(one, two)
    assertvalueequal(one, two, tol)

def assertshapeequal(one, two):
    if one.shape != two.shape:
        raise AssertionError(f"Shapes don't match {one.shape}, {two.shape}")


def assertvalueequal(one, two, tol=EPS2):
    if not allclose(one, two, atol=tol):
        raise AssertionError("Elements don't match!\n tol:\n{}\n one:\n{}\n two:\n{}\n diff:\n{}".format(tol, one, two, one - two))


def assertisarray(one, two):
    if not isinstance(one, ndarray):
        raise AssertionError("One is not an array")
    if not isinstance(two, ndarray):
        raise AssertionError("Two is not an array")
