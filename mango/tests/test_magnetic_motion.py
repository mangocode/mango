from contextlib import suppress
from copy import deepcopy
import unittest
from unittest import skip
from unittest.mock import MagicMock, Mock, patch
import numpy as np
from mango.magnetic_motion import (num_iters, dictionary_creation,
                                   field, save_m, calculate)
from mango.constants import _variables
from mango.tests.numpy_assert import assertNumpyArraysEqual
from mango.tests import captured_output


# class Neel_Relax(unittest.TestCase):

#     def setUp(self):
#         self.counter = countneel(1, 1, 2)

#     def test___init__(self):
#         assertNumpyArraysEqual(self.counter.count, np.zeros(2))

#     def test_run(self):
#         A_save = self.counter.A.copy()
#         self.counter.run(0.1, np.zeros(2))
#         self.assertNotEqual(self.counter.A, A_save)

#         A_save = self.counter.A.copy()
#         self.counter.run(0.1, np.zeros(2))
#         self.assertEqual(self.counter.A, A_save)

#         self.counter.run(1000, np.ones(2))
#         assertNumpyArraysEqual(self.counter.mag, -np.ones(2))
#         self.assertGreater(self.counter.count[0], 0)
#         self.assertGreater(self.counter.count[1], 0)


class functionmotion(unittest.TestCase):

    def test_numiters(self):
        self.assertEqual(num_iters(4, 5), 1)
        self.assertEqual(num_iters(5, 5), 1)
        self.assertEqual(num_iters(6, 5), 2)
        self.assertEqual(num_iters(10, 5), 2)
        self.assertEqual(num_iters(11, 5), 3)

    @skip('Unfinished test')
    def test_integrate(self):  # (f, *args, **kwds):
        pass
        # return f(*args, **kwds)

    def test_save_m(self):
        save = save_m(MagicMock(), MagicMock())
        tD = save.memory(MagicMock(), MagicMock(), time)
        # TODO assertions
        save = save_m(MagicMock(), MagicMock())
        tD = save.memory(MagicMock(), MagicMock(), time)
        # TODO assertions

    def test_dictionary_creation(self):  # (f, *args, **kwds):
        test_flags = _variables(**{"hello": True, "neel": True})
        test_vars = _variables(**{"savechunk": 1, "name": "test", "finishtime": "Now", "defaults": None, "time": 0})

        for nm in range(1, 4):
            test_vars.no_molecules = nm
            testdict = dictionary_creation(test_vars, test_flags)
            testextra = dictionary_creation(test_vars, test_flags, extra=1)

            self.assertEqual(testdict['iter_time'].shape, (1, 2))

            for dictionary in [testdict, testextra]:

                # self.assertEqual(dictionary['energy']['kineticm'].shape, (1, nm))

                for i in ['position', 'magnetisation', 'forces', 'momentum']:
                    self.assertEqual(dictionary[i].shape, (1, nm, 3))
                # for i in ['CoM', 'CoM_vel']:
                #     self.assertEqual(dictionary[i].shape, (1, 3))
                # for i in ['total', 'total_mag', 'total_angular']:
                #     self.assertEqual(dictionary['momenta'][i].shape, (1, 3))
                # for i in ['kinetic', 'trans_pot', 'mag_pot', 'total']:
                #     self.assertEqual(dictionary['energy'][i].shape, (1,))

                self.assertEqual(dictionary['neel_relaxation'].shape, (1, nm))

            for i in ['finishtime', 'defaults', 'time', 'name']:
                with self.assertRaises(KeyError):
                    testdict['vars'][i]

            # self.assertFalse(testextra['vars']['RandNoState'])

        self.assertEqual(dictionary_creation(test_vars, test_flags, extra=0), None)


class calculation(unittest.TestCase):
    def test_field(self):  # (f, *args, **kwds):
        for i in [0, 1e6]:
            self.assertEqual(field(i, nu=0), 167.0)
        self.assertEqual(field(0.5, 2, 0.5), 2)

        axis = np.zeros(3)
        axis[2] = 1

        class calctest(calculate):

            def __init__(self):
                self.h_axis = axis.copy()
                self.Hext = np.zeros(3)

                class time():
                    time = 0.5

                class var():
                    H_0 = 2
                    nu = 0.5

                self.time = time()
                self.var = var()

        ctest = calctest()

        ctest._returnextfield()
        assertNumpyArraysEqual(ctest.Hext, axis * 2)

        ctest._returnnoextfield()
        assertNumpyArraysEqual(ctest.Hext, axis)

    def test_noise(self):
        noise = np.zeros((3, 3, 3))
        Hext = np.ones((3, 3))

        class calctest(calculate):

            def __init__(self):
                self._returnnonoise = self._noop
                self.noise_setup = noise.copy()
                self.Hext = Hext.copy()

                class var():
                    no_molecules = 3
                    c2 = np.ones(3)

                self.var = var()

            def Error(self, err):
                print(err)

        ctest = calctest()
        ctest._returnnonoise()
        assertNumpyArraysEqual(ctest.noise_setup, noise)
        assertNumpyArraysEqual(ctest.Hext, Hext)
        ctest._returnnoise()

        with self.assertRaises(AssertionError):
            assertNumpyArraysEqual(ctest.noise_setup, noise)
        with self.assertRaises(AssertionError):
            assertNumpyArraysEqual(ctest.Hext, Hext)

    def test_calculate(self):

        class copywrite():

            def __init__(self, *args, **kwargs):
                self.saveargs = {}
                self.key = 0

            def save(self, *args, **kw):
                self.saveargs[self.key] = deepcopy(args[0])
                self.key += 1

        def Error(*args):
            print(args)
        cw = copywrite()
        wd = MagicMock()
        posit = MagicMock()
        var = MagicMock()
        flg = MagicMock()
        timer = MagicMock()
        np.random.seed(12345)
        part_array = np.random.random((2, 3))
        skip = 10
        nmax = 107
        savechunk = 5

        skip_iters = num_iters(nmax, skip)
        written = np.append(np.arange(1, skip_iters + 1, savechunk), skip_iters + 1)
        prop_dict = {"pos": part_array.copy(), "mom": part_array.copy(), "mag": part_array.copy(),
                     "neel_count": False, "forces": part_array.copy(), 'iter_time': np.array((10, 2))}
        prop_dict['energy'] = {"total": np.zeros(3), "totalmag": np.zeros(3), "angular": np.zeros(3),
                               "kinetic": 0, "trans_pot": 0, "mag_pot": 0,
                               "total_E": 0, "CoM": np.zeros(3), "CoM_vel": 0, "ekin_m": np.zeros(2)}
        test_vars = {"savechunk": savechunk, "name": "test", "finishtime": "Now", "defaults": None, "time": time, 'RandNoState': {},
                     "no_molecules": 2, "nmax": nmax, "extra_iter": 0, "skip": skip, "skip_iters": skip_iters, "dt": 10, "stats": [1]}
        test_flgs = {"noise": False, "suscep": True, "field": False, "restart": None, "neel": False, "prog": False, "nout": 0, "save_type": "hdf5"}

        posit.prop = prop_dict
        posit.initialconditions.return_value = prop_dict
        timer.gettimegone.return_value = 999
        timer.gettimeleft.return_value = 999

        for i, j in test_vars.items():
            var.__dict__[i] = j
        for i, j in test_flgs.items():
            flg.__dict__[i] = j

        prefix = "mango.magnetic_motion."
        with captured_output() as (out, err):
            with patch(prefix + "_time"):
                with patch(prefix + "c.random") as cr:
                    cr.state = None
                    with patch(prefix + 'write_data') as wd:
                        wd().setup.return_value = "hdf5"
                        wd().write.side_effect = cw.save
                        calc = calculate(**{"posit": posit,
                                            "mag": prop_dict['mag'], "pos": prop_dict['pos'], "mom": prop_dict['mom'],
                                            "var": var, "flg": flg, "timer": timer})
                        calc.run(0)

        numwrites = wd().write.call_count
        data = cw.saveargs
        it_tish = forcessh = possh = magsh = momsh = 0
        name = 'test1.hdf5'
        origvar = {}
        for call in data.keys():
            origvar[call] = {}
            it_tish += data[call]['iter_time'].shape[0]
            forcessh += data[call]['forces'].shape[0]
            possh += data[call]['position'].shape[0]
            magsh += data[call]['magnetisation'].shape[0]
            momsh += data[call]['momentum'].shape[0]
            self.assertEqual(data[call]['name'], name)

            # Length test keys
            for i in ['iter_time', 'forces', 'name', 'position', 'magnetisation', 'momentum']:
                del data[call][i]

            with suppress(KeyError):
                data[call]['flags'] = removeempty(data[call]['flags'])

            data[call]['vars'] = removeempty(data[call]['vars'])

            _mockkeys = list(data[call]['vars'].keys())
            for i in _mockkeys:
                if i not in ['_mock_mock_calls', 'method_calls']:
                    origvar[call][i] = data[call]['vars'][i]

        mockvars = {}
        for call in origvar:
            callkeys = list(origvar[call].keys())
            mockvars[call] = {}
            for key in callkeys:
                if key.startswith('_mock'):
                    mockvars[call][key] = origvar[call][key]
                    del origvar[call][key]
                assert key not in ['finishtime', 'defaults', 'time', 'name']

        for lentest in [it_tish, forcessh, possh, magsh, momsh]:
            self.assertEqual(lentest, origvar[call]['written'])

        self.assertEqual(numwrites, calc.writes + 2)

        keyslist = list(origvar.keys())
        print(keyslist)
        w_count = [1, 6, 11, 12]
        for call, value in origvar.items():
            self.assertEqual(w_count[call], written[call])
            if call not in [0, keyslist[-1]]:
                self.assertEqual(value['skip_iters'], skip_iters)


class time:

    def iter():
        return 0

    def time():
        return 0


def removeempty(data):
    _mockkeys = list(data.keys())
    for i in _mockkeys:
        if ((i.startswith(("_mock", "_spec")) or i == 'method_calls') and
                str(data[i]) in ['None', '0', '[]', '{}', 'sentinel.DEFAULT', '', 'False']):
            del data[i]
    return data


if __name__ == '__main__':
    unittest.main(verbosity=2)
