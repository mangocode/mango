import unittest
from unittest.mock import patch, mock_open
from io import StringIO
from numpy import array_str, array, zeros, random, einsum

import mango

from mango.tests import captured_output
from mango.tests.test_arguments import helpout
from mango.tests import magliq as ml
from mango.tests.magliq import constants, cluster
from mango.tests.numpy_assert import assertvalueequal, assertNumpyArraysEqual


class init(unittest.TestCase):

    def test_help(self):
        with captured_output() as (out, err):
            with patch('mango.arguments.c._banner', return_value=''):
                with self.assertRaises(SystemExit) as se:
                    mango.help()
            self.assertEqual(se.exception.code, 0)
            output = out.getvalue()
            assert output.startswith(helpout('cli'))


class regtest(unittest.TestCase):

    def setUp(self):
        self.nm = 2

        with captured_output() as (out, err):
            with patch('mango.magnetic_motion.mp_handle') as mp_patch:
                with patch('mango.arguments.listdir', return_value=[]):
                    with patch('mango.arguments.makedirs'):
                        with patch('mango.io.open', mock_open()):
                            mango.run({'Iterations': 10, "NumberParticles": self.nm, 'ParticleRadius': 0.5,
                                       'Boxsize': 400, 'Timestep': 200, 'Temperature': 300, "SkipSave": 100,
                                       'Optimisation': False})

        print(out.getvalue(), err.getvalue(), mp_patch.call_count)
        data = mp_patch().call_args_list[0][0][1].__dict__
        data['posit'].initialconditions()

        # dictcreate = data['dictcreate']
        del data['dictcreate']

        import_set = {'physical_constants', '__file__', 'exyz', 'cluster', '__name__', 'hh_mag',
                      'fmin_slsqp', 'stdout', '__loader__', '__builtins__', 'exit', '__cached__',
                      'ff_wca', '__package__', '__doc__', 'constants', 'vv_wca', 'ff_mag', 'main',
                      'vv_mag', '__spec__', 'update_dist', 'np'}

        # self.assertEqual(set(ml.__dict__.keys()), import_set)

        with captured_output() as (mlout, mlerr):
            (ml.AMU, ml.KB, ml.GBARE, ml.BMAG, ml.EPS1, ml.EPS2,
             ml.epsilon, ml.sigma, ml.rc_wca, ml.skip, ml.dt, ml.mnp_radius,
             ml.MAXITER, ml.mitercount) = constants()

        ml.dt = data['var'].dt
        ml.mnp_radius = data['var'].radius[0]
        ml.skip = data['var'].skip
        ml.rc_wca = 2 * data['var'].radius[0]
        ml.epsilon = data['var'].epsilon
        ml.sigma = ml.rc_wca / (2.**(1. / 6.))
        # ml.GBARE *= 1000

        with captured_output() as (clusto, cluste):
            with patch('mango.tests.magliq.cluster.read_frame', return_value=(data['posit'].prop['pos'].copy(),
                                                                              data['posit'].prop['mom'].copy(),
                                                                              data['posit'].prop['mag'].copy())):
                mldata = cluster('', data['var'].temp)
                mldata.natom = self.nm

        self.data = data
        self.mldata = mldata
        self.import_set = import_set

        self.pmm_check()

    def tearDown(self):
        for i in ('AMU', 'KB', 'GBARE', 'BMAG', 'EPS1', 'EPS2', 'dt', 'mnp_radius',
                  'epsilon', 'sigma', 'rc_wca', 'skip', 'MAXITER', 'mitercount'):
            delattr(ml, i)

    def test_constants(self):

        pmm = ['pos', 'mom', 'mag']
        dkeys = self.data['var'].__dict__.keys()
        mlkeys = self.mldata.__dict__.keys()
        constkeys = set(ml.__dict__.keys()).difference(self.import_set)
        mliqkeys = constkeys.intersection(dkeys)
        dkeys_ml = set(dkeys).intersection(mlkeys)
        dpkeys_ml = set(self.data['posit'].__dict__.keys()).intersection(mlkeys).difference(pmm)

        namekeys = {'temp': 'Troom', 'no_molecules': 'natom', 'boxsize': 'box', 'ms': 'Ms', 'Mdens': 'Mdens'}
        namekeys2 = {'stoc_constant': 'gammat'}
        namekeys3 = {'limit': 'rc_wca'}
        namekeys4 = {'KB': 'KB', 'GBARE': 'GBARE', 'EPS': 'EPS1', 'EPS2': 'EPS2', }
        frivkeys = {'directory', 'defaults', 'name', 'logs', 'finishtime', 'savechunk', 'stats', 'nmax', 't0',
                    'extra_iter', 'location', 'potential', 'RandNoState', 'Error', 'scfiter', 'skip_iters',
                    "R", "inputfile", "walltime", "c2"}
        notimpl_ml = {'temp0', 'tauN_0', 'keff', 'phi', 'theta', 'chi0', 'nu', 'H_0', 'tauB', 'block'}
        notimpl_mgo = {'mitercount', 'BMAG', 'AMU', 'MAXITER', 'mnp_radius'}

        tested = {}

        tuple1 = (self.data['var'], self.mldata)
        tuple2 = (self.data['posit'], self.mldata)
        tuple3 = (self.data['var'], ml)
        types = [[dkeys_ml, tuple1], [dpkeys_ml, tuple2], [mliqkeys, tuple3]]
        nametypes = [[namekeys, tuple1], [namekeys2, tuple2], [namekeys3, tuple3], [namekeys4, (mango.c, ml)]]

        for (tpe, (d, m)) in types:
            for i in tpe:
                tested = asserteq(tested, d.__dict__[i], m.__dict__[i], i)

        for i in pmm:
            tested = asserteq(tested, self.data['posit'].__dict__[i], self.mldata.__dict__[i], i, Npy=True)
            tested = asserteq(tested, self.data['posit'].__dict__[i], self.data['posit'].prop[i], i, Npy=True)

        for (tpe, (d, m)) in nametypes:
            for i, j in tpe.items():
                if i == 'Mdens':
                    tested = asserteq(tested, d.ms / d.dens, m.Mdens, 'Mdens')
                elif i == 'limit':
                    tested = asserteq(tested, d.__dict__[i], 1 / m.__dict__[j], i, j)
                else:
                    tested = asserteq(tested, d.__dict__[i], m.__dict__[j], i, j)

        untested_dk = set(dkeys).difference(tested.keys()).difference(frivkeys).difference(notimpl_ml)

        untested_mlk = set(mlkeys).difference(tested.values())

        untested_ml = set(constkeys).difference(tested.values()).difference(notimpl_mgo)

        self.assertEqual(set(), untested_ml)
        self.assertEqual(set(), untested_mlk)
        self.assertEqual(set(), untested_dk)

    def test_optimise(self):
        # No thermal tested
        self.pmm_check()
        with captured_output() as (out, err):
            self.data['posit'].optimise(toll=mango.c.EPS * 1e-5)
            # This fails due to machine error on functions unless iters are set higher
            self.mldata.optimise(toll=mango.c.EPS * 1e-5, iters=20000)

        self.pmm_check(tol=mango.c.EPS)

    def test_propagate(self):
        class time():
            def __init__(self):
                self.iter = 0

        tm = time()
        self.data['posit'].neel_count = None
        self.data['posit'].time = tm
        self.data['posit'].propagate(zeros((self.nm, 3)), zeros((3, self.nm, 3)))

        # with captured_output() as (out, err):
        with patch('mango.tests.magliq.open', mock_open()):
            with patch('mango.tests.magliq.np.random.standard_normal', return_value=zeros((self.nm, 3))):
                self.mldata.propagate(1, dt=ml.dt, skip=ml.skip)

        self.pmm_check()

        randoms = random.standard_normal(size=(self.nm, 3))
        self.data['posit'].propagate(einsum('ij,i->ij ', randoms, self.data['var'].c2), array([randoms, randoms, randoms]))

        with captured_output() as (out, err):
            with patch('mango.tests.magliq.open', mock_open()):
                with patch('mango.tests.magliq.np.random.standard_normal', return_value=randoms):
                    self.mldata.propagate(1, dt=ml.dt, skip=ml.skip)

        self.pmm_check()

    def pmm_check(self, tol=mango.c.EPS2):
        assertNumpyArraysEqual(self.data['posit'].pos, self.mldata.pos, tol=tol)
        assertNumpyArraysEqual(self.data['posit'].mom, self.mldata.mom, tol=tol)
        assertNumpyArraysEqual(self.data['posit'].mag, self.mldata.mag, tol=tol)


def create_dummyxyz(pos_r, mom_r, mag_r):
    def stripper(arr):
        return array_str(arr).replace('[', '').replace(']', '').split('\n')

    xyzstring = '{}\n#comment\n'.format(pos_r.shape[0])

    pos = stripper(pos_r)
    mom = stripper(mom_r)
    mag = stripper(mag_r)

    for i in range(pos_r.shape[0]):
        xyzstring += "MNP " + pos[i] +" "+ mom[i] +" "+ mag[i] + '\n'

    return StringIO(xyzstring)


def asserteq(store, i, j, ni, nj=None, Npy=False):
    if nj is None:
        a0 = f'(key: {ni})'
        nj = ni
    else:
        a0 = f'(key: {ni} {nj})'
    try:
        if Npy:
            assertNumpyArraysEqual(i, j)
        else:
            assertvalueequal(i, j)
    except AssertionError as AE:
        args = list(AE.args)
        args[0] += a0
        AE.args = tuple(args)
        raise AE

    store[ni] = nj
    return store


if __name__ == '__main__':
    unittest.main(verbosity=2)
