import unittest
from os.path import dirname

from mango.imports import read, write, fftw, getpotentials, matplotlib
from mango.managers import start_server
from mango.tests import stEQ, captured_output, xfail, platform
from mango.errors import _strings
from mango.constants import c
import mango.potentials


class imports(unittest.TestCase):

    def setUp(self):
        c.Error = start_server("Error")

    def tearDown(self):
        c.Error("END")

    def _readwritemod(self, func):
        ftype = {'hdf5': 'mango.hdf5io', 'pkl': '_pickle', 'txt': None}

        for i in ftype:
            out, savetype = func(i)
            try:
                self.assertEqual(i, savetype)
                self.assertEqual(out.__module__, ftype[i])
            except AttributeError:
                self.assertEqual(out, ftype[i])

    @xfail(platform == "darwin",reason='NotImplementedError on Mac OSX because of broken sem_getvalue', raises=NotImplementedError)
    def test_write(self):
        errors = []
        self._readwritemod(write)
        self.assertEqual(errors, stEQ.read())

    def test_read(self):
        self._readwritemod(read)

    def test_fftw(self):
        errors = [['  Message: Using scipy fft for Susceptibility\n' +
                   '            install fftw and pyfftw for better performance\n',]]
        fft, ffts, ifft, bytealign = fftw()
        try:
            import pyfftw
            self.assertEqual(fft.__module__, 'pyfftw.interfaces.scipy_fftpack')
            self.assertEqual(ifft.__module__, 'pyfftw.interfaces.scipy_fftpack')
        except ImportError:
            self.assertEqual(fft.__module__, 'mango.imports')
            self.assertEqual(ifft.__module__, 'mango.imports')
            self.assertEqual(errors[0], stEQ.read())
        self.assertEqual(ffts.__name__, 'scipy.fftpack')

    def test_mpl(self):
        # Needs properly testing
        mpl = matplotlib()
        mpl.suscep()
        mpl.col()
        mpl.showg()
        mpl.close()

    @xfail(platform == "darwin",reason='NotImplementedError on Mac OSX because of broken sem_getvalue', raises=NotImplementedError)
    def test_getpot(self):
        testp = 'magliq_debug'
        testp_loc = dirname(mango.potentials.__file__) + '/magliq_debug.py'
        string = ['mango.potentials.', '']
        errors = [['  Message: Loaded mango.potentials.magliq_debug\n'],
                  ['  Message: Loaded magliq_debug\n']]
        for e in range(len(errors)):
            errors[e][:0] = ['  Message: Debugging run\n', ]
            errors[e] += ['  Warning: function force_vars not replaced.\n', '  Warning: function energy_vars not replaced.\n']

        for i, j in enumerate([testp, testp_loc]):
            module = getpotentials(j)
            self.assertEqual(module.__name__, string[i] + testp)
            self.assertEqual(errors[i], stEQ.read())


if __name__ == '__main__':
    unittest.main(verbosity=2)
