import unittest
from unittest import skip
from unittest.mock import Mock, patch

from mango.mango import main, _main_run
from mango.tests import captured_output


class testmain(unittest.TestCase):

    @skip('Unfinished test')
    def test_inquirer(self):
        pass
    #     inquirer()

    def test_main(self):
        class timer:

            def finished():
                pass
        with captured_output() as (out, err):
            with patch('mango.arguments.makedirs'):
                with patch('mango.arguments.listdir', return_value=[]):
                    with patch('mango.mango.integrate') as mi:
                        mi.return_value = [timer, 'name']
                        main(False, {})
        # TODO assertions


if __name__ == '__main__':
    unittest.main(verbosity=2)
