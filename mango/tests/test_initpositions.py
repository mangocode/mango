import unittest
import urllib

from mango.constants import c
from mango.initpositions import urllj, initpos
from mango.tests.numpy_assert import assertNumpyArraysEqual
from mango.tests import stEQ, xfail, platform
from mango.managers import start_server
from mango.errors import ExitQuiet, _strings
from numpy import genfromtxt, zeros, concatenate
# from urllib import error as urlerror
from os import path
from io import StringIO, BytesIO


class positions(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(positions, self).__init__(*args, **kwargs)
        self.lj5_loc = "{}/5particles_lj.txt".format(path.dirname(path.realpath(__file__)))
        self.lj5_locwc = "{}/5particles_ljwc.txt".format(path.dirname(path.realpath(__file__)))
        self.lj5_xyz = "{}/5particles_lj.xyz".format(path.dirname(path.realpath(__file__)))
        self.lj5_bxyz = "{}/5particles_lj_broken.xyz".format(path.dirname(path.realpath(__file__)))
        self.lj5_6cxyz = "{}/5particles_lj_6col.xyz".format(path.dirname(path.realpath(__file__)))
        self.lj5_9cxyz = "{}/5particles_lj_9col.xyz".format(path.dirname(path.realpath(__file__)))
        self.lj5 = genfromtxt(self.lj5_loc)
        self.sigma = 10

    def setUp(self):
        c.Error = start_server("Error")
        previous = MockHTTPHandler.install()
        self.addCleanup(MockHTTPHandler.remove, previous)

    def tearDown(self):
        c.Error("END")

    def test_initpos(self):
        sigma5 = zeros(5) + self.sigma
        sigma1 = zeros(1) + self.sigma

        (pos, mag, mom), xyz = initpos(5, sigma5)
        assertNumpyArraysEqual(pos, self.lj5 * sigma5[:, None])
        (pos, mag, mom), xyz = initpos(5, sigma5, location=self.lj5_loc)
        assertNumpyArraysEqual(pos, self.lj5 * sigma5[:, None])
        (pos, mag, mom), xyz = initpos(5, sigma5, location=self.lj5_locwc)
        assertNumpyArraysEqual(pos, self.lj5 * sigma5[:, None])
        (pos, mag, mom), xyz = initpos(5, sigma5, location=self.lj5_xyz)
        assertNumpyArraysEqual(pos, self.lj5)
        (pos, mag, mom), xyz = initpos(5, sigma5, location=self.lj5_6cxyz)
        assertNumpyArraysEqual(concatenate((pos, mag), axis=1), concatenate((self.lj5, self.lj5), axis=1))
        (pos, mag, mom), xyz = initpos(5, sigma5, location=self.lj5_9cxyz)
        assertNumpyArraysEqual(concatenate((pos, mag, mom), axis=1), concatenate((self.lj5, self.lj5, self.lj5), axis=1))
        (pos, mag, mom), xyz = initpos(1, sigma1)
        assertNumpyArraysEqual(pos, zeros((1, 3)))

    def test_init_2p(self):
        twoarray = zeros((2, 3))
        box = 100
        twoarr_BB = twoarray.copy()
        twoarr_BB[0, :] = [-self.sigma / 4, 0, 0]
        twoarr_BB[1, :] = [self.sigma / 4, 0, 0]
        (pos, mag, mom), xyz = initpos(2, self.sigma, boxsize=box)
        assertNumpyArraysEqual(pos, twoarr_BB)
        box = 5
        twoarr_LB = twoarray.copy()
        twoarr_LB[0, :] = [-box / 8, 0, 0]
        twoarr_LB[1, :] = [box / 8, 0, 0]
        (pos, mag, mom), xyz = initpos(2, self.sigma, boxsize=box)
        assertNumpyArraysEqual(pos, twoarr_LB)

    @xfail(platform == "darwin",reason='NotImplementedError on Mac OSX because of broken sem_getvalue', raises=NotImplementedError)
    def test_init_Errors(self):
        location = ["./not_a_file", self.lj5_bxyz, False]
        errors = ["{}Unable to find location file '{}'\n\n".format(_strings._F[0], location[0]),
                  '{}Input array shape not consistant with number of molecules specified\n\n'.format(_strings._F[0]),
                  '{}This is only setup for under 150 particles\n'.format(_strings._F[0]) +
                  '{}for larger systems please specify a position file.\n'.format(_strings.tab + _strings._F[1] * ' ') +
                  '{}Larger structures may be available at'.format(_strings.tab + _strings._F[1] * ' ') +
                  ' http://doye.chem.ox.ac.uk/jon/structures/LJ/\n\n']
        nums = [5, 5, 151]

        for no, err, loc in zip(nums, errors, location):
            self.setUp()
            self._init_Errors(no, err, loc)
            self.tearDown()

    def _init_Errors(self, i, error, loc=False):
        with self.assertRaises(ExitQuiet):
            if loc:
                initpos(i, self.sigma, location=loc)
            else:
                initpos(i, self.sigma)

        self.assertEqual(error, stEQ.read()[0])

    @xfail(platform == "darwin",reason='NotImplementedError on Mac OSX because of broken sem_getvalue', raises=NotImplementedError)
    def test_urllj(self):  # (f, *args, **kwds):
        errors = ['{}Not found online for 500 molecules\n\n'.format(_strings._F[0]),
                  '{}Internet Connection Error, Particle locations'.format(_strings._F[0]) +
                  ' cannot be downloaded\n\n']

        assertNumpyArraysEqual(urllj(5), self.lj5)
        with self.assertRaises(ExitQuiet):
            urllj(500)
        self.assertEqual(errors[0], stEQ.read()[0])

        self.setUp()
        with self.assertRaises(ExitQuiet):
            urllj(5000)
        self.assertEqual(errors[1], stEQ.read()[0])


class MockHTTPHandler(urllib.request.HTTPHandler):

    def mock_response(self, req):
        url = req.get_full_url()

        if url.endswith('500'):

            raise urllib.error.HTTPError(url, 404, 'File Not Found', '', StringIO())
        elif url.endswith('5000'):
            raise urllib.error.URLError(url)
        elif url.endswith('5'):
            file = "{}/5particles_lj.txt".format(path.dirname(path.realpath(__file__)))
            with open(file, 'rb') as r:
                resp = urllib.request.addinfourl(BytesIO(r.read()), {'Content-Type': 'application/txt'}, url, 200)
                resp.msg = 'OK'
            return resp
        raise RuntimeError('Unhandled URL', url)

    http_open = mock_response

    @classmethod
    def install(cls):
        previous = urllib.request._opener
        urllib.request.install_opener(urllib.request.build_opener(cls))
        return previous

    @classmethod
    def remove(cls, previous=None):
        urllib.request.install_opener(previous)


if __name__ == '__main__':
    unittest.main(verbosity=2)
