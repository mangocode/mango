import unittest
from unittest import skip
from mango.managers import start_server
from mango.errors import ExitQuiet, _strings
from wurlitzer import pipes
from io import StringIO
from mango.tests import stOQ, stEQ, captured_output
from time import sleep

class ServerTest(unittest.TestCase):

    @skip('kills test suite')
    def test_start_server(self):
        errors = ['{}Warning\n'.format(_strings._W[0]),
                  '{}Message\n'.format(_strings._M[0]),
                  '{}Kill\n\n'.format(_strings._F[0])]
        # Needs fixing always passes
        Error = start_server("Error")

        Error("EV False ./ 1")
        Error("W Warning")
        # self.assertEqual([], stEQ.read())

        Error("EV True ./ 1")
        Error("W Warning")
        self.assertEqual(errors[0], stEQ.read()[0])

        Error(">M Message")
        self.assertEqual(errors[1], stEQ.read()[0])
        # with self.assertRaises(ExitQuiet):
            # Error("F Kill")
        # self.assertEqual(errors[2], stEQ.read()[0])

    @skip('kills test suite')
    def test_end_start_server(self):
        Error = start_server("Error")
        Error("END")
        # Check pid exists

    @skip('kills test suite')
    def test_prg_server(self):
        progress = start_server("Prog")
        progress("END")
        # Check pid exists

    @skip('Unfinished test')
    def test_serverclass(self):
        pass


if __name__ == '__main__':
    unittest.main(verbosity=2)
