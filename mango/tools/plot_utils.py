import numpy as np
from functools import partial
import matplotlib.patches as mpatches

from mango.imports import matplotlib

mpl = matplotlib()
(figure, PdfPages, setp, gcf, ticker, handlermap) = mpl.prettyplot()


def force_fit_loader(ftype, rad, nm, ff):
    # mag: chi, x_fit, comb2, output, out3, b
    return np.load(f'{ff}{ftype}{rad}{nm}plotsave.npz', allow_pickle=True)['values']


def axeswidth(gv, linewidth=0.5):
    """Make mpl ais lines thinner."""
    for axis in ['top', 'bottom', 'left', 'right']:
        gv.spines[axis].set_linewidth(linewidth)


def _skip_lines(filename, cs):
    with open(filename, "rb") as file:
        cst = getattr(cs, filename)
        for lno, line in enumerate(file):
            # v_print(f"\r{lno}", end="\r")
            if line.startswith(b" ("):
                line = line.replace(b'+-', b'-')
            elif line.startswith(b"#"):
                cst[lno] = line
            yield line


def trimmer(ind, *args):
    """Trim data to only show elements in index."""
    args = list(args)
    for no, a in enumerate(args):
        args[no] = a[ind]
    return args


def dosaxes(gv, xl=None, yl=None):
    """Create pretty dos graph."""
    if xl is not None:
        gv.set_xlabel(xl)
        gv.ticklabel_format(style='sci', axis='both', scilimits=(0, 0), useOffset=False)
    else:
        setp(gv.get_xticklabels(), visible=False)

    gv.set_ylabel(yl)
    gv.set_yscale('log')
    gv.set_xscale('log')
    gv.tick_params(axis='both', direction='in', which="both", width=0.5)
    axeswidth(gv)


def flatten_dict(dd, separator='.', prefix=''):
    """Flatten file dictionary."""
    return {prefix + separator + k if prefix else k: v
            for kk, vv in dd.items()
            for k, v in flatten_dict(vv, separator, kk).items()
            } if isinstance(dd, dict) else {prefix: dd}


def v_print(*args, **kw):
    """Pretty print."""
    largs = len(args)
    pad = 0 if not isinstance(args[0], str) or args[0] == "File:" else (14 - len(args[0]))
    pad2 = 0 if not largs > 1 else 46 - len(args[1]) if isinstance(args[1], str) and args[1].endswith("dat") else 0
    print(args[0], " " * pad, args[1] if largs > 1 else '', " " * pad2, *args[2:], **kw)


class logskipper():
    """Skip data logarithmically."""

    def __init__(self):
        """Init."""
        pass

    def get(self, x, sk):
        """Get logskipped data index."""
        da = np.where(x > 0)
        xv = x[da]
        if hasattr(self, 'xv') and np.array_equal(self.xv, xv) and self.sk == sk:
            return self.idx
        else:
            return self._calc(xv, sk, da)

    def _calc(self, xv, sk, da):
        v_print("\nCalculating logarithmic data skipping\n")
        self.xv = xv
        self.sk = sk
        xmax = np.max(xv)
        xmin = np.min(xv)
        spacing = np.geomspace(xmin, xmax, num=xv.shape[0] // sk, dtype=float)
        self.idx = np.zeros_like(spacing, dtype=int)
        for no, i in enumerate(spacing):
            self.idx[no] = (np.abs(xv - i)).argmin()
        self.idx = np.unique(self.idx)
        self.idx += np.min(da[0])
        return self.idx


def logticks(gva, tsep=0.1, odd=True):
    """Modify mpl logticks."""
    gva.set_minor_locator(ticker.LogLocator(base=10.0, subs=np.arange(0, 1, step=tsep), numticks=100))
    gva.set_minor_formatter(ticker.NullFormatter())
    gva.set_major_locator(ticker.LogLocator(base=10.0, numticks=100))
    gva.set_major_formatter(ticker.FuncFormatter(partial(major_formatter, odd)))


def major_formatter(odd, y, pos):
    ly = int(np.log10(y))
    label = "{}{}{}".format("$10^{", ly, "}$")
    if odd != 'all':
        label = label if ly % 2 == (1 if odd else 0) else ''
    return label


def combineerrors(d1, d2):
    d1y2 = d1[:, 1]**2
    d1yE2 = d1[:, 4]**2
    d2y2 = d2[:, 1]**2
    d2yE2 = d2[:, 1]**2

    return np.sqrt((d1y2 * d1yE2) + (d2y2 * d2yE2)) / d1[:, 1]


def add_subplot_axes(gv, rect):
    """Add subplot to a graph."""
    fig = gcf()
    box = gv.get_position()
    width = box.width
    height = box.height
    x = rect[0]
    y = rect[1]
    import types
    gv.axspan = types.MethodType(axspan, gv)
    v_print(x, y, width, height)
    # gv.axspan(x, x + width - 0.005, y - 0.05, y + height -0.1, transform=gv.transAxes, facecolor='g', zorder=10)
    gv.axspan(x, x + width - 0.005, y - 0.05, y + rect[3], transform=gv.transAxes, facecolor='w', zorder=1)

    inax_position = gv.transAxes.transform(rect[0:2])
    transFigure = fig.transFigure.inverted()
    infig_position = transFigure.transform(inax_position)
    x = infig_position[0]
    y = infig_position[1]
    width *= rect[2]
    height *= rect[3]
    subax = fig.add_axes([x, y, width, height])
    x_labelsize = subax.get_xticklabels()[0].get_size() * rect[2]**0.5
    y_labelsize = subax.get_yticklabels()[0].get_size() * rect[3]**0.5
    subax.xaxis.set_tick_params(labelsize=x_labelsize)
    subax.yaxis.set_tick_params(labelsize=y_labelsize)
    return subax


def axspan(self, xmin, xmax, ymin=0, ymax=1, **kwargs):
    """Subplot method."""
    # trans = self.get_xaxis_transform(which='grid')

    # process the unit information
    self._process_unit_info([xmin, xmax], [ymin, ymax], kwargs=kwargs)

    # first we need to strip away the units
    xmin, xmax = self.convert_xunits([xmin, xmax])
    ymin, ymax = self.convert_yunits([ymin, ymax])

    verts = [(xmin, ymin), (xmin, ymax), (xmax, ymax), (xmax, ymin)]
    p = mpatches.Polygon(verts, **kwargs)
    # p.set_transform(trans)
    self.add_patch(p)
    # self.autoscale_view(scaley=False, scalex=False)
    return p
