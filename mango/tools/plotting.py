import numpy as np

from argparse import ArgumentParser
from scipy.optimize import curve_fit
from functools import partial

from mango.managers import serverwrapper
from mango.constants import c
from mango.pp.util import onevent, showgraph
from mango.tools.plot_utils import (figure, PdfPages, handlermap, mpl,
                                    _skip_lines, v_print, logskipper, logticks,
                                    axeswidth, add_subplot_axes, force_fit_loader,
                                    trimmer, dosaxes, flatten_dict, combineerrors)


class cstore():
    """Store Class."""

    def __init__(self, **argd):
        """Update self."""
        self.__dict__.update(argd)

    def update(self, **argd):
        """Update self."""
        self.__dict__.update(argd)


def args():
    """Parse args and read in files."""
    parser = ArgumentParser()

    parser.add_argument('-ct', default='M', help='acf type', type=str, nargs='+', choices=["M", "V", "A", "I"])
    parser.add_argument('-R', default=1, help='Run Number', type=int)
    parser.add_argument('-M', default=1, help='Number of particles', type=int)
    parser.add_argument('-B', default=20, help='Number of blocks', type=int)
    parser.add_argument('-rad', default=1.0, help='particle radius', type=float)
    parser.add_argument('-Troom', default=300.0, help='Temperature', type=float)
    parser.add_argument('-eta', default=0.01002, help='Viscosity', type=float)  # [g/(cm*s)]
    parser.add_argument('-fp', default='', help='File Directory', type=str)
    parser.add_argument('-DOS_only', action='store_true', default=False)
    parser.add_argument('-showodr', action='store_true', default=False)
    parser.add_argument('-sk', action='store_true', help='skip some data for smaller filesize', default=False)
    parser.add_argument('-sg', action='store_true', help='show graph', default=False)
    parser.add_argument('-ff', type=str, help='force fitting load directory', default=False)

    p = parser.parse_args()

    p.Files = {**{f"{t}DOS": {"ALIGN": None, "NORM": None} for t in p.ct},
               **{f"{t}ACF": {"ALIGN": None, "NORM": None} for t in p.ct},
               **{f"{t}ROT": {"DOS": None, "ACF": None} for t in p.ct}}

    p.fp = p.fp + "/" if not p.fp.endswith('/') and not p.fp == '' else p.fp

    prefix = f"{p.fp}S_Run{p.R}_mol-{p.M}_autocorr_"
    midix = f'_blk{p.B}'
    suffix = '.dat'

    if p.M != 1:
        for k, v in p.Files.items():
            kv = k
            for k2 in v.keys():
                kv2 = "" if k2 == "NORM" else k2
                if k.endswith("ROT"):
                    kv = "R" + kv2
                    kv2 = 'ALIGN'
                p.Files[k][k2] = (f'{prefix}{kv}{midix}{kv2}{suffix}',
                                  np.complex if "ACF" in kv else np.float)
    else:
        p.Files["MDOS"]["NORM"] = (f'{prefix}MDOS{midix}{suffix}', np.float)

    p.Files_flat = flatten_dict(p.Files)

    cs = getdata(p)
    return cs, p


def getdata(p):
    """Read in and store data."""
    cs = cstore()
    if not p.DOS_only:
        p.Files_flat_acf = {k: f for k, f in p.Files_flat.items() if f is not None and "DOS" not in f[0]}
        cs.update(**{i[0]: {} for i in p.Files_flat_acf.values()})
        for f, t in p.Files_flat_acf.values():
            v_print("File:", f, "data_type:", t)
            getattr(cs, f)['data'] = np.genfromtxt(_skip_lines(f, cs), dtype=t, comments='#')

    p.Files_flat = {k: f for k, f in p.Files_flat.items() if f is not None and "DOS" in f[0]}
    cs.update(**{i[0]: {} for i in p.Files_flat.values()})
    for f, t in p.Files_flat.values():
        v_print("File:", f, "data_type:", t)
        getattr(cs, f)['data'] = np.genfromtxt(_skip_lines(f, cs), dtype=t, comments='#')
    return cs


class Debye():
    """Collection of Debye functions."""

    def __init__(self):
        """Init."""
        self.full_comb_p = self.full_comb_f2

    def _full(self, oo, chi, tau):
        return chi / (1 - 1j * oo * tau)

    def full(self, *args, **kw):
        """Full Debye."""
        return self._full(*args, **kw).imag

    def approx1(self, chi, oo, tau):
        """Fitting function ~ approx for tauB *omega>> 1."""
        return chi / (oo * tau)

    def approx2(self, oo, A):
        """Fitting function ~ approx for tauB *omega>> 1."""
        return A / (oo)

    def _full_sc(self, chi, tau, oo, const):
        return const * chi / (1 - 1j * const * oo * tau)

    def full_sc(self, *args, **kw):
        return self._full_sc(*args, **kw).imag

    def wrap_cf(self, *args):
        v_print("testing", args)
        exit(0)
        tau, const = args[-2]
        oo = args[-1]
        args = args[:-2]

        return self.full_comb_f(*args, oo, tau, const)

    def wrap_cf2(self, *args):
        const = args[-2]
        oo = args[-1]
        args = args[:-2]

        return self.full_comb_f(*args, oo, const)

    def full_comb_f(self, chi, chi0, tau0, oo, tau, const):
        return (self.approx1(chi, oo, tau) + self._full_sc(chi0, tau0, oo, const)).imag

    def full_comb_f2(self, chi, chi0, tau0, tau, oo, const):
        return (self._full(oo, chi, tau) + self._full_sc(chi0, tau0, oo, const)).imag


def Mv(R, extra=False):
    """Mmag and Vol dependent on radius."""
    if not extra:
        print('hi')
        if R == 1.0:
            return (0.000673,
                    4.1887902)  # Vol [1e-18cm^3] r1.0
    if R == 0.5:
        return (0.000311096212522,  # Mmag [1e-12emu] r0.5
                0.523598775598)  # Vol [1e-18cm^3] r0.5
    elif R == 1.0:
        return (0.00248877,  # Mmag [1e-12emu] r1.0
                4.1887902)  # Vol [1e-18cm^3] r1.0
    elif R == 2.0:
        return (0.01991016,  # Mmag [1e-12emu] r2.0
                33.51032164)  # Vol [1e-18cm^3] r2.0
    elif R == 5.0:
        return (0.31109621252273995,  # Mmag [1e-12emu] r5.0
                523.5987756)  # Vol [1e-18cm^3] r5.0
    elif R == 7.5:
        return (1.04994971726,  # Mmag [1e-12emu] r7.5
                1767.14586764)  # Vol [1e-18cm^3] r7.5


def adjustments(p):
    """Graph adjustments."""
    d = {1.0: {1: {"DOS": {"sk": 30, "skI": None},
                   "AR": {"xy": None, "xytext": None},
                   "ACF": {"x": None, "y": None, "sk": None},
                   "lim": ((2, 30), None),
                   "ins": {"x": None, "y": None, "w": None, "h": None}},
               2: {"DOS": {"sk": 30, "skI": 50},
                   "AR": {"xy": (None, None), "xytext": (None, None)},
                   "ACF": {"x": 40, "y": 1e-7, "sk": 10},
                   "lim": ((1, 30), None),
                   "ins": {"x": 0.3, "y": 0.1, "w": 0.25, "h": 0.25}},
               5: {"DOS": {"sk": 30, "skI": 50},
                   "AR": {"xy": (1e2, 2e-2), "xytext": (1e2, 8e-2)},
                   "ACF": {"x": 25, "y": 1e-5, "sk": 50},
                   "lim": ((2e-1, 1e1), (1e1, 150)),
                   "ins": {"x": 0.2, "y": 0.1, "w": 0.25, "h": 0.25}},
               10: {"DOS": {"sk": 30, "skI": 50},
                    "AR": {"xy": (1e2, 2e-2), "xytext": (1e2, 8e-2)},
                    "ACF": {"x": 200, "y": 1e-5, "sk": 200},
                    "lim": ((5e-2, 3), (1e1, 150)),
                    "ins": {"x": 0.2, "y": 0.1, "w": 0.25, "h": 0.25}},
               20: {"DOS": {"sk": 30, "skI": 50},
                    "AR": {"xy": (1e2, 2e-2), "xytext": (1e2, 8e-2)},
                    "ACF": {"x": 200, "y": 1e-5, "sk": 200},
                    "lim": ((5e-2, 1), (1e1, 150)),
                    "ins": {"x": 0.2, "y": 0.1, "w": 0.25, "h": 0.25}}},
         2.0: {1: {"DOS": {"sk": 30, "skI": None},
                   "AR": {"xy": (None, None), "xytext": (None, None)},
                   "ACF": {"x": None, "y": None, "sk": None},
                   "lim": ((5, 1e2), None),
                   "ins": {"x": None, "y": None, "w": None, "h": None}},
               2: {"DOS": {"sk": 30, "skI": 50},
                   "AR": {"xy": (None, None), "xytext": (None, None)},
                   "ACF": {"x": 200, "y": 1e-8, "sk": 20},
                   "lim": ((1, 5e1), None),
                   "ins": {"x": 0.4, "y": 0.1, "w": 0.25, "h": 0.25}},
               5: {"DOS": {"sk": 30, "skI": 50},
                   "AR": {"xy": (1e3, 4e-3), "xytext": (1e3, 2e-2)},
                   "ACF": {"x": 10, "y": 1e-6, "sk": 10},
                   "lim": ((5e-1, 1e2), (1e2, 1e3)),
                   "ins": {"x": 0.3, "y": 0.1, "w": 0.25, "h": 0.25}},
               10: {"DOS": {"sk": 30, "skI": 50},
                    "AR": {"xy": (1e3, 4e-3), "xytext": (1e3, 2e-2)},
                    "ACF": {"x": 80, "y": 1e-7, "sk": 50},
                    "lim": ((5e-1, 3e1), (1e2, 1e3)),
                    "ins": {"x": 0.2, "y": 0.1, "w": 0.25, "h": 0.25}},
               20: {"DOS": {"sk": 30, "skI": 50},
                    "AR": {"xy": (10**3, 5 * 10**-4), "xytext": (10**3, 10**-4)},
                    "ACF": {"x": 300, "y": 1e-5, "sk": 200},
                    "lim": ((5e-1, 1e1), (2e2, 2e3)),
                    "ins": {"x": 0.2, "y": 0.1, "w": 0.25, "h": 0.25}}},
         5.0: {1: {"DOS": {"sk": 30, "skI": None},
                   "AR": {"xy": (None, None), "xytext": (None, None)},
                   "ACF": {"x": None, "y": None, "sk": None},
                   "lim": (5, 1e2),
                   "ins": {"x": None, "y": None, "w": None, "h": None}},
               2: {"DOS": {"sk": 30, "skI": 50},
                   "AR": {"xy": (None, None), "xytext": (None, None)},
                   "ACF": {"x": 200, "y": 1e-8, "sk": 20},
                   "lim": (5e-1, 1e1),
                   "ins": {"x": 0.4, "y": 0.1, "w": 0.25, "h": 0.25}},
               5: {"DOS": {"sk": 30, "skI": 50},
                   "AR": {"xy": (10**3, 5 * 10**-4), "xytext": (10**3, 10**-4)},
                   "ACF": {"x": 10, "y": 1e-6, "sk": 10},
                   "lim": (5e-1, 1e1),
                   "ins": {"x": 0.3, "y": 0.1, "w": 0.25, "h": 0.25}},
               10: {"DOS": {"sk": 30, "skI": 50},
                    "AR": {"xy": (10**3, 5 * 10**-4), "xytext": (10**3, 10**-4)},
                    "ACF": {"x": 80, "y": 1e-7, "sk": 50},
                    "lim": (5e-1, 1e1),
                    "ins": {"x": 0.2, "y": 0.1, "w": 0.25, "h": 0.25}},
               20: {"DOS": {"sk": 30, "skI": 50},
                    "AR": {"xy": (10**3, 5 * 10**-4), "xytext": (10**3, 10**-4)},
                    "ACF": {"x": 300, "y": 1e-5, "sk": 200},
                    "lim": (5e-1, 1e1),
                    "ins": {"x": 0.2, "y": 0.1, "w": 0.25, "h": 0.25}}}}
    ret = d[p.rad][p.M]
    if not p.extra:
        ret['lim'] = ((2, 30), None)
    return ret


class fitting():

    def __init__(self, data, p, db, scale, chi, tauB):
        self.files = {}
        self.n = 0
        self.addfile(data, scale, chi, tauB)
        self.db = db

    def _fitter(self, model1, model2, x, y, ydev, guess1, guess2):
        factor, factor_err = self._cf(model1, x, y, ydev, guess1)
        output = self._odr(model2, x, y, ydev, guess2)
        self._diff(factor, factor_err, output)
        return factor, factor_err, output

    def _setup(self, lim, filenum):
        d = self.files[filenum]
        x_fit = [lim[0] / d['tauB'], lim[1] / d['tauB']]
        fit = np.where((d['x'] > x_fit[0]) & (d['x'] < x_fit[1]))
        # v_print("fit", fit)
        x = d['x'][fit]
        y = d['y'][fit]
        ydev = d['ydev'][fit]

        v_print("Xfit", x_fit, lim)

        y = x * y * d['scale']
        ydev = x * ydev * d['scale']

        return x, y, ydev, x_fit, d['chi'], d['tauB']

    def _cf(self, f, x, y, sigma, p0):
        popt, pcov = curve_fit(f, x, y, p0=p0,
                               sigma=sigma,
                               absolute_sigma=True)

        err = np.sqrt(np.diag(pcov))
        # chi_err = err[:, 0]
        tau0_err = err

        # chi_fit = popt[0]
        tau_B_fit = popt
        a = 1e12
        rd = 1 / (2 * np.pi)
        tb = self.files[0]['tauB']
        freq = tb / tau_B_fit
        freq_err = freq * tau0_err / tau_B_fit
        v_print("CF", 'tb', "TauB_fit", "TauB_err", 'TauB_Freq_r', 'TauB_Freq_err_r', 'Hz', 'Hz_err')
        v_print(" ", tb, rd * a / tb, tau_B_fit * 10**-12, tau0_err * 10**-12, freq,
                freq_err, a * freq / tb, a * freq_err / tb, rd * (a * freq / tb), rd * (a * freq_err / tb))

        return tau_B_fit, tau0_err

    def _odr(self, f, x, y, sigma, p0):
        from scipy.odr import ODR, Model, RealData
        model = Model(self._wrap(f))
        data = RealData(x, y, sx=sigma)
        odr = ODR(data, model, beta0=p0)
        odr.set_job(fit_type=0)
        output = odr.run()

        v_print("ODR", output.beta, output.sd_beta)

        return output

    def _diff(self, cf, cfe, od):
        v_print("%Diff", np.absolute(cf - od.beta) / cf)
        v_print("%Diff_err", np.absolute(cfe - od.sd_beta) / cfe)

    def _wrap(self, model):
        def newf(*args):
            guess = args[0]
            return model(args[1], *guess)
        return newf

    def addfile(self, data, scale, chi, tauB):
        xind = np.where(data[:, 0] > 0)

        self.files[self.n] = {'x': data[:, 0][xind],
                              'y': data[:, 1][xind],
                              'ydev': data[:, 4][xind],
                              'scale': scale,
                              'chi': chi,
                              'tauB': tauB}
        self.n += 1

    def tp(self, lim):
        x, y, ydev, x_fit, chi, tauB = self._setup(lim, 0)

        model = partial(self.db.approx1, chi)
        guess = tauB
        tau_B_fit, tau_B_fit_err, output = self._fitter(model, model, x, y, ydev, guess, [guess])

        self.tau_B_fit = tau_B_fit
        self.tau_B_fit2 = output.beta

        return tau_B_fit, x_fit, output, tau_B_fit_err

    def shoulder(self, lim, guess):
        x, y, ydev, x_fit, chi, tauB = self._setup(lim, 1)

        model = partial(self.db.full_sc, chi, tauB)
        guess = [guess]

        tau_B_fit, tau_B_fit_err, output = self._fitter(model, model, x, y, ydev, guess, guess)

        return tau_B_fit, x_fit, output

    def combine(self, lim, guess):
        x, y, ydev, x_fit, chi0, tauB0 = self._setup(lim, 1)
        *_, chi, tauB = self._setup(lim, 0)

        model = partial(self.db.full_comb_f, chi, chi0, tauB0)

        guess = [tauB, guess]
        tau_B_fit, tau_B_fit_err, output = self._fitter(model, model, x, y, ydev, guess, guess)

        return tau_B_fit, x_fit, output

    def combine_s(self, lim, guess):
        x, y, ydev, x_fit, chi0, tauB0 = self._setup(lim, 1)
        *_, chi, tauB = self._setup(lim, 0)

        model1 = partial(self.db.full_comb_f2, chi, chi0, tauB0, self.tau_B_fit)
        model2 = partial(self.db.full_comb_f2, chi, chi0, tauB0, self.tau_B_fit2)

        guess = [guess]

        tau_B_fit, tau_B_fit_err, output = self._fitter(model1, model2, x, y, ydev, guess, guess)

        return tau_B_fit, x_fit, output


def tauR(Vp, a, Troom, eta, mnp=10):
    b = mnp * a
    Vp = mnp * (4 * np.pi * a**3 / 3)
    tau0 = 3 * eta * Vp / (c.KB * Troom)
    with np.errstate(all='raise'):
        try:
            rho = a / b
            rho2 = rho**2
            rho4 = 1 - rho2**2
            sqr2 = np.sqrt(1 - rho2)

            factor = tau0 * (2 / 3) * rho4
            logvar = (1 + sqr2) / rho

            logfac1 = (rho2 * (2 - rho2) / sqr2)
            log1 = logfac1 * np.log(logvar)

            tauPar = factor / (log1 - rho2)

            logfac2 = (rho2 * (1 - 2 * rho2) / sqr2)
            log2 = logfac2 * np.log(logvar)

            tauPerp = 2 * factor / (log2 + 1)

            _tauR = 1 / ((1 / 3) * ((1 / tauPar) + (2 / tauPerp)))

            tauR_tau0 = _tauR / tau0
        except Exception as e:
            v_print(e)
            _tauR = tau0
            tauR_tau0 = 1
    v_print("Tau_R", _tauR, tauR_tau0)
    return _tauR


def plotter(ls, layernum, skip, xy_zip):
    leg_list = []
    for x, y, ydev, axis in xy_zip:
        x, y, ydev = trimmer(ls.get(x, skip), x, y, ydev)
        Exp_err = axis.fill_between(x, y - ydev, y + ydev, color='#cccccc', zorder=layernum[0])
        Expr = axis.scatter(x, y, 10, marker='x', lw=0.5, color=f'C{layernum[1]}', zorder=layernum[0])
        leg_list += [Exp_err, Expr]
    return x, leg_list


def plot_fit(axes, x, chi0, db_fit, zorder=4, **kw):
    return [(axes[2].plot(x, db_fit / chi0, lw=1.0, zorder=zorder, **kw)[0],
             axes[1].plot(x, db_fit / (chi0 * x), lw=1.0, zorder=zorder, **kw)[0])]


def setlim(axes):
    a = axes[1].get_xlim()
    b = axes[2].get_xlim()
    n1 = 9.5e2
    n2 = 5e3
    n3 = 3.5e3
    n4 = 3.5e4
    n1_1 = 1e3
    n = n1
    axes[1].set_xlim((a[0], n))
    axes[2].set_xlim((b[0], n))
    if False:
        ay = axes[2].get_ylim()
        axes[2].set_ylim((10**-5, ay[1]))

def getscale(cs, files):
    scales = []
    for f, g in files:
        file = getattr(cs, f)
        file['scale'] = float(file[0].split(b':')[-1].decode()[1:-1])
        scales += [file['scale']]
    yscale = [scales[0] / scales[1], scales[1] / scales[1], scales[2] / scales[1]]
    return yscale


def mdosfigure():
    axes = {}
    fig = figure(figsize=(8, 6))
    axes[1] = fig.add_subplot(211)
    axes[2] = fig.add_subplot(212)

    axes[1].annotate('(a)', weight='bold', xy=(0, 0), xycoords='axes fraction', fontsize=9,
                     xytext=(7, 7), textcoords='offset points',
                     ha='left', va='bottom')
    axes[2].annotate('(b)', weight='bold', xy=(0, 1), xycoords='axes fraction', fontsize=9,
                     xytext=(7, -5), textcoords='offset points',
                     ha='left', va='top')

    dosaxes(axes[1], yl=r'Magnetic DOS (arb. units)')
    dosaxes(axes[2], xl=r'$\omega\tau_B$', yl=r'$\chi/\chi_0$')
    logticks(axes[1].yaxis, tsep=0.2, odd=False)
    logticks(axes[2].yaxis, tsep=0.2, odd='all')
    return fig, axes


def macffigure():
    fig = figure(figsize=(8, 6))
    axis = fig.add_subplot(111)
    axis.set_xlabel(r"Time t ($10^{-6}$ s)")
    axis.set_ylabel("Magnetic autocorrelation (arb. unit)", labelpad=10)

    axis.ticklabel_format(style='sci', axis='both', scilimits=(-10, 10), useOffset=False)
    axis.set_yscale('log')
    # logticks(axis.yaxis)
    axis.tick_params(direction='in', which="both", width=0.5)

    # _, m = axis.get_xlim()
    # _, m2 = axis.get_ylim()

    # axis.set_xlim(0, adj['ACF']['x'])
    # axis.set_ylim(adj['ACF']['y'], m2)
    axeswidth(axis)
    return fig, axis


def mdosfit(cs, p, db, adj, graph_type, tauB, scale, chi0, Vp):
    Mn = 54.93805
    La = 138.9055
    Sr = 87.62
    Ox = 19.9994
    freq = 2 * np.pi * 175e3 * 1e-12
    field = 10.95e3 * (4 * np.pi * 1e-3) * 1e-6
    rhoMn = 6.99 * Mn / ((1 - 0.35) * La + 0.35 * Sr + Mn + 3 * Ox)
    SARovchiw = 0.5 * freq * field**2 * 1e24 * 1e-7 / rhoMn

    if p.ff:
        (chi, x_fit, comb, output, out3, tau_B_fit, tau_B_fit_err), yscale = force_fit_loader('mag', p.rad, p.M, p.ff)
        print(chi, x_fit, comb, output, out3, tau_B_fit, tau_B_fit_err)
    else:
        chi = float(getattr(cs, p.Files["MDOS"]["NORM"][0])[0].split(b":")[1].split(b",")[0]) / (2 * np.pi * c.KB * p.Troom * Vp)
        print(chi)

        v_print('Chi', chi)

        fit = fitting(getattr(cs, p.Files["MROT"]["DOS"][0] if p.extra and p.M != 1 else p.Files["MDOS"]["NORM"][0])['data'].real,
                      p, db, scale, chi, tauB)

        tau_B_fit, x_fit, output, tau_B_fit_err = fit.tp(adj["lim"][0])

        if adj["lim"][1] is not None and p.extra:
            fit.addfile(getattr(cs, p.Files["MDOS"]["NORM"][0])['data'].real, scale, chi0, tauB)
            d, x_fit2, output2 = fit.shoulder(adj["lim"][1], 0.02)
            v_print("x_fit2", x_fit2)
            comb, x_fit, out3, = fit.combine_s([adj['lim'][0][0], adj['lim'][1][1]], 0.02)
        else:
            comb = None
            out3 = None
    
        files = [data for t, data in p.Files_flat.items() if t.startswith(graph_type)]
        yscale = getscale(cs, files)
        np.savez_compressed(f'./mag{p.rad}{p.M}plotsave', values=(np.array([chi, x_fit, comb, output, out3, tau_B_fit, tau_B_fit_err]), yscale))
    dbf = db.full(freq, chi0, tau_B_fit)
    v_print('SAR', SARovchiw * dbf, '±', SARovchiw * dbf * db.full(freq, chi0, tau_B_fit_err / tau_B_fit))

    if adj["lim"][1] is not None and p.extra:
        dbf_s = db.full_comb_p(chi, chi0, tauB, tau_B_fit, freq, comb)
        v_print('SAR_withshoulder', SARovchiw * dbf_s, '±', SARovchiw * dbf_s * db.full_comb_p(chi, chi0, tauB, tau_B_fit_err / tau_B_fit, freq, comb))

    return chi, x_fit, comb, output, out3, tau_B_fit, yscale


def plot_DOS(p, cs, ls, adj, graph_type, db, axes, chi, chi0, tauB, vol, scale, x_fit, comb, output, out3, tau_B_fit, yscale):

    layernum = {0: [2, 1], 1: [3, 0], 2: [1, 2]} if p.M != 1 else {0: [1, 0]}
    ex_legend = []
    files = [data for t, data in p.Files_flat.items() if t.startswith(graph_type)]
    for n, (f, g) in enumerate(files):
        num = layernum[n]
        file = getattr(cs, f)
        v_print("File Comments", f, file[0])
        data = file['data']
        x1 = data[:, 0] * tauB
        y1 = yscale[n] * data[:, 0] * data[:, 1] * scale / chi0
        ydev1 = yscale[n] * data[:, 0] * data[:, 4] * scale / chi0

        y2 = yscale[n] * data[:, 1] * scale / (chi0 * tauB)
        ydev2 = yscale[n] * data[:, 4] * scale / (chi0 * tauB)

        x, leg_list = plotter(ls, num,
                              adj['DOS']['sk'] if p.sk else 1,
                              zip((x1, x1), (y1, y2), (ydev1, ydev2), (axes[2], axes[1])))
        ex_legend += [tuple(leg_list)]

    if p.M != 1:
        setlim(axes)
        ex_legend[0], ex_legend[1] = ex_legend[1], ex_legend[0]

    # Plotting fit lines
    xtb = x / tauB

    ex_legend += plot_fit(axes, x, chi0,
                          db.full_comb_p(chi, chi0, tauB, tau_B_fit, xtb, comb) if adj["lim"][1] is not None and p.extra else db.full(xtb, chi, tau_B_fit),
                          ls='-', color='k')
    ex_legend += plot_fit(axes, x, chi0, db.full(xtb, chi0, tauB), ls='--', dashes=(5, 5), color='k')
    if p.M != 1 and p.extra:
        ex_legend += plot_fit(axes, x, chi0, db.full(xtb, chi, tauR(vol, p.rad, p.Troom, p.eta, mnp=p.M)),
                              ls='-', color='b', zorder=4 if p.M != 1 else 3)

    if p.showodr:
        ex_legend += plot_fit(axes, x, chi0, db.full_comb_p(chi, chi0, tauB, xtb, output.beta, out3.beta), ls='-', color='m')
    print(x_fit[0] * tauB)

    # Fitting Hash region
    if adj['lim'][1] is None or adj['lim'][0][1] >= adj['lim'][1][0]:
        axes[2].axvspan(adj['lim'][0][0], adj['lim'][0 if adj['lim'][1] is None else 1][1],
                        alpha=0.1, color='k', fill=False, hatch="///", zorder=0)
    else:
        axes[2].axvspan(*adj['lim'][0], alpha=0.1, color='k', fill=False, hatch="///", zorder=0)
        axes[2].axvspan(*adj['lim'][1], alpha=0.1, color='k', fill=False, hatch="///", zorder=0)
    
    # Legend Labels
    leglab = ['Total', 'Aligned', 'Rotational', 'Fit', 'Debye', 'Equivalent\n ellipsoid'] if p.M != 1 else ['Total', 'Fit', 'Debye', 'Equivalent ellipsoid', 'odr']

    leg = axes[1].legend(ex_legend, leglab, loc='upper right', handler_map=handlermap, **{'fontsize': 9})

    # Arrow
    if p.M not in [1, 2] and p.extra:
        axes[2].annotate('', **adj['AR'], xycoords='data', arrowprops=dict(facecolor='black',
                                                                           width=0.2, headwidth=5, headlength=5, shrink=0.05),)

    return leg, ex_legend


def plot_ACF(p, cs, ls, adj, graph_type, ax2, chi0, scale):
    layernum = {0: [2, 1], 1: None, 2: [1, 4]}
    ex_legend = []
    for n, (f, g) in enumerate([data for t, data in p.Files_flat_acf.items() if t.startswith(graph_type)]):
        num = layernum[n]
        file = getattr(cs, f)
        v_print("File Comments", f, file[0])
        data = file['data'].real
        x1 = data[:, 0] * 1e-6
        y1 = data[:, 1] * scale / chi0
        ydev1 = data[:, 4] * scale / chi0
        if n == 1:
            sav_y1 = y1.copy()
            sav_ydev1 = ydev1.copy()
        elif n == 2:
            _ydev = np.sqrt(np.absolute(sav_y1**2 * sav_ydev1**2 + y1**2 * ydev1**2 - 2 * y1 * sav_y1 * np.cov(sav_y1, y1, ddof=0)[0][1]))
            x, leg_list = plotter(ls, num, adj['ACF']['sk'], [[x1, sav_y1 - y1, _ydev, ax2]])
            ex_legend += [tuple(leg_list)]
        else:
            x, leg_list = plotter(ls, num, adj['ACF']['sk'], [[x1, y1, ydev1, ax2]])
            ex_legend += [tuple(leg_list)]

    leglab = ['Aligned', 'Diff - Total and Rotational']
    leg = ax2.legend(ex_legend, leglab, loc='upper right', **{'fontsize': 9})

    return leg, ex_legend


def plot_subplot(p, cs, ls, adj, ax, tauB):
    xpos = 0.2 if adj['ins']['x'] is None else adj['ins']['x']
    ypos = 0.1 if adj['ins']['y'] is None else adj['ins']['y']
    width = 0.25 if adj['ins']['w'] is None else adj['ins']['w']
    height = 0.25 if adj['ins']['h'] is None else adj['ins']['h']

    subpos = [xpos, ypos, width, height]
    subax1 = add_subplot_axes(ax, subpos)
    subax1.set_xscale('log')
    v_print(p.F)
    d1 = getattr(cs, p.F[0])['data']
    d2 = getattr(cs, p.F[2])['data']

    x = d1[:, 0] * tauB
    y = (d1[:, 1] - d2[:, 1]) / d1[:, 1]
    ydev = d1[:, 0] * combineerrors(d1, d2)

    x, y, ydev = trimmer(ls.get(x, adj['DOS']['skI']), x, y, ydev)

    sperr = subax1.fill_between(x, y - ydev, y + ydev, color='#cccccc')
    sp = subax1.scatter(x, y, 10, marker='x', lw=0.5)

    logticks(subax1.xaxis, tsep=0.2)
    subax1.tick_params(direction='in', which="both", width=0.5)
    axeswidth(subax1)

    sp_legend = [(sperr, sp)]
    leg = subax1.legend(sp_legend, ['Rel. Diff.'], prop={'size': 5})

    return leg, sp_legend


def mag(cs, p, fig_event):
    adj = adjustments(p)
    db = Debye()
    ls = logskipper()

    Mmag, vol = Mv(p.rad, p.extra)

    tauB = 3 * p.eta * vol / (c.KB * p.Troom)  # [1e-12s]
    Vp = p.M * vol
    scale = 1. / (2 * c.KB * p.Troom * Vp)
    chi0 = Mmag**2 / (3 * c.KB * p.Troom * vol)  # []
    # inv2p = 1 / (2 * np.pi)
    print(chi0)
    chi, x_fit, comb, output, out3, tau_B_fit, yscale = mdosfit(cs, p, db, adj, 'M', tauB, scale, chi0, Vp)
    fig, axes = mdosfigure()
    leg, dos_legend = plot_DOS(p, cs, ls, adj, 'M', db, axes, chi, chi0, tauB, vol, scale, x_fit, comb, output, out3, tau_B_fit, yscale)

    fig_event.connect(fig, leg, dos_legend)
    fig.subplots_adjust(left=0.1 if False else 0.17, bottom=0.1, right=0.9, top=0.9, wspace=0, hspace=0)

    if not p.DOS_only:
        fig2, axis = macffigure()
        leg, macf_legend = plot_ACF(p, cs, ls, adj, 'M', axis, chi0, scale)
        fig_event.connect(fig2, leg, macf_legend)

    if False:
        leg, sp_legend = plot_subplot(p, cs, ls, adj, axes[1], tauB)
        fig_event.connect(fig, leg, sp_legend)

    with PdfPages("file.pdf") as pdf:
        pdf.savefig(fig, dpi=1200, bbox='tight', transparent=True)


@serverwrapper("Error")
def main():

    cs, p = args()

    fig_event = onevent(mpl)
    
    #### BODGE
    p.extra = False
    ################

    for graph_type in p.ct:
        if graph_type == 'M':
            mag(cs, p, fig_event)
        else:
            print(f'TODO: write plotting function for {graph_type}')

    showgraph(mpl, p.sg)


if __name__ == '__main__':
    main()
