from sys import stdout, exit
from contextlib import suppress

with suppress(NameError):
    __IPYTHON__
    from IPython.utils import io

    class Stdout():
        write(*args, **kw)

    stdout = Stdout()

from os import get_terminal_size
from collections import defaultdict
from mango.constants import c


class prog():
    """
    Progress Bar
    """

    def __init__(self):
        self.it = {}
        self.opreflen = 0
        self.size = 40

    @staticmethod
    def END():
        exit(0)

    @staticmethod
    def Test():
        print("Progress Bar activated\n",
              "I am a progress bar, I may slow stuff down. Sorry!")

    def setup(self, *data):
        """
        Set size of progress bar.

        Parameters
        ----------
        data: str
            list of processes and size of bar
            eg '[1, 2, 3] size=40'

        """
        data = ' '.join(data).split('size=')
        self.sections = len(data[0].strip('[]').split())
        self.size = int(data[1]) if len(data) > 1 and data[1] != '' else self.size

    def bar(self, prefix="", it=0, stat="1"):
        """
        Print loading bar to screen.

        Parameters
        ----------
        prefix: string
            String to have before loading bar
        it: float/str
            fraction complete
        stat: str
            current stage

        """
        if not c.tinfo['otty']:
            return
        if not it == "end":
            self._printbar(prefix, float(it), stat)
            return
        else:
            self._printbar(prefix, "end", stat)
            stdout.flush()
            stdout.write("\n")

    def _printbar(self, prefix, it, stat):

        self.npreflen = len(prefix)
        if self.npreflen < self.opreflen:
            prefix += " " * (self.opreflen - self.npreflen)
        else:
            self.opreflen = self.npreflen

        tr = get_terminal_size()
        tr = tr.columns
        if not isinstance(it, float):
            total = 100
        else:
            self.it[stat] = float((it / self.sections) * 100)
            total = sum(self.it.values())

        size2 = 100 / self.size
        length = len(prefix) + (total // size2) + (self.size - total // size2) + 5

        while (tr - 5) <= length:
            if self.size > (tr - 20):
                self.size = self.size - 1
                size2 = 100 / self.size
                prefix = ""
            else:
                prefix = prefix[:-1]
                size2 = 100 / self.size
            length = len(prefix) + (total // size2) + (self.size - total // size2) + 5

        padding = " " * (tr - int(length) - 5)
        hashes = "#" * int(total // size2)
        dashes = "-" * int(self.size - total // size2)
        if total >= 99:
            dashes = ""
            hashes += "#"
        stdout.flush()
        stdout.write("{}[{}{}]{:.2f}%{}\r".format(prefix, hashes, dashes, total, padding))


def _clientProg(*args, **kw):
    pass


if __name__ == '__main__':
    pass
