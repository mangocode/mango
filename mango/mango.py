"""
Mango.

A program to simulate magnetic nanoparticles in a non magnetic fluid.
Developed by James Cook 2015-2019 with supervison by Lorenzo Stella.
"""

# External Dependencies
from sys import exit

# Internal Dependencies
from mango.debug import debug, profile
from mango.constants import c

from mango.arguments import parse, filenaming, verbosity
from mango.managers import serverwrapper
from mango.errors import _strings
from mango.pp.main import readin
from mango.magnetic_motion import integrate

from mango.time import start, end


def _main_run(argparse=True, opts={}):
    """Error Catching."""
    try:
        if opts not in ["-h", '-ifh']:
            assert type(opts) == dict
    except AssertionError:
        print("{}Arguments must be a dictionary".format(_strings._F))

    try:
        profile(main, argparse, opts, file="mprofiler.prof") if c.profile else main(argparse, opts)
    except KeyboardInterrupt:
        exit("{}Exiting".format(_strings._B[0]))
    except EOFError:
        exit(1)


@debug(['main'])
@serverwrapper("Error")
def main(argparse, opts):
    """
    Main.

    the input is parsed
    variables are stored
    calculations are started
    Any plotting is completed as required

    """
    # Setup
    var, flg = setup(argparse, opts)

    # Calculation
    if not flg.pp:
        timer, var.name = integrate(var, flg)
    else:
        timer = end(var.finishtime)
        # TODO find why file not closed so postprocessing can possibly run after calculation if desired
        readin(fname=var.name, run=flg.run, directory=var.directory, block=var.block, flg=flg)

    print(timer.finished())


def setup(argparse, opts):

    var, flg = parse(argparse, opts)
    var.finishtime = start(var.walltime)
    filenaming(var, flg)
    verbosity(var, flg)
    return var, flg


if __name__ == "__main__":
    _main_run(argparse=True)
