from mango.debug import debug
from time import time


class start():
    """Set walltime of the program for clean exiting."""

    def __init__(self, walltime):
        self.start_time = time()
        self.walltime = walltime
        self.finishtime = self.start_time + self.walltime


class end():
    """Make sure the program exits within the walltime."""

    def __init__(self, startinst):
        self.__dict__.update(startinst.__dict__)

    def gettimeleft(self):
        """Return time left in run."""
        self.current_time = time()
        self.timeleft = self.finishtime - self.current_time
        return self.timeleft

    def gettimegone(self):
        self.current_time = time()
        self.timegone = self.current_time - self.start_time
        return self.timegone

    def finished(self):
        """Return current run time."""
        self.finished = time()
        self.timetaken = self.finished - self.start_time
        return "Completed in {:.2f} seconds".format(self.timetaken)


class grace():

    def __init__(self, stats):
        """Allow time for file saving."""
        self.time = 10 * len(stats) if 10 * len(stats) > 120 else 120


class _time():
    """Time and iteration storage."""

    @debug(['time'])
    def __init__(self, dt, t0):
        self.dt = dt
        self.time = t0
        self.iter = 0

    @debug(['time'])
    def time_update(self):
        self.time += self.dt
        self.iter += 1


if __name__ == '__main__':
    pass
