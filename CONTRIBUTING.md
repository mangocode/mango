Style
=====

* Please follow the [PEP-8 Guidelines](https://www.python.org/dev/peps/pep-0008) when contributing new code or modifying existing code

* 4 space tabs


TODO
====

General possible speed improvements:

    - numexpr/Cython

MPI/OpenMP:

    - Currently uses basic splitting of 1 statistic per core. See multiproc.py for todos

Restart file:

    - Currently on a hard stop that manages to finish memoryloop writes two lines instead of one to restartfile
        - fixed for restarting, it just doesnt look good

Postprocessing:

    - Fix kinetic temperature plotting function
    - Clean up basic data plotting (especially column_manip in arguments.py - its horrible)

Tests:

    - Build in tests for project using existing debug infrastructure if needed
    - Files there but not all tests written/integrated

Other:

    - vispy

Known Issues:


