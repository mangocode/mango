"""Version File."""
import subprocess
import os 
def get_gitversion():
    try:
        s = subprocess.run(['git', 'rev-parse', '--short', 'HEAD'], cwd=os.path.dirname(__file__), stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True)
        return '-g_' + s.stdout.decode('ascii').strip()
    except subprocess.CalledProcessError:
        return ''

version = '0.8.1'+get_gitversion()
