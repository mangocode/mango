#!/usr/bin/env python

from setuptools import setup
from sys import version_info
from _version import version

__version__ = version

assert version_info >= (3, 6)

reqs = ['scipy >= 1.0.0',
        'numpy >= 1.19',
        'matplotlib >= 2.0.0',
       ]

optional = ['tables >= 3.6.0',
            'pyfftw >= 0.11.1']

visuals = ['vispy >= 0.6.1',
           'imageio >= 2.6.1',
           'imageio-ffmpeg >= 0.3.0']

minors = ['blessings >= 1.7',
          'inquirer >= 2.6.3']

mpi = ['mpi4py']

tests = ['pytest',
         'pytest-asyncio',
         'wurlitzer']

extras = {'full': optional + minors + visuals,
          'fast': optional,
          'visuals': visuals,
          'minors': minors,
          'mpi': mpi,
          'tests': tests}

setup(
    name="mango",
    version=__version__,
    description="mango",
    packages=["mango"],
    author='James Cook',
    author_email='jcook05@qub.ac.uk',
    url='https://gitlab.com/jcook/Mango',
    install_requires=reqs,
    extra_requires=extras,
    entry_points={
        'console_scripts':
        ['mango = mango.mango:_main_run',
         'mango_mpi = mango.multiproc:ismpi',
         'mango_test = mango.tests:run_all',
         'mango_inputregen = mango.io:inputregen',
         'mango_restartregen = mango.io:restart_gen',
         'mango_strings = mango.tools.string_generation:strings',
         'mango_vars = mango.io:getvar',
         'mango_plotsuscep = mango.tools.plotting:main',
         'mango_vis = mango.tools.visualiser:main']
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: BSD Clause 3 License',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Scientific/Engineering :: Physics'
        'Topic :: Simulation :: Magnetism',
        'Topic :: Simulation :: Molecular Dynamics',
    ],
)
